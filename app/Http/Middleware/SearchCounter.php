<?php
namespace App\Http\Middleware;

use Closure;

class SearchCounter
{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// 给默认的请求参数。
		if (! $request->country_code) {
			$request->offsetSet('country_code', 'CN');
		}

		return $next($request);
	}
}
