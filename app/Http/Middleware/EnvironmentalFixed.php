<?php
namespace App\Http\Middleware;

use Closure;

/**
 * 修正负载均机器反向代理回来的IP
 *
 * @author  
 *
 */
class EnvironmentalFixed
{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// 修正来源IP。
		$real_ip = $request->server->get('HTTP_REMOTEIP') ?: $request->server->get('HTTP_ALI_CDN_REAL_IP');
		if ($real_ip) {
			$request->server->set('REMOTE_ADDR', $real_ip);
		}

		// 修正CDN强制跳转HTTP -> HTTPS配置。
		if ($request->header('x-client-scheme') === 'https') {
			$request->server->set('HTTPS', 'on');
		}

		return $next($request);
	}
}
