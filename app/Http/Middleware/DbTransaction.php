<?php
namespace App\Http\Middleware;

use Closure;
use DB;

/**
 * 将所有请求放到事务中
 *
 * @author  
 *
 */
class DbTransaction
{

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		DB::beginTransaction();
		$response = $next($request);
		if ($response->getStatusCode() < 500) {
			DB::commit();
		} else {
			DB::rollBack();
		}
		return $response;
	}
}
