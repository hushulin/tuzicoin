<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendMobileVerify;
use App\Http\Requests\SendEmailVerify;
use App\Notifications\MobileVerify;
use App\Notifications\EmailVerify;
use App\Models\Country;
use Carbon\Carbon;
use Notification;
use Cache;

/**
 * 验证码
 *
 * @author
 *
 */
class VerifyController extends Controller
{

    /**
     * 发送手机短信验证码
     */
    public function mobile(SendMobileVerify $request)
    {
        $cooldown = 60; // 验证码发送冷却间隔。

        $key = 'Verify@smscode:' . $request->country_code . '-' . $request->mobile;

        // 检测间隔时间。
        if (($sms = Cache::get($key)) && ($seconds = $sms['created_at']->diffInSeconds(null, false)) < $cooldown) {
            // 返回剩余冷却时间。
            return $cooldown - $seconds;
        }
        // 创建验证码。
        $sms = [
          'code' => sprintf('%06d', mt_rand(0, 999999))
        ];

        sms_send($request->mobile, $sms['code']);

        // 发送短信验证码。
        //$country = Country::find($request->country_code);
        //Notification::route('sms', $country->area_code . $request->mobile)->notify(new MobileVerify($sms['code']));

        // 将验证码放入缓存中。
        $sms['created_at'] = Carbon::now();
        $expires_at = Carbon::now()->addMinutes(5); // 过期时间为 5 分钟。
        Cache::put($key, $sms, $expires_at);
        return $cooldown;
    }

    /**
     * 发送邮件验证码
     */
    public function email(SendEmailVerify $request)
    {
        $cooldown = 60; // 验证码发送冷却间隔。

        $key = 'Verify@emailcode:' . md5($request->email);

        // 检测间隔时间。
        if (($email = Cache::get($key)) && ($seconds = $email['created_at']->diffInSeconds(null, false)) < $cooldown) {
            // 返回剩余冷却时间。
            return $cooldown - $seconds;
        }

        // 创建验证码。
        $email = [
          'code' => sprintf('%06d', mt_rand(0, 999999))
        ];

        // 发送邮件验证码。
        Notification::route('mail', $request->email)->notify(new EmailVerify($email['code']));

        // 将验证码放入缓存中。
        $email['created_at'] = Carbon::now();
        $expires_at = Carbon::now()->addMinutes(30); // 过期时间为 30 分钟。
        Cache::put($key, $email, $expires_at);
        return $cooldown;
    }
}
