<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExchangeRate;

class ExchangeRateController extends Controller
{

	public function price(Request $request)
	{
		$data = ExchangeRate::find($request->name);
		if (is_null($data)) {
			return 0;
		}
		return $data->price;
	}
}
