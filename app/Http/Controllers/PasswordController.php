<?php
namespace App\Http\Controllers;

use App\Http\Requests\ModifyPassword;
use App\Http\Requests\ModifySecondaryPassword;
use Auth;

/**
 * 密码管理
 *
 * @author  
 *
 */
class PasswordController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function showLoginPasswordForm()
	{
		return view('password.login');
	}

	public function modifyLoginPassword(ModifyPassword $request)
	{
		// 修改当前用户的登录密码。
		$current_user = Auth::user();
		$current_user->password = $request->new_password;
		$current_user->save();

		// 返回成功信息。
		return redirect()->route('password.login')->withSuccess(__('Modify the login password successfully.'));
	}

	public function showSecondaryPasswordForm()
	{
		return view('password.secondary');
	}

	public function modifySecondaryPassword(ModifySecondaryPassword $request)
	{
		// 修改当前用户的二级密码。
		$current_user = Auth::user();
		$current_user->secondary_password = $request->new_password;
		$current_user->save();

		// 返回成功信息。
		return redirect()->route('password.secondary')->withSuccess(__('Modify the secondary password successfully.'));
	}
}
