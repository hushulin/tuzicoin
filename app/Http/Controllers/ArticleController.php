<?php
namespace App\Http\Controllers;

use App\Models\Article;

/**
 * 文章
 *
 * @author  
 *
 */
class ArticleController extends Controller
{

	/**
	 * 文章详情
	 */
	public function show()
	{
	    $request = app('request');
        $id = $request->input('id', 0);
        $type = $request->input('type', 0);
        $title = '';
        if($type==1) {
            $title = '重要公告';
        } elseif($type==2) {
            $title = '帮助';
        }
        if(empty($id)) {
            $data = Article::where('type',$type)->orderBy('id','desc')->first();
        } else {
            $data = Article::find($id);
        }
		if (is_null($data)) {
			abort(404);
		}
		// 重要公告。
		$articles = Article::where('type',$type)->latest()->get();

		return view('article.show')->with(compact('data', 'articles', 'title'));
	}
}
