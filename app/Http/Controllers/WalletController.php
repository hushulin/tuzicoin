<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\WalletSend;
use Endroid\QrCode\QrCode;
use App\Models\Transaction;
use App\Models\Withdraw;
use App\Models\Address;
use App\Models\Wallet;
use App\Models\User;
use Auth, Lock;

/**
 * 钱包
 *
 * @author  
 *
 */
class WalletController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function showWalletForm(Request $request, $tab = 'deposit', $coin = null)
	{
		if (is_null($coin)) {
			$coin = array_first(C('coins'));
		}
		$request->offsetSet('tab', $tab);
		$request->offsetSet('coin', $coin);

		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得钱包。
		$wallet = $current_user->wallet($coin);
		// 取得地址。
		$address = $current_user->address($coin);

		// 取得所有未到账的存款列表。
		$deposits = $current_user->deposits()->where('coin', $coin)->where('confirmations', 0)->latest()->get();

		// 取得所有待处理的提现列表。
		$withdraws = $current_user->withdraws()->where('coin', $coin)->where('status', Withdraw::STATUS_PENDING)->latest()->get();

		// 取得交易记录。
		$transactions = $current_user->transactions()->with('target')->where('coin', $coin)->latest('id');
		if ($request->filled('type')) {
			$transactions->where('type', $request->type);
		}
		$transactions = $transactions->paginate(10);

		return view('wallet.index')->with(compact('tab', 'wallet', 'address', 'deposits', 'withdraws', 'transactions'));
	}

	/**
	 * 钱包收款二维码图片
	 */
	public function qrcode($address, $amount = null)
	{
		$uri = "bitcoin:{$address}";
		if ($amount) {
			$uri .= "?amount={$bitcoin_amount}";
		}
		$qrcode = new QrCode();
		$qrcode->setText($uri);
		$qrcode->setSize(120);
		$qrcode->setMargin(0);
		return response($qrcode->writeString(), 200, [
			'Content-Type' => $qrcode->getContentType()
		]);
	}

	/**
	 * 发送数字货币
	 */
	public function send(WalletSend $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得钱包。
		$wallet = $current_user->wallet($request->coin);

		// 判断目标地址是否是站内用户，若是则取出目标用户。
		$address = Address::where('coin', $request->coin)->where('address', $request->address)->where('user_id', '!=', 0)->first();
		$target_user = $address ? $address->user : null;

		// 在并发锁中执行。
		$response = null;
		$lock_key = 'Wallet@send:' . $current_user->id;
		try {
			Lock::acquire($lock_key);
			if (is_null($target_user)) {
				// 网络转出。
				$response = $this->sendNetworkOut($request, $wallet);
			} else {
				// 平台转出。
				$response = $this->sendPlatformOut($request, $wallet, $target_user);
			}
		} finally {
			Lock::release($lock_key);
		}
		return $response;
	}

	/**
	 * 平台转出
	 * 平台转出为转账。
	 */
	protected function sendPlatformOut(WalletSend $request, $wallet, $target_user)
	{
		// 创建交易。
		$transaction = new Transaction();
		$transaction->wallet()->associate($wallet);
		$transaction->amount = $request->quantity;
		$transaction->address = $request->address; // 转出目标地址。
		$transaction->remark = $request->remark ?: '';
		$transaction->type = Transaction::TYPE_PLATFORM_OUT; // 平台转出。
		$transaction->fee = 0; // 平台间转账免手续费。
		$transaction->target()->associate($target_user);

		// 检查并扣除可用余额。
		if ($wallet->balance_available < $transaction->amount) {
			return back()->withInput()->withErrors([
				'quantity' => __('Insufficient balance available.')
			]);
		}
		$wallet->decrement('balance_available', $transaction->amount);

		// 保存交易。
		$transaction->save();

		// 返回成功信息。
		return back()->withSuccess(__('Sent successfully.'));
	}

	/**
	 * 网络转出
	 * 网络转出为提现。
	 */
	protected function sendNetworkOut(WalletSend $request, $wallet)
	{
		// 创建提现。
		$withdraw = new Withdraw();
		$withdraw->wallet()->associate($wallet);
		$withdraw->amount = $request->quantity;
		$withdraw->fee = C('network_out_fee_' . strtolower($wallet->coin));
		$withdraw->address = $request->address;
		$withdraw->remark = $request->remark ?: '';

		// 检查并冻结可用余额。
		$total_amount = $withdraw->amount + $withdraw->fee;
		if ($wallet->balance_available < $total_amount) {
			return back()->withInput()->withErrors([
				'quantity' => __('The available balance is not sufficient to cover the handling fee.')
			]);
		}
		$wallet->decrement('balance_available', $total_amount);
		$wallet->increment('balance_locked', $total_amount);

		// 保存提现。
		$withdraw->save();

		// 返回成功信息。
		return back()->withSuccess(__('Send success, please wait for platform review.'));
	}
}
