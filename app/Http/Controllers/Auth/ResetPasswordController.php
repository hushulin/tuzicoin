<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPassword;
use App\Models\Country;
use App\Models\User;

/**
 * 重置密码
 *
 * @author  
 *
 */
class ResetPasswordController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest')->only('showResetPasswordForm', 'resetPassword');
		$this->middleware('auth')->only('showResetSecondaryPasswordForm');
	}

	public function showCreateSecondaryPasswordForm()
	{
		$countries = Country::all();
		return view('auth.passwords.create')->with(compact('countries'));
	}

	public function showResetPasswordForm()
	{
		$countries = Country::all();
		return view('auth.passwords.reset')->with(compact('countries'));
	}

	public function resetPassword(ResetPassword $request)
	{
		// 重置登录密码。
		$user = User::where('country_code', $request->country_code)->where('mobile', $request->mobile)->first();
		$user->password = $request->new_password;
		$user->save();

		// 返回成功信息。
		return redirect()->route('login')->withSuccess(__('Modify the login password successfully.'));
	}

	public function showResetSecondaryPasswordForm()
	{
		$countries = Country::all();
		return view('auth.passwords.secondary')->with(compact('countries'));
	}

	public function resetSecondaryPassword(ResetPassword $request)
	{
		// 重置二级密码。
		$user = User::where('country_code', $request->country_code)->where('mobile', $request->mobile)->first();
		$user->secondary_password = $request->new_password;
		$user->save();

		// 返回成功信息。
		return redirect()->route('security')->withSuccess(__('Modify the secondary password successfully.'));
	}
}
