<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;
use App\Models\Country;
use App\Models\User;
use App\Models\BitcoinAddress;

/**
 * 用户注册
 *
 * @author  
 *
 */
class RegisterController extends Controller
{
	/*
	 |--------------------------------------------------------------------------
	 | Register Controller
	 |--------------------------------------------------------------------------
	 |
	 | This controller handles the registration of new users as well as their
	 | validation and creation. By default this controller uses a trait to
	 | provide this functionality without requiring any additional code.
	 |
	 */

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'username' => 'required|string|max:255|unique:users',
			'country_code' => 'required|exists:countries,code',
			'mobile' => [
				'required',
				Rule::phone()->countryField('country_code'),
				Rule::unique('users')->where(function ($query) use ($data) {
					$query->where('country_code', $data['country_code']);
				})
			],
			'smsverify' => 'required|smsverify',
			'password' => 'required|string|min:6'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	protected function create(array $data)
	{
		return User::create([
			'username' => $data['username'],
			'country_code' => $data['country_code'],
			'mobile' => $data['mobile'],
			'password' => $data['password'],
			'mobile_verified' => true
		]);
	}

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showRegistrationForm()
	{
		$countries = Country::all();
		return view('auth.register')->with(compact('countries'));
	}
}
