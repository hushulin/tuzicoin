<?php

namespace App\Http\Controllers;

use App\Models\PayInfo;
use Illuminate\Http\Request;
use App\Models\Counter;
use App\Models\User;

/**
 * 用户公共资料
 *
 * @author
 *
 */
class UserController extends Controller
{

    /**
     * 用户公共主页
     */
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            abort(404);
        }

        // 限定请求参数。
        if (!in_array($request->type, [
          Counter::TYPE_ONLINE_BUY,
          Counter::TYPE_ONLINE_SELL
        ])) {
            $request->offsetSet('type', Counter::TYPE_ONLINE_BUY);
        }

        // 取得购买类型的柜台列表。
        $counters = $user->counters()->latest()->where('type', $request->type)->where('status',
          Counter::STATUS_OPEN)->paginate();

        return view('user.show')->with(compact('user', 'counters'));
    }

    /**
     * 显示用户头像
     */
    public function showAvatar($user_id)
    {
        // 取得用户。
        $user = User::find($user_id);

        // 取得头像图片。
        if (is_null($user) || empty($user->avatar)) {
            abort(404);
        }
        $image = $user->avatar['image'];
        $mime = $user->avatar['mime'];

        // 返回图片。
        return response()->file(storage_path('app/' . $image), [
          'Content-type' => $mime
        ]);
    }


    public function updateAliPay(Request $request)
    {
        $params = $this->validate($request, [
          'ali_user_name' => 'required',
          'ali_account'   => 'required',
          'alipay_img'    => 'nullable',
          'alipay_img'         => 'nullable|image|max:2048'
        ]);
        if ($alipay_img = $request->file('alipay_img')) {
            $file_name = time() . '.' . $alipay_img->getClientOriginalExtension();
            $params['alipay_img'] = '/app/public/' . $file_name;
            $alipay_img->move(public_path('app/public'), $file_name);
        }
        $this->handlePayInfo($params);
        return back()->withSuccess(__('Submit successfully.'));

        //return response()->redirectTo(route('pay.info.setting'));
    }

    public function updateWechatPay(Request $request)
    {
        $params = $this->validate($request, [
          'wechat_user_name' => 'required',
          'wechat_account'   => 'required',
          'wechat_img'       => 'nullable',
            'wechat_img' => 'nullable|image|max:2048',
        ]);
        if ($alipay_img = $request->file('wechat_img')) {
            $file_name = time() . '.' . $alipay_img->getClientOriginalExtension();
            $params['wechat_img'] = '/app/public/' . $file_name;
            $alipay_img->move(public_path('app/public'), $file_name);
        }
        $this->handlePayInfo($params);

        // 返回成功信息。
        return back()->withSuccess(__('Submit successfully.'));
        // return response()->redirectTo(route('pay.info.setting'));
    }

    public function updateBank(Request $request)
    {
        $params = $this->validate($request, [
          'bank_user_name' => 'required',
          'bank_account'   => 'required',

        ]);
        $this->handlePayInfo($params);

        // 返回成功信息。
        return back()->withSuccess(__('Submit successfully.'));
    }


    private function handlePayInfo($data)
    {
        $user = \Auth::user();
        $pay_info = PayInfo::where('uid', '=', $user->id)->first();
        if (!$pay_info) {
            return PayInfo::insert([
              'uid'      => $user->id,
              'pay_info' => json_encode($data)
            ]);
        }
        if (!$pay_info->pay_info) {
            return $pay_info->save(['pay_info' => json_encode($data)]);
        }
        $update_data = array_merge(json_decode($pay_info->pay_info, true), $data);
        return $pay_info->update(['pay_info' => json_encode($update_data)]);
    }


}
