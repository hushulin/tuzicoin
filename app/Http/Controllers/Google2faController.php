<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use Endroid\QrCode\QrCode;
use Auth;

/**
 * 谷歌二次验证
 *
 * @author  
 *
 */
class Google2faController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * 绑定
	 */
	public function showBindForm()
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		$google2fa = new Google2FA();

		// 生产谷歌二步验证密钥。
		$secret = old('secret', $google2fa->generateSecretKey());

		// 生成二维码。
		$uri = $google2fa->getQRCodeUrl(config('app.name'), $current_user->username, $secret);
		$qrcode = new QrCode($uri);
		$qrcode->setSize(150)->setMargin(0);
		$qrcode_src = 'data:' . $qrcode->getContentType() . ';base64,' . base64_encode($qrcode->writeString());

		return view('google2fa.bind')->with(compact('secret', 'qrcode_src'));
	}

	public function bind(Request $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 检查验证码是否正确。
		$secret = $current_user->google2fa_secret ?: $request->secret;
		$code = $request->code;
		$google2fa = new Google2FA();
		if (! $google2fa->verifyKey($secret, $code)) {
			return back()->withInput()->withErrors([
				'code' => __('Google Verification Code validation failed.')
			]);
		}

		// 绑定谷歌二次验证。
		$current_user->google2fa_secret = $request->secret;
		$current_user->save();

		// 返回成功信息。
		return redirect()->route('security')->withSuccess(__('Binding Google Verification is successfully.'));
	}

	/**
	 * 解除绑定
	 */
	public function showUnbindForm()
	{
		return view('google2fa.unbind');
	}

	public function unbind(Request $request)
	{
		$request->validate([
			'code' => 'required|google2fa'
		]);

		// 解除谷歌二次验证。
		$current_user = Auth::user();
		$current_user->google2fa_secret = '';
		$current_user->save();

		// 返回成功信息。
		return redirect()->route('security')->withSuccess(__('Google verification unbundled success.'));
	}
}
