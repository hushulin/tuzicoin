<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ignore;
use Auth;

/**
 * 黑名单
 *
 * @author  
 *
 */
class IgnoreController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * 添加黑名单
	 */
	public function add(Request $request)
	{
		if (Auth::id() != $request->user_id) {
			Ignore::updateOrCreate([
				'source_id' => Auth::id(),
				'target_id' => $request->user_id
			]);
		}
		return back();
	}

	/**
	 * 移除黑名单
	 */
	public function remove(Request $request)
	{
		$data = Auth::user()->ignores()->where('target_id', $request->user_id)->first();
		if ($data) {
			$data->delete();
		}
		return back();
	}

	/**
	 * 我屏蔽的人
	 */
	public function my()
	{
		$data = Auth::user()->ignores()->with('target')->paginate(10);

		return view('ignore.my')->with(compact('data'));
	}
}
