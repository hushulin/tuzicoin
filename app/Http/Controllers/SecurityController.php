<?php

namespace App\Http\Controllers;

use App\Http\Requests\BindEmail;
use App\Http\Requests\BindMobile;
use App\Models\PayInfo;
use Auth;

/**
 * 安全中心
 *
 * @author
 *
 */
class SecurityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('security.index');
    }

    /**
     * 绑定邮箱
     */
    public function showEmailForm()
    {
        return view('security.email');
    }

    public function bindEmail(BindEmail $request)
    {
        // 绑定邮箱。
        $current_user = Auth::user();
        $current_user->email = $request->email;
        $current_user->email_verified = true;
        $current_user->save();

        // 返回成功信息。
        return redirect()->route('security')->withSuccess(__('Binding email is successfully.'));
    }

    /**
     * 绑定手机
     */
    public function showMobileForm()
    {
        return view('security.mobile');
    }

    public function bindMobile(BindMobile $request)
    {
        // 绑定手机。
        $current_user = Auth::user();
        $current_user->mobile = $request->new_mobile;
        $current_user->save();

        // 返回成功信息。
        return redirect()->route('security')->withSuccess(__('Binding mobile is successfully.'));
    }

    public function showPaySettingForm()
    {
        $user = \Auth::user();
        $pay_info = PayInfo::where('uid', '=', $user->id)->first();
        $data['pay_info'] = [];
        if ($pay_info && $pay_info->pay_info) {
            $data['pay_info'] = json_decode($pay_info->pay_info,true);
        }

        return view('security.pay_setting', $data);
    }
}
