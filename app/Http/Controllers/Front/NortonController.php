<?php
/**
 * Created by PhpStorm.
 * User: norton
 * Date: 2018/5/26
 * Time: 下午8:45
 */

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;


class NortonController
{
    public function counterCreate()
    {
        return view('front.create');
    }
    public function createVirtualOrder()
    {
        return view('front.virtual');
    }
}