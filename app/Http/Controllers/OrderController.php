<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateOrder;
use App\Http\Requests\OrderReview;
use App\Notifications\MessageHaveRead;
use App\Models\Message;
use App\Models\Counter;
use App\Models\Order;
use Auth, Lock, Hash;

/**
 * 订单管理
 *
 * @author  
 *
 */
class OrderController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * 创建订单
	 */
	public function create(CreateOrder $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得柜台。
		$counter = Counter::find($request->counter_id);

		// 检查柜台状态。
		if ($counter->status === Counter::STATUS_CLOSED) {
			return back()->withInput()->withErrors(__('The counter is closed.'));
		}

		// 创建订单。
		$order = new Order();
		$order->counter()->associate($counter);
		// 买家与卖家信息。
		switch ($counter->type) {
			case Counter::TYPE_ONLINE_BUY:
				$order->buyer()->associate($counter->user);
				$order->seller()->associate($current_user);
				break;
			case Counter::TYPE_ONLINE_SELL:
				$order->seller()->associate($counter->user);
				$order->buyer()->associate($current_user);
				break;
		}

		// 成交价。
		$order->price = $counter->price;
		// 计算金额与数量。
		switch ($request->in) {
			case 'amount':
				$order->amount = $request->amount;
				$order->quantity = $order->amount / $order->price;
				break;
			case 'quantity':
				$order->quantity = $request->quantity;
				$order->amount = $order->quantity * $order->price;
				break;
		}
		// 确保精度。
		$order->quantity = round($order->quantity, 8);
		$order->amount = round($order->amount, 2);
		// 同步到请求的参数中。
		$request->offsetSet('amount', $order->amount);

		// 检查柜台的限额。
		$this->validate($request, [
			'amount' => 'numeric|between:' . $counter->min_amount . ',' . $counter->max_amount
		]);

		// 计算手续费。
		$order->fee = round($order->quantity * C('counter_fee') / 100, 8);

		// 计算需要锁定的金额。
		$amount_locked = $order->quantity;
		if ($order->seller_id == $counter->user_id) {
			$amount_locked += $order->fee;
		}

		// 检查卖方的钱包可用余额并冻结。
		// 冻结余额需要在并发锁中执行。
		$lock_key = 'UserBalance:' . $order->seller_id;
		try {
			Lock::acquire($lock_key);
			$wallet = $order->seller->wallet($counter->coin);
			if ($wallet->balance_available < $amount_locked) {
				Lock::release($lock_key);
				return back()->withInput()->withErrors([
					$request->in => __('Seller available balance is insufficient.')
				]);
			}
			$wallet->decrement('balance_available', $amount_locked);
			$wallet->increment('balance_locked', $amount_locked);
		} finally {
			Lock::release($lock_key);
		}

		// 持久化订单。
		$order->save();

		// 返回到订单详情页。
		return redirect()->route('order.detail', [
			'id' => $order->id
		]);
	}

	/**
	 * 订单详情
	 */
	public function detail(Request $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得订单。
		$order = Order::byUser($current_user)->with('counter')->find($request->id);
		if (is_null($order)) {
			abort(403);
		}

		// 取得聊天信息列表。
		$messages = $order->messages()
			->byUser($current_user)
			->with('source', 'target')
			->latest()
			->take(1000)
			->get()
			->reverse()
			->values();

		//  标记当前用户的所有消息为已读。
		if (! $messages->isEmpty()) {
			// 通知发送人对方已读。
			$notify_messages = $messages->filter(function ($message) {
				return ! $message->is_read;
			});
			if (! $notify_messages->isEmpty()) {
				Message::whereIn('id', $notify_messages->pluck('id'))->update([
					'is_read' => true
				]);
				$notify_messages = $notify_messages->filter(function ($message) {
					return $message->type != Message::TYPE_SYSTEM;
				});
				if (! $notify_messages->isEmpty()) {
					$notify_messages->first()->source->notify(new MessageHaveRead($notify_messages));
				}
			}
		}

		// 返回订单详情视图。
		return view('order.detail')->with(compact('order', 'messages', 'opcode'));
	}

	/**
	 * 订单列表
	 */
	public function showList(Request $request)
	{
		// 限定请求参数。
		if (! in_array($request->type, [
			'Processing',
			'Completed'
		])) {
			$request->offsetSet('type', 'Processing');
		}

		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得订单列表。
		$model = Order::byUser($current_user)->with('counter', 'buyer', 'seller')->latest();
		switch ($request->type) {
			case 'Processing': // 进行中的交易，包含未评价。
				$model->processing($current_user);
				break;
			case 'Completed': // 已完成的交易，包含已评价。
				$model->completed($current_user);
				break;
		}

		// 取得每张订单发给当前用户的未读消息数。
		$model->withCount([
			'messages AS unread_message_count' => function ($query) use ($current_user) {
				$query->where('target_id', $current_user->id)->where('is_read', false);
			}
		]);

		// 取得单页数据。
		$orders = $model->paginate(10);

		// 分别取得进行中与已完成的订单未读消息数。
		$processing_message_count = Message::where('target_id', $current_user->id)->where('is_read', false)->whereHas('order', function ($model) use ($current_user) {
			$model->processing($current_user);
		})->count();
		$completed_message_count = Message::where('target_id', $current_user->id)->where('is_read', false)->whereHas('order', function ($model) use ($current_user) {
			$model->completed($current_user);
		})->count();

		return view('order.panel')->with(compact('orders', 'processing_message_count', 'completed_message_count'));
	}

	/**
	 * 撤销订单
	 */
	public function revoke(Request $request)
	{
		Lock::granule('Order:' . $request->id, function () use ($request) {

			// 取得当前登录用户。
			$current_user = Auth::user();

			// 取得要撤销的订单。
			$order = Order::byUser($current_user)->find($request->id);

			// 待付款状态可被买家撤销。
			if ($order && $order->buyer_id === $current_user->id && in_array($order->status, [
				Order::STATUS_WAIT_PAYMENT,
				Order::STATUS_UNCONFIRMED
			])) {

				// 修改订单状态。
				$order->status = Order::STATUS_REVOKED;
				$order->save();
			}
		});
	}

	/**
	 * 重启订单
	 */
	public function restart(Request $request)
	{
		Lock::granule('Order:' . $request->id, function () use ($request) {

			// 取得当前登录用户。
			$current_user = Auth::user();

			// 取得要重启的订单。
			$order = Order::byUser($current_user)->find($request->id);

			// 已过期状态可被卖家重启。
			if ($order && $order->status === Order::STATUS_EXPIRED && $order->seller_id === $current_user->id) {

				// 计算需要锁定的金额。
				$amount_locked = $order->quantity;
				if ($order->seller_id == $order->counter->user_id) {
					$amount_locked += $order->fee;
				}

				// 检查卖方的比特币可用余额并冻结。
				// 冻结余额需要在并发锁中执行。
				$lock_key = 'UserBalance:' . $order->seller_id;
				try {
					Lock::acquire($lock_key);
					$wallet = $order->seller->wallet($counter->coin);
					if ($wallet->balance_available < $amount_locked) {
						Lock::release($lock_key);
						return back()->withErrors(__('Seller available balance is insufficient.'));
					}
					$wallet->decrement('balance_available', $amount_locked);
					$wallet->increment('balance_locked', $amount_locked);
				} finally {
					Lock::release($lock_key);
				}

				// 修改订单状态。
				$order->status = Order::STATUS_WAIT_PAYMENT;
				$order->save();
			}
		});
	}

	/**
	 * 已付款
	 */
	public function payment(Request $request)
	{
	    $check = Hash::check($request->input('secondary_password', ''), Auth::user()->secondary_password);
        if(!$check) {
            return response()
                ->json(['code'=>1,'msg'=>'错误的安全密码','result'=>null], 200, [], JSON_UNESCAPED_UNICODE);
        }
		Lock::granule('Order:' . $request->id, function () use ($request) {
			// 取得当前登录用户。
			$current_user = Auth::user();

			// 取得要付款的订单。
			$order = Order::byUser($current_user)->find($request->id);

			// 待付款状态买家可付款。
			if ($order && $order->buyer_id === $current_user->id && $order->status === Order::STATUS_WAIT_PAYMENT) {

				// 修改订单状态。
				$order->status = Order::STATUS_UNCONFIRMED;
				$order->save();
			}
		});
        return response()
            ->json(['code'=>0,'msg'=>'标记已付款完成,成功!','result'=>null], 200, [], JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 释放托管
	 */
	public function release(Request $request)
	{
		Lock::granule('Order:' . $request->id, function () use ($request) {

			// 取得当前登录用户。
			$current_user = Auth::user();

			// 取得要付款的订单。
			$order = Order::byUser($current_user)->find($request->id);

			// 卖家可确认付款。
			if ($order && $order->seller_id === $current_user->id && in_array($order->status, [
				Order::STATUS_WAIT_PAYMENT,
				Order::STATUS_UNCONFIRMED
			])) {

				// 修改订单状态。
				$order->status = Order::STATUS_CONFIRMED;
				$order->save();
			}
		});
	}

	/**
	 * 评论订单
	 */
	public function review(OrderReview $request)
	{
		Lock::granule('Order:' . $request->id, function () use ($request) {

			// 取得当前登录用户。
			$current_user = Auth::user();

			// 取得要评论的订单。
			$order = Order::byUser($current_user)->find($request->id);
			if ($order && $order->status === Order::STATUS_CONFIRMED) {

				// 修改对应的评论。
				switch ($current_user->id) {
					case $order->buyer_id:
						$order->buyer_feedback = $request->feedback;
						break;
					case $order->seller_id:
						$order->seller_feedback = $request->feedback;
						break;
				}
				$order->save();
			}
		});
	}
}
