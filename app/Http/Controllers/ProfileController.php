<?php
namespace App\Http\Controllers;

use App\Http\Requests\SaveProfile;
use App\Http\Requests\VerificationIdentityCard;
use App\Http\Requests\VerificationPassport;
use App\Http\Requests\VerificationDriverLicense;
use App\Http\Requests\ImageUpload;
use App\Models\Authentication;
use Auth;

/**
 * 用户信息
 *
 * @author  
 *
 */
class ProfileController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * 基本信息
	 */
	public function index()
	{
		return view('profile.index');
	}

	/**
	 * 保存基本信息
	 */
	public function save(SaveProfile $request)
	{
		// 修改当前用户的二级密码。
		$current_user = Auth::user();
		$current_user->introduction = $request->introduction;
		$current_user->save();

		// 返回成功信息。
		return back()->withSuccess(__('Saved successfully.'));
	}

	/**
	 * 修改头像
	 */
	public function avatar(ImageUpload $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 保存头像图片。
		$image = $request->file('image');
		list ($width, $height, $type) = getimagesize($image->path());
		$current_user->avatar = [
			'image' => $image->store('avatar'),
			'width' => $width,
			'height' => $height,
			'mime' => image_type_to_mime_type($type)
		];
		$current_user->save();

		// 返回最新的头像信息。
		return $current_user->avatar;
	}

	/**
	 * 身份验证
	 */
	public function showVerificationForm()
	{
		// 取得验证信息。
		$authentication = Auth::user()->authentication;

		// 返回验证提交表单。
		return view('profile.verification.index')->with(compact('authentication'));
	}

	/**
	 * 身份验证-中国大陆身份证
	 */
	public function verificationIdentityCard(VerificationIdentityCard $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 检查用户是否已提交过验证信息。
		// 若已提交过，则直接跳转到认证中心。
		if ($current_user->authentication_id) {
			return redirect()->route('profile.verification');
		}

		// 创建验证信息。
		$authentication = new Authentication();
		$authentication->type = Authentication::TYPE_IDENTITY_CARD;
		$authentication->realname = $request->realname;
		$authentication->id_number = $request->id_number;
		$authentication->save();
		$authentication->user()->save($current_user);

		// 返回成功信息。
		return back()->withSuccess(__('Submit successfully.'));
	}

	/**
	 * 身份验证-护照
	 */
	public function verificationPassport(VerificationPassport $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 检查用户是否已提交过验证信息。
		// 若已提交过，则直接跳转到认证中心。
		if ($current_user->authentication_id) {
			return redirect()->route('profile.verification');
		}

		// 创建验证信息。
		$authentication = new Authentication();
		$authentication->type = Authentication::TYPE_PASSPORT;
		if(!$request->hasFile('image')){
        // 返回成功信息。
        return back()->withSuccess(__('Submit successfully.'));
		}
		$image = $request->file('image');
		list ($width, $height, $type) = getimagesize($image->path());
		$authentication->extra = [
			'image' => $image->store('authentication'),
			'width' => $width,
			'height' => $height,
			'mime' => image_type_to_mime_type($type)
		];
		$authentication->save();
		$authentication->user()->save($current_user);

		// 返回成功信息。
		return back()->withSuccess(__('Submit successfully.'));
	}

	/**
	 * 身份验证-驾照
	 */
	public function verificationDriverLicense(VerificationDriverLicense $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 检查用户是否已提交过验证信息。
		// 若已提交过，则直接跳转到认证中心。
		if ($current_user->authentication_id) {
			return redirect()->route('profile.verification');
		}

		// 创建验证信息。
		$authentication = new Authentication();
		$authentication->type = Authentication::TYPE_DRIVER_LICENSE;
		$image = $request->file('image');
		list ($width, $height, $type) = getimagesize($image->path());
		$authentication->extra = [
			'image' => $image->store('authentication'),
			'width' => $width,
			'height' => $height,
			'mime' => image_type_to_mime_type($type)
		];
		$authentication->save();
		$authentication->user()->save($current_user);

		// 返回成功信息。
		return back()->withSuccess(__('Submit successfully.'));
	}

	/**
	 * 身份验证图片
	 */
	public function showVerificationImage()
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得验证图片。
		if (is_null($authentication = $current_user->authentication) || empty($authentication->extra['image'])) {
			abort(404);
		}
		$image = $authentication->extra['image'];
		$mime = $authentication->extra['mime'];

		// 返回图片。
		return response()->file(storage_path('app/' . $image), [
			'Content-type' => $mime
		]);
	}
}
