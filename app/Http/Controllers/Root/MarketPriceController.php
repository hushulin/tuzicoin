<?php
namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MarketPrice;

/**
 * 市场价信息
 *
 * @author  
 *
 */
class MarketPriceController extends Controller
{

	/**
	 * 市场价列表
	 */
	public function getList(Request $request)
	{
		// 取得数据模型。
		$model = MarketPrice::oldest('currency')->oldest('market')->oldest('id');

		// 附加筛选条件。
		foreach ([
			'market',
			'currency'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
			}
		}

		// 取得单页数据。
		$data = $model->paginate($request->cookie('limit', 15));

		// 附加翻页参数。
		$data->appends($request->all());

		return view('root.market-price.list', compact('data'));
	}
}
