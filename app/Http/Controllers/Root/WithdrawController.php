<?php
namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Withdraw;
use Carbon\Carbon;

/**
 * 提现管理
 *
 * @author  
 *
 */
class WithdrawController extends Controller
{

	/**
	 * 提现列表
	 */
	public function getList(Request $request)
	{
		// 验证输入。
		$this->validate($request, [
			'date' => [
				'nullable',
				'regex:/^\d{4}-\d{2}-\d{2}\s~\s\d{4}-\d{2}-\d{2}$/'
			]
		]);

		// 取得时间范围。
		$date_start = null;
		$date_end = null;
		if ($request->filled('date')) {
			list ($date_start, $date_end) = explode(' ~ ', $request->date);
			$date_start = Carbon::parse($date_start)->startOfDay();
			$date_end = Carbon::parse($date_end)->endOfDay();
		}

		// 取得数据模型。
		$model = Withdraw::with('user')->latest('id');

		// 附加筛选条件。
		foreach ([
			'user_id',
			'coin',
			'status'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
			}
		}
		if ($date_start) {
			$model->where('created_at', '>=', $date_start);
		}
		if ($date_end) {
			$model->where('created_at', '<=', $date_end);
		}

		// 关联表搜索。
		foreach ([
			'username',
			'mobile'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->whereHas('user', function ($query) use ($field, $value) {
					$query->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
				});
			}
		}

		// 取得单页数据。
		$data = $model->paginate($request->cookie('limit', 15));

		// 附加翻页参数。
		$data->appends($request->all());

		return view('root.withdraw.list', compact('data'));
	}

	/**
	 * 处理完成
	 */
	public function postCompleted(Request $request)
	{
		// 修改数据状态。
		$data = Withdraw::find($request->id);
		if (! is_null($data)) {
			$data->status = Withdraw::STATUS_SUCCESS;
			$data->save();
		}

		// 返回成功信息。
		return redirect()->back()->withMessageSuccess('已成功处理完成该提现。');
	}

	/**
	 * 拒绝提现
	 */
	public function postRefuse(Request $request)
	{
		// 验证输入。
		$this->validate($request, [
			'reason' => 'required'
		]);

		// 修改数据状态。
		$data = Withdraw::find($request->id);
		if (! is_null($data)) {
			$data->status = Withdraw::STATUS_FAILURE;
			$data->reason = $request->reason ?: '';
			$data->save();
		}

		// 返回成功信息。
		return redirect()->back()->withMessageSuccess('已成功驳回该提现申请。');
	}
}