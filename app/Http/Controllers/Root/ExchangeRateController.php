<?php
namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExchangeRate;

/**
 * 法币汇率
 *
 * @author  
 *
 */
class ExchangeRateController extends Controller
{

	/**
	 * 汇率列表
	 */
	public function getList(Request $request)
	{
		// 取得数据模型。
		$model = ExchangeRate::oldest('name');

		// 附加筛选条件。
		foreach ([
			'name'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
			}
		}

		// 取得单页数据。
		$data = $model->paginate($request->cookie('limit', 15));

		// 附加翻页参数。
		$data->appends($request->all());

		return view('root.exchange-rate.list', compact('data'));
	}
}
