<?php
namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Counter;
use Carbon\Carbon;

/**
 * 柜台管理
 *
 * @author  
 *
 */
class CounterController extends Controller
{

	/**
	 * 柜台列表
	 */
	public function getList(Request $request)
	{
		// 验证输入。
		$this->validate($request, [
			'date' => [
				'nullable',
				'regex:/^\d{4}-\d{2}-\d{2}\s~\s\d{4}-\d{2}-\d{2}$/'
			]
		]);

		// 取得时间范围。
		$date_start = null;
		$date_end = null;
		if ($request->filled('date')) {
			list ($date_start, $date_end) = explode(' ~ ', $request->input('date'));
			$date_start = Carbon::parse($date_start)->startOfDay();
			$date_end = Carbon::parse($date_end)->endOfDay();
		}

		// 取得数据模型。
		$model = Counter::latest('id');
		$model->with('user');

		// 附加筛选条件。
		foreach ([
			'id',
			'status'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
			}
		}
		if ($date_start) {
			$model->where('created_at', '>=', $date_start);
		}
		if ($date_end) {
			$model->where('created_at', '<=', $date_end);
		}

		// 关联表搜索。
		foreach ([
			'username',
			'mobile'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->whereHas('user', function ($model) use ($field, $value) {
					$model->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
				});
			}
		}

		// 取得单页数据。
		$data = $model->paginate($request->cookie('limit', 15));

		// 附加翻页参数。
		$data->appends($request->all());

		return view('root.counter.list', compact('data'));
	}
}
