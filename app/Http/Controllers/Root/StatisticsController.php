<?php
namespace App\Http\Controllers\Root;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Cache;
use DB;

/**
 * 数据统计
 *
 * @author  
 *
 */
class StatisticsController extends Controller
{

	/**
	 * 统计时间范围
	 */
	protected $date_start, $date_end;

	public function __construct(Request $request)
	{
		// 验证输入。
		$this->validate($request, [
			'date_range' => 'regex:/^\d{4}-\d{2}-\d{2}\s~\s\d{4}-\d{2}-\d{2}$/'
		]);

		// 取得时间范围。
		if ($request->filled('date_range')) {
			list ($this->date_start, $this->date_end) = explode(' ~ ', $request->input('date_range'));
			$this->date_start = Carbon::parse($this->date_start)->startOfDay();
			$this->date_end = Carbon::parse($this->date_end)->endOfDay();
		} else {
			// 默认最近一个月。
			$this->date_start = Carbon::now()->subMonth()->startOfDay();
			$this->date_end = Carbon::now()->endOfDay();
		}
	}

	/**
	 * 用户统计
	 */
	public function getUser(Request $request)
	{
		// 新增用户统计。
		$new_users = collect(DB::select('
			SELECT
				`date` ,
				COUNT(*) AS `count`
			FROM
			(
				SELECT
					DATE_FORMAT( `created_at` , "%Y-%m-%d" ) AS `date`
				FROM
					`users`
				WHERE
					`created_at` BETWEEN ? AND ?
				ORDER BY `date` ASC
			) t
			GROUP BY `date`
		', [
			$this->date_start,
			$this->date_end
		]));

		// 活跃用户统计。
		$active_users = collect(DB::select('
			SELECT
				`date` ,
				COUNT(*) AS `count`
			FROM
			(
				SELECT
					DISTINCT `user_id` ,
					DATE_FORMAT( `created_at` , "%Y-%m-%d" ) AS `date`
				FROM
					`user_login_logs`
				WHERE
					`created_at` BETWEEN ? AND ?
				ORDER BY `date` ASC
			) t
			GROUP BY `date`
		', [
			$this->date_start,
			$this->date_end
		]));

		// 补全时间线上的所有节点。
		for ($date = with(clone $this->date_start); $date->lte($this->date_end); $date->addDay()) {
			$date_string = $date->toDateString();

			$point = $new_users->where('date', $date_string);
			if ($point->isEmpty()) {
				$new_users->push((object) [
					'date' => $date_string,
					'count' => 0
				]);
			}

			$point = $active_users->where('date', $date_string);
			if ($point->isEmpty()) {
				$active_users->push((object) [
					'date' => $date_string,
					'count' => 0
				]);
			}
		}

		// 刷新排序。
		$new_users = $new_users->sort()->values();
		$active_users = $active_users->sort()->values();

		// 用户数与在线数。
		$user_count = User::count();
		$online_count = count(online_users());

		return view('root.statistics.user')->with([
			'user_count' => $user_count,
			'online_count' => $online_count,
			'new_users' => $new_users,
			'active_users' => $active_users,
			'date_range' => $this->date_start->toDateString() . ' ~ ' . $this->date_end->toDateString()
		]);
	}

	/**
	 * 流水统计
	 */
	public function getFlow(Request $request)
	{
		// 取出统计数据。
		$data = collect();

		// 取出时间线上的所有节点。
		for ($date = with(clone $this->date_start); $date->lte($this->date_end); $date->addDay()) {

			$date_string = $date->toDateString();

			// 当日的统计缓存十秒，当日之前的统计缓存一天。
			$expires = Carbon::now()->startOfDay();
			if ($expires->gt($date)) {
				$expires->addDay();
			}
			$expires = Carbon::now(); // TODO: 暂时禁用缓存。
			$point = Cache::remember('Statistics@Flow:' . $date_string, $expires, function () use ($date) {
				$point = DB::select('
					SELECT
						`coin` ,
						`type` ,
						SUM( `amount` ) AS `total_amount`
					FROM
						`transactions`
					WHERE
						`created_at` BETWEEN ? AND ?
					GROUP BY `coin`, `type`
				', [
					$date->startOfDay()->toDateTimeString(),
					$date->endOfDay()->toDateTimeString()
				]);
				$point = collect($point);
				return $point;
			});

			foreach (array_keys(trans('wallet.coin')) as $coin) {
				foreach (array_keys(trans('transaction.type')) as $type) {
					$item = $point->where('coin', $coin)->where('type', $type)->first();
					$data->push((object) [
						'date' => $date_string,
						'coin' => $coin,
						'type' => $type,
						'total_amount' => (float) @$item->total_amount
					]);
				}
			}
		}

		// 总收取手续费。
		$total_fee_btc = (float) Transaction::where('coin', Transaction::COIN_BTC)->sum('fee');
		$total_fee_ltc = (float) Transaction::where('coin', Transaction::COIN_LTC)->sum('fee');
		// 总存款比特币。
		$total_deposit_btc = (float) Transaction::where('coin', Transaction::COIN_BTC)->where('type', Transaction::TYPE_NETWORK_IN)->sum('amount');
		$total_deposit_ltc = (float) Transaction::where('coin', Transaction::COIN_LTC)->where('type', Transaction::TYPE_NETWORK_IN)->sum('amount');
		// 总提现比特币。
		$sold_withdraw_btc = (float) Transaction::where('coin', Transaction::COIN_BTC)->where('type', Transaction::TYPE_NETWORK_OUT)->sum('amount');
		$sold_withdraw_ltc = (float) Transaction::where('coin', Transaction::COIN_LTC)->where('type', Transaction::TYPE_NETWORK_OUT)->sum('amount');

		return view('root.statistics.flow')->with([
			'data' => $data,
			'date_range' => $this->date_start->toDateString() . ' ~ ' . $this->date_end->toDateString()
		])->with(compact('total_fee_btc', 'total_fee_ltc', 'total_deposit_btc', 'total_deposit_ltc', 'sold_withdraw_btc', 'sold_withdraw_ltc'));
	}
}