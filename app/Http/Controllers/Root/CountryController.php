<?php
namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;

/**
 * 国家信息
 *
 * @author  
 *
 */
class CountryController extends Controller
{

	/**
	 * 国家列表
	 */
	public function getList(Request $request)
	{
		// 取得数据模型。
		$model = Country::oldest('code');

		// 附加筛选条件。
		foreach ([
			'code',
			'area_code'
		] as $field) {
			if ($request->filled($field)) {
				$value = $request->input($field);
				$model->{is_array($value) ? 'whereIn' : 'where'}($field, $value);
			}
		}

		// 取得单页数据。
		$data = $model->paginate($request->cookie('limit', 15));

		// 附加翻页参数。
		$data->appends($request->all());

		return view('root.country.list', compact('data'));
	}
}
