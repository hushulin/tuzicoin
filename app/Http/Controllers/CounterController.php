<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCounter;
use App\Http\Requests\EditCounter;
use App\Models\PayInfo;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Counter;
use App\Models\Wallet;
use Auth, Lock;

/**
 * 柜台管理
 *
 * @author
 *
 */
class CounterController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * 柜台详情
     */
    public function show(Request $request)
    {
        $counter = Counter::find($request->id);
        if (is_null($counter)) {
            abort(404);
        }
        $data = [
          'counter'  => $counter,
          'pay_info' => []
        ];
        $user = \Auth::user();
        $pay_info = PayInfo::where('uid', '=', $counter->user_id)->first();
        $data['pay_info'] = [];
        if ($pay_info && $pay_info->pay_info) {
            $data['pay_info'] = json_decode($pay_info->pay_info, true);
        }

        return view('counter.show', $data);
    }

    /**
     * 显示创建柜台页面
     */
    public function showCreateForm()
    {
        $countries = Country::all();
        $currencies = Currency::all();
        return view('counter.create')->with(compact('countries', 'currencies'));
    }

    /**
     * 创建柜台
     */
    public function create(CreateCounter $request)
    {
        // 取得当前登录用户。
        $current_user = Auth::user();

        // 创建柜台。
        $counter = new Counter();
        $counter->fill($request->all());
        $counter->user()->associate($current_user);
        $counter->save();

        // 返回成功信息。
        return back()->withSuccess(__('Submitted successfully.'));
    }

    /**
     * 显示编辑柜台页面
     */
    public function showEditForm(Request $request)
    {
        $counter = Auth::user()->counters()->find($request->id);
        $countries = Country::all();
        $currencies = Currency::all();
        return view('counter.edit')->with(compact('countries', 'currencies', 'counter'));
    }

    /**
     * 编辑柜台
     */
    public function edit(EditCounter $request)
    {
        // 取得要编辑的柜台。
        $counter = Auth::user()->counters()->find($request->id);
        if (is_null($counter)) {
            abort(404);
        }

        // 保存编辑。
        $counter->fill($request->except('coin', 'type'));
        $counter->save();

        // 返回成功信息。
        return back()->withSuccess(__('Saved successfully.'));
    }

    /**
     * 柜台管理
     */
    public function list(Request $request)
    {
        // 限定请求参数。
        if (!in_array($request->type, [
          Counter::TYPE_ONLINE_BUY,
          Counter::TYPE_ONLINE_SELL
        ])) {
            $request->offsetSet('type', Counter::TYPE_ONLINE_BUY);
        }
        if (!in_array($request->status, [
          Counter::STATUS_OPEN,
          Counter::STATUS_CLOSED
        ])) {
            $request->offsetSet('status', Counter::STATUS_OPEN);
        }

        // 取得当前登录用户。
        $current_user = Auth::user();

        // 取得购买类型的柜台列表。
        $counters = $current_user->counters()->latest()->where('type', $request->type)->where('status',
          $request->status)->paginate();

        // 返回视图。
        return view('counter.list')->with(compact('counters'));
    }

    /**
     * 关闭柜台
     */
    public function closed(Request $request)
    {
        $counter = Auth::user()->counters()->find($request->id);
        if ($counter) {
            $counter->status = Counter::STATUS_CLOSED;
            $counter->save();
        }
        return back()->withSuccess(__('Operation is successfully.'));
    }

    /**
     * 开放柜台
     */
    public function open(Request $request)
    {
        $counter = Auth::user()->counters()->find($request->id);
        if ($counter) {
            $counter->status = Counter::STATUS_OPEN;
            $counter->save();
        }
        return back()->withSuccess(__('Operation is successfully.'));
    }

    /**
     * 关闭柜台
     */
    public function zd(Request $request)
    {
        $user    = Auth::user();
        $counter = $user->counters()->find($request->id);
        if ($counter) {
            try {
                $zdday = intval($request->zdday);
                $zdday = $zdday || 1;
                $time = time();
                $wallet = $user->wallet(Wallet::COIN_TUZI);
                if(!in_array($zdday,[1,3])) {
                    Lock::release('Counter:' . $counter->id);
                    return response()
                        ->json(['code'=>1,'msg'=>__('错误的参数'),'result'=>null], 200, [], JSON_UNESCAPED_UNICODE);
                }
                if($zdday==1) {
                    $amount = 50;
                } elseif ($zdday==3) {
                    $amount = 120;
                }
                if ($wallet->balance_available < $amount) {
                    Lock::release('Counter:' . $counter->id);
                    return response()
                        ->json(['code'=>1,'msg'=>__('TUZI币不足'),'result'=>null], 200, [], JSON_UNESCAPED_UNICODE);
                }
                $counter->zd_stime = date('Y-m-d H:i:s', $time);
                $counter->zd_etime = date('Y-m-d H:i:s', $time+24*3600*$zdday);
                $counter->save();
                $wallet->decrement('balance_available', $amount);
                Lock::release('Counter:' . $counter->id);
                return response()
                    ->json(['code'=>0,'msg'=>__('Operation is successfully.'),'result'=>null], 200, [], JSON_UNESCAPED_UNICODE);
            } finally {
                Lock::release('Counter:' . $counter->id);
            }
        }

    }
}
