<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SendMessage;
use App\Models\Message;
use App\Models\Order;
use Auth;

/**
 * 消息对话
 *
 * @author  
 *
 */
class MessageController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * 发送消息
	 */
	public function send(SendMessage $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得消息发送目标订单。
		$order = Order::byUser($current_user)->find($request->order_id);
		if (is_null($order)) {
			abort(403);
		}

		// 文本消息。
		if ($request->filled('message_text')) {
			$message = new Message();
			$message->order()->associate($order);
			$message->source()->associate($current_user);
			$message->type = Message::TYPE_TEXT;
			$message->content = $request->message_text;
			$message->save();
		}

		// 图片消息。
		if ($request->hasFile('message_image')) {
			$image = $request->file('message_image');
			list ($width, $height, $type) = getimagesize($image->path());
			$mime = image_type_to_mime_type($type);
			$message = new Message();
			$message->order()->associate($order);
			$message->source()->associate($current_user);
			$message->type = Message::TYPE_IMAGE;
			$message->content = $image->store('message');
			$message->extra = [
				'width' => $width,
				'height' => $height,
				'mime' => $mime
			];
			$message->save();
		}

		if ($request->wantsJson()) {
			$message->is_read = false;
			return $message;
		} else {
			return back();
		}
	}

	/**
	 * 标记消息为已读
	 */
	public function read(Request $request)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得要标记状态的消息。
		$message = Message::where('target_id', $current_user->id)->find($request->id);
		if (is_null($message)) {
			abort(404);
		}

		// 修改消息已读状态。
		$message->is_read = true;
		$message->save();
	}

	/**
	 * 显示消息内的图片
	 */
	public function showImage($message_id)
	{
		// 取得当前登录用户。
		$current_user = Auth::user();

		// 取得要查看图片的消息。
		$message = Message::byUser($current_user)->find($message_id);
		if (is_null($message)) {
			abort(404);
		}

		// 返回图片。
		return response()->file(storage_path('app/' . $message->content), [
			'Content-type' => $message->extra['mime']
		]);
	}
}
