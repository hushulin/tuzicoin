<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trust;
use Auth;

/**
 * 信任关系
 *
 * @author  
 *
 */
class TrustController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * 添加信任
	 */
	public function add(Request $request)
	{
		if (Auth::id() != $request->user_id) {
			Trust::updateOrCreate([
				'source_id' => Auth::id(),
				'target_id' => $request->user_id
			]);
		}
		return back();
	}

	/**
	 * 移除信任
	 */
	public function remove(Request $request)
	{
		$data = Auth::user()->trusts()->where('target_id', $request->user_id)->first();
		if ($data) {
			$data->delete();
		}
		return back();
	}

	/**
	 * 信任我的人
	 */
	public function by()
	{
		$data = Trust::where('target_id', Auth::id())->with('source')->paginate(10);
		return view('trust.by')->with(compact('data'));
	}

	/**
	 * 我信任的人
	 */
	public function my()
	{
		$data = Auth::user()->trusts()->with('target')->paginate(10);

		return view('trust.my')->with(compact('data'));
	}
}
