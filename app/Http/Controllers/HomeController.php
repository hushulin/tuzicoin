<?php

namespace App\Http\Controllers;

use App\Http\Middleware\SearchCounter;
use Illuminate\Http\Request;
use App\Models\Counter;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Article;
use DB;

/**
 * 公共页面
 *
 * @author
 *
 */
class HomeController extends Controller
{

    public function __construct()
    {
       // $this->middleware(SearchCounter::class)->only('buy', 'sell');
    }

    /**
     * 首页
     */
    public function index()
    {
        // 取两个购买与两个出售。
        $model = Counter::with('user');
        $model->where('status', Counter::STATUS_OPEN);
        $model->where('country_code', 'CN');
        $model->where('currency_code', 'CNY');
        $model->balanceEnough();
        $buys = (clone $model)->where('type', Counter::TYPE_ONLINE_SELL)
            ->orderBy(DB::raw('if(zd_stime<=now()&&now()<=zd_etime,datediff(zd_etime,zd_stime),0)'), 'desc')
            ->orderBy(DB::raw('if(zd_stime<=now()&&now()<=zd_etime,zd_stime,0)'), 'desc')
            ->orderBy('price', 'asc')
            ->orderBy('price', 'desc')
            ->limit(2)->get();
        $sells = (clone $model)->where('type', Counter::TYPE_ONLINE_BUY)
            ->orderBy(DB::raw('if(zd_stime<=now()&&now()<=zd_etime,datediff(zd_etime,zd_stime),0)'), 'desc')
            ->orderBy(DB::raw('if(zd_stime<=now()&&now()<=zd_etime,zd_stime,0)'), 'desc')
            ->orderBy('price', 'asc')
            ->orderBy('price', 'desc')
            ->limit(2)->get();
        $counters = $buys->merge($sells);

        // 取得最新公告。
        $notice = Article::where('type', '1')->latest()->first();

        // 返回视图。
        return view('home')->with(compact('counters', 'notice'));
    }

    /**
     * 购买
     */
    public function buy(Request $request, $coin = Counter::COIN_BTC)
    {
        return $this->search($request, $coin, Counter::TYPE_ONLINE_SELL);
    }

    /**
     * 出售
     */
    public function sell(Request $request, $coin = Counter::COIN_BTC)
    {
        return $this->search($request, $coin, Counter::TYPE_ONLINE_BUY);
    }

    /**
     * 搜索柜台
     */
    public function search(Request $request, $coin, $type)
    {
        $request->offsetSet('coin', $coin);

        // 取得数据。
        $model = Counter::orderBy('price', $type == Counter::TYPE_ONLINE_SELL ? 'asc' : 'desc');
        $model->with('user');
        $model->where('coin', $coin);
        $model->where('status', Counter::STATUS_OPEN);
        $model->where('type', $type);
        //$model->where('country_code', $request->country_code);
        if ($request->filled('currency_code')) {
            $model->where('currency_code', $request->currency_code);
        }
        if ($request->filled('payment_provider')) {
            $model->where('payment_provider', $request->payment_provider);
        }
        $model->balanceEnough();
        $counters = $model->paginate(20);

        // 国家与货币数据。
        $countries = Country::all();
        $currencies = Currency::all();

        // 返回列表视图。
        return view('search.counter')->with(compact('counters', 'countries', 'currencies'));
    }
}
