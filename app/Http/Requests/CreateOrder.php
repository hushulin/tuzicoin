<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;

/**
 * 创建订单
 *
 * @author  
 *
 */
class CreateOrder extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'counter_id' => [
				'required',
				Rule::exists('counters', 'id')->where(function ($query) {
					$query->where('user_id', '!=', Auth::id());
				})
			],
			'in' => 'in:amount,quantity',
			'amount' => 'required|numeric'
		];
	}
}
