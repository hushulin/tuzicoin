<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;

class BindMobile extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$current_user = Auth::user();
		return [
			'old_smsverify' => 'required|smsverify:' . $current_user->mobile . ',' . $current_user->country_code,
			'new_mobile' => [
				'required',
				Rule::phone()->countryField('country_code'),
				Rule::unique('users', 'mobile')->where(function ($query) {
					$query->where('country_code', $this->country_code);
				})
			],
			'new_smsverify' => 'required|smsverify:new_mobile,' . $current_user->country_code
		];
	}
}
