<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * 重置登录密码
 *
 * @author  
 *
 */
class ResetPassword extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'country_code' => 'required|exists:countries,code',
			'mobile' => [
				'required',
				Rule::phone()->countryField('country_code'),
				Rule::exists('users')->where(function ($query) {
					$query->where('country_code', $this->country_code);
				})
			],
			'smsverify' => 'required|smsverify',
			'new_password' => 'required|string|min:6'
		];
	}
}
