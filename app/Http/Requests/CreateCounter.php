<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Counter;

class CreateCounter extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules()
    {
        return [
            //'coin' => 'required|in:' . join(',', C('coins')),
            'coin_area' => 'required|in:' . join(',', C('coins')),
            'type' => 'required|in:' . join(',', [
                    Counter::TYPE_ONLINE_BUY,
                    Counter::TYPE_ONLINE_SELL
                ]),
            //'country_code' => 'required|exists:countries,code',
            //'currency_code' => 'required|exists:currencies,code',
            //'margin' => 'required|numeric|between:-99.99,99.99',
            'min_price' => 'nullable|integer',
            'min_amount' => 'required|integer|min:' . C('min_amount'),
            'max_amount' => 'required|integer|gt:min_amount',
            'payment_window_minutes' => [
                'required_if:type,' . Counter::TYPE_ONLINE_BUY,
                'nullable',
                'integer'
            ],
            'payment_provider' => 'required|in:' . join(',', array_keys(trans('counter.payment_provider'))),
            'message' => 'required|string|max:4096'
        ];
    }
}
