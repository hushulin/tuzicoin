<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * 发送消息
 *
 * @author  
 *
 */
class SendMessage extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'order_id' => 'required',
			'message_text' => 'required_without:message_image|max:1000',
			'message_image' => 'nullable|image|max:2048'
		];
	}
}
