<?php
namespace App\Http\Requests\Root;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use App\Models\Address;

/**
 * 导入钱包地址
 *
 * @author  
 *
 */
class AddressSave extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'coin' => 'required|in:' . join(',', [
				Address::COIN_BTC,
				Address::COIN_LTC
			]),
			'address_list' => 'required|array'
		];
	}

	public function validator(ValidationFactory $factory)
	{
		// 预处理收款地址列表。
		if ($this->filled('address_list') && ! is_array($this->address_list)) {
			$address_list = preg_split('/\r|\n/', $this->address_list);
			$address_list = array_unique($address_list);
			$address_list = array_filter($address_list);
			$this->merge([
				'address_list' => array_values($address_list)
			]);
		}

		$validator = parent::createDefaultValidator($factory);

		// 检查比特币地址格式。
		$validator->sometimes('address_list.*', 'base58check', function ($input) {
			return $input->coin === Address::COIN_BTC;
		});

		return $validator;
	}

	/**
	 * Get custom attributes for validator errors.
	 *
	 * @return array
	 */
	public function attributes()
	{
		return [
			'coin' => '币种',
			'address_list' => '钱包地址'
		];
	}
}
