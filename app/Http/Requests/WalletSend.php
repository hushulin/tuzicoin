<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use App\Models\Address;
use Auth;

/**
 * 发送比特币
 *
 * @author  
 *
 */
class WalletSend extends FormRequest
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'coin' => 'required|in:' . join(',', C('coins')),
			'address' => 'required',
			'quantity' => 'required|numeric|min:0.00000001',
			'secondary_password' => 'required|secondary_password',
			'remark' => 'nullable|string|max:255'
		];
	}

	public function validator(ValidationFactory $factory)
	{
		$validator = parent::createDefaultValidator($factory);

		// 检查比特币地址格式。
		$validator->sometimes('address', 'base58check', function ($input) {
			return $input->coin === Address::COIN_BTC;
		});

		// 若会员启用了两步验证，则需要验证两步验证代码。
		$validator->sometimes('google2fa', 'required|google2fa', function () {
			return Auth::user()->google2fa_secret;
		});

		return $validator;
	}
}
