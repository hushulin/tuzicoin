<?php
namespace App\Observers;

use App\Models\Deposit;
use App\Models\Transaction;

/**
 * 存款观察者
 *
 * @author  
 *
 */
class DepositObserver extends Observer
{

	public function saved(Deposit $model)
	{
		if ($model->isDirty('confirmations') && $model->confirmations > 0 && $model->getOriginal('confirmations') == 0) {

			// 创建交易。
            $transaction = new Transaction();
            if($transaction->where('txid','=',$model->txid)->count() == 0) {
                $transaction->user_id = $model->user_id;
                $transaction->coin = $model->coin;
                $transaction->amount = $model->amount;
                $transaction->type = Transaction::TYPE_NETWORK_IN; // 网络转入。
                $transaction->txid = $model->txid;
                $transaction->save();
            }
		}
	}
}