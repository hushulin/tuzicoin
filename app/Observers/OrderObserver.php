<?php
namespace App\Observers;

use App\Events\OrderCreated;
use App\Events\OrderStatusUpdated;
use App\Events\OrderFeedback;
use App\Models\Order;
use App\Models\Trust;
use App\Models\Ignore;
use Carbon\Carbon;

/**
 * 订单观察者
 *
 * @author  
 *
 */
class OrderObserver extends Observer
{

	public function created(Order $model)
	{
		// 增加买家与卖家之间关系的交易数。
		Trust::where(function ($query) use ($model) {
			$query->where('source_id', $model->buyer_id)->where('target_id', $model->seller_id);
		})->orWhere(function ($query) use ($model) {
			$query->where('source_id', $model->seller_id)->where('target_id', $model->buyer_id);
		})->increment('trade_count');
		Ignore::where(function ($query) use ($model) {
			$query->where('source_id', $model->buyer_id)->where('target_id', $model->seller_id);
		})->orWhere(function ($query) use ($model) {
			$query->where('source_id', $model->seller_id)->where('target_id', $model->buyer_id);
		})->increment('trade_count');

		// 订单创建事件。
		event(new OrderCreated($model));
	}

	public function saving(Order $model)
	{
		// 过期时间。
		if (empty($model->expires_at) || ($model->isDirty('status') && $model->status == Order::STATUS_WAIT_PAYMENT)) {
			$model->expires_at = Carbon::now()->addMinutes($model->counter->payment_window_minutes);
		}
	}

	public function updated(Order $model)
	{
		if ($model->isDirty('status')) {
			// 订单状态变更事件。
			event(new OrderStatusUpdated($model));
		}

		// 订单评价。
		if ($model->isDirty('buyer_feedback', 'seller_feedback')) {
			event(new OrderFeedback($model));
		}
	}
}