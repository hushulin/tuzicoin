<?php
namespace App\Observers;

use App\Models\Transaction;

/**
 * 交易观察者
 *
 * @author  
 *
 */
class TransactionObserver extends Observer
{

	public function creating(Transaction $model)
	{
		if ($model->wallet_id) {
			// 交易所属用户。
			$model->user_id = $model->wallet->user_id;
			// 交易币种。
			$model->coin = $model->wallet->coin;
		} else {
			// 交易所属钱包。
			$model->wallet()->associate($model->user->wallet($model->coin));
		}
	}

	public function created(Transaction $model)
	{
		switch ($model->type) {
			case Transaction::TYPE_NETWORK_IN: // 网络转入。
			case Transaction::TYPE_PLATFORM_IN: // 平台转入。

				// 增加可用余额。
				$model->wallet()->increment('balance_available', $model->amount + $model->fee);

				break;
			case Transaction::TYPE_PLATFORM_OUT: // 平台转出一定对应一笔平台转入。

				// 创建交易。
				$transaction = new Transaction();
				$transaction->user()->associate($model->target);
				$transaction->coin = $model->coin;
				$transaction->amount = $model->amount;
				$transaction->type = Transaction::TYPE_PLATFORM_IN; // 平台转入。
				$transaction->fee = 0; // 平台间转账免手续费。
				$transaction->target_id = $model->user_id;
				$transaction->relatedTransaction()->associate($model);
				$transaction->save();

				// 关联相关交易。
				$model->syncOriginal();
				$model->relatedTransaction()->associate($transaction);
				$model->save();

				break;
		}
	}
}