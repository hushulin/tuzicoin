<?php

namespace App\Observers;

use App\Models\Counter;

/**
 * 柜台观察者
 *
 * @author
 *
 */
class CounterObserver extends Observer
{
    public function saving(Counter $model)
    {
        if ($model->coin_area == 'TUZI') {
            $model->coin = $model::COIN_TUZI;
        }
        $model->country_code='CN';
        $model->currency_code='CNY';
    }

    public function saving_bakk(Counter $model)
    {

        if ($model->coin_area == 'tuzi') {
            $model->coin = $model::COIN_BTC;
        } elseif ($model->coin_area == 'shuzibi') {
            $model->coin = $model::COIN_LTC;
            $model->payment_provider = '';
            $model->min_amount='0';
            $model->max_amount='0';

        } elseif ($model->coin_area == 'yingjian') {
            $model->coin = 'tuzi';
            $model->payment_provider = '';
            $model->min_amount='0';
            $model->max_amount='0';
        }

        $model->country_code='CN';
        $model->currency_code='CNY';
        //var_dump($model->coin_area);exit();

        //		// 处理字段类型。
        //		$model->min_price = (int) $model->min_price;
        //
        //		// 处理不同类型柜台的付款期限。
        //		if ($model->type == Counter::TYPE_ONLINE_BUY) {
        //			$model->payment_window_minutes = (int) $model->payment_window_minutes;
        //		} else {
        //			$model->payment_window_minutes = 30;
        //		}
        //
        //		// 刷新价格。
        //		$price_func = strtolower($model->coin) . '_usd';
        //		$model->price = $price_func();
        //		$model->price *= $model->exchangeRate->price ?? 0; // 根据汇率取得柜台使用货币的比特币价格。
        //		$model->price *= 1 + $model->margin / 100; // 计算溢价后的价格。
        //		// 价格不可低于最低价。
        //		if ($model->min_price > 0 && $model->price < $model->min_price) {
        //			$model->price = $model->min_price;
        //		}
    }
}