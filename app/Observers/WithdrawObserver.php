<?php
namespace App\Observers;

use App\Models\Withdraw;
use App\Models\Transaction;

/**
 * 提现单观察者
 *
 * @author  
 *
 */
class WithdrawObserver extends Observer
{

	public function creating(Withdraw $model)
	{
		if ($model->wallet_id) {
			// 交易所属用户。
			$model->user_id = $model->wallet->user_id;
			// 交易币种。
			$model->coin = $model->wallet->coin;
		} else {
			// 交易所属钱包。
			$model->wallet()->associate($model->user->wallet($model->coin));
		}
	}

	public function updated(Withdraw $model)
	{
		if ($model->isDirty('status')) {
			switch ($model->status) {
				case Withdraw::STATUS_SUCCESS: // 提现成功。

					// 扣除冻结余额。
					$model->wallet()->decrement('balance_locked', $model->amount + $model->fee);

					// 创建交易。
					$transaction = new Transaction();
					$transaction->wallet()->associate($model->wallet);
					$transaction->amount = $model->amount;
					$transaction->address = $model->address; // 转出目标地址。
					$transaction->fee = $model->fee; // 网络转出手续费。
					$transaction->type = Transaction::TYPE_NETWORK_OUT; // 网络转出。
					$transaction->remark = $model->remark;

					// 保存交易。
					$transaction->save();

					break;
				case Withdraw::STATUS_FAILURE: // 提现失败。

					// 返还冻结余额。
					$model->wallet()->decrement('balance_locked', $model->amount + $model->fee);
					$model->wallet()->increment('balance_available', $model->amount + $model->fee);

					break;
			}
		}
	}
}