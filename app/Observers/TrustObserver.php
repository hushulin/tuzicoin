<?php
namespace App\Observers;

use App\Models\Trust;
use App\Models\Order;

/**
 * 信任观察者
 *
 * @author  
 *
 */
class TrustObserver extends Observer
{

	public function creating(Trust $model)
	{
		// 取得交易次数。
		$model->trade_count = Order::where(function ($query) use ($model) {
			$query->where('buyer_id', $model->source_id)->where('seller_id', $model->target_id);
		})->orWhere(function ($query) use ($model) {
			$query->where('buyer_id', $model->target_id)->where('seller_id', $model->source_id);
		})->count();
	}

	public function created(Trust $model)
	{
		// 增加信任计数。
		$model->target()->increment('trust_count');
	}

	public function deleted(Trust $model)
	{
		// 减少信任计数。
		$model->target()->decrement('trust_count');
	}
}