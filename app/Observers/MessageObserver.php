<?php
namespace App\Observers;

use App\Models\Message;
use App\Events\MessageCreated;
use App\Notifications\MessageHaveRead;

/**
 * 消息观察者
 *
 * @author  
 *
 */
class MessageObserver extends Observer
{

	public function created(Message $model)
	{
		$broadcast = broadcast(new MessageCreated($model));
		if ($model->type != Message::TYPE_SYSTEM) {
			$broadcast->toOthers();
		}
	}

	public function creating(Message $model)
	{
		// 根据订单补全消息目标人信息。
		if (empty($model->target_id)) {
			if ($model->source_id != $model->order->buyer_id) {
				$model->target_id = $model->order->buyer_id;
			} else {
				$model->target_id = $model->order->seller_id;
			}
		}
	}

	public function updated(Message $model)
	{
		if ($model->type != Message::TYPE_SYSTEM && $model->isDirty('is_read') && $model->is_read) {
			// 通知发送人对方已读。
			$model->source->notify(new MessageHaveRead($model));
		}
	}
}