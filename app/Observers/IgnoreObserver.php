<?php
namespace App\Observers;

use App\Models\Ignore;
use App\Models\Order;

/**
 * 黑名单观察者
 *
 * @author  
 *
 */
class IgnoreObserver extends Observer
{

	public function creating(Ignore $model)
	{
		// 取得交易次数。
		$model->trade_count = Order::where(function ($query) use ($model) {
			$query->where('buyer_id', $model->source_id)->where('seller_id', $model->target_id);
		})->orWhere(function ($query) use ($model) {
			$query->where('buyer_id', $model->target_id)->where('seller_id', $model->source_id);
		})->count();
	}
}