<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

	/**
	 * 币种：比特币
	 */
	const COIN_BTC = 'BTC';

	/**
	 * 币种：莱特币
	 */
	const COIN_LTC = 'LTC';

	protected $fillable = [
		'coin',
		'address'
	];

	/**
	 * 所属用户
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
