<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\TrustObserver;

/**
 * 信任关系
 *
 * @author  
 *
 */
class Trust extends Model
{

	protected $fillable = [
		'source_id',
		'target_id'
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new TrustObserver());
	}

	public function source()
	{
		return $this->belongsTo(User::class);
	}

	public function target()
	{
		return $this->belongsTo(User::class);
	}
}
