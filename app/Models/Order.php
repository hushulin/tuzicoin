<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\OrderObserver;

/**
 * 订单
 *
 * @author
 *
 */
class Order extends Model
{

	/**
	 * 状态：待打款
	 */
	const STATUS_WAIT_PAYMENT = 'WaitPayment';

	/**
	 * 状态：未确认
	 */
	const STATUS_UNCONFIRMED = 'Unconfirmed';

	/**
	 * 状态：已确认
	 */
	const STATUS_CONFIRMED = 'Confirmed';

	/**
	 * 状态：已撤销
	 */
	const STATUS_REVOKED = 'Revoked';

	/**
	 * 状态：已过期
	 */
	const STATUS_EXPIRED = 'Expired';

	/**
	 * 评论：好评
	 */
	const FEEDBACK_POSITIVE = 'Positive';

	/**
	 * 评论：中评
	 */
	const FEEDBACK_NEUTRAL = 'Neutral';

	/**
	 * 评论：差评
	 */
	const FEEDBACK_NEGATIVE = 'Negative';

	protected $casts = [
		'expires_at' => 'datetime',
		'buyer_id' => 'integer',
		'seller_id' => 'integer'
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new OrderObserver());
	}

	/**
	 * 柜台
	 */
	public function counter()
	{
		return $this->belongsTo(Counter::class);
	}

	/**
	 * 买家
	 */
	public function buyer()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 卖家
	 */
	public function seller()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 消息列表
	 */
	public function messages()
	{
		return $this->hasMany(Message::class);
	}

	/**
	 * 所属用户的订单
	 */
	public function scopeByUser($query, $user)
	{
		return $query->where('buyer_id', $user->id)->orWhere('seller_id', $user->id);
	}

	/**
	 * 进行中的交易，包含未评价。
	 */
	public function scopeProcessing($query, $user)
	{
		return $query->where(function ($query) use ($user) {
			$query->whereIn('status', [
				Order::STATUS_WAIT_PAYMENT,
				Order::STATUS_UNCONFIRMED
			]);
			$query->orWhere('status', Order::STATUS_CONFIRMED)->where(function ($query) use ($user) {
				$query->where('buyer_id', $user->id)->whereNull('buyer_feedback');
				$query->orWhere('seller_id', $user->id)->whereNull('seller_feedback');
			});
		});
	}

	/**
	 * 已完成的交易，包含已评价。
	 */
	public function scopeCompleted($query, $user)
	{
		return $query->where(function ($query) use ($user) {
			$query->whereIn('status', [
				Order::STATUS_REVOKED,
				Order::STATUS_EXPIRED
			]);
			$query->orWhere('status', Order::STATUS_CONFIRMED)->where(function ($query) use ($user) {
				$query->where('buyer_id', $user->id)->whereNotNull('buyer_feedback');
				$query->orWhere('seller_id', $user->id)->whereNotNull('seller_feedback');
			});
		});
	}
}
