<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\MessageObserver;

/**
 * 消息
 *
 * @author  
 *
 */
class Message extends Model
{

	/**
	 * 类型：文本
	 */
	const TYPE_TEXT = 'Text';

	/**
	 * 类型：图片
	 */
	const TYPE_IMAGE = 'Image';

	/**
	 * 类型：系统
	 */
	const TYPE_SYSTEM = 'System';

	protected $casts = [
		'is_read' => 'boolean',
		'extra' => 'collection'
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new MessageObserver());
	}

	public function order()
	{
		return $this->belongsTo(Order::class);
	}

	public function source()
	{
		return $this->belongsTo(User::class);
	}

	public function target()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 所属用户的消息
	 */
	public function scopeByUser($query, $user)
	{
		return $query->where('source_id', $user->id)->orWhere('target_id', $user->id);
	}
}
