<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\CounterObserver;

/**
 * 柜台
 *
 * @author  
 *
 */
class Counter extends Model
{
    /**
     * 币种：兔子币
     */
    const COIN_TUZI = 'TUZI';

	/**
	 * 币种：比特币
	 */
	const COIN_BTC = 'BTC';

	/**
	 * 币种：莱特币
	 */
	const COIN_LTC = 'LTC';

	/**
	 * 状态：开放
	 */
	const STATUS_OPEN = 'Open';

	/**
	 * 状态：关闭
	 */
	const STATUS_CLOSED = 'Closed';

	/**
	 * 类型：在线购买
	 */
	const TYPE_ONLINE_BUY = 'OnlineBuy';

	/**
	 * 类型：在线出售
	 */
	const TYPE_ONLINE_SELL = 'OnlineSell';

	/**
	 * 付款方式：现金存款
	 */
	const PAYMENT_PROVIDER_CASH_DEPOSIT = 'CashDeposit';

	/**
	 * 付款方式：银行转账
	 */
	const PAYMENT_PROVIDER_NATIONAL_BANK = 'NationalBank';

	/**
	 * 付款方式：支付宝
	 */
	const PAYMENT_PROVIDER_ALIPAY = 'Alipay';

	/**
	 * 付款方式：微信支付
	 */
	const PAYMENT_PROVIDER_WECHAT_PAY = 'WechatPay';

	/**
	 * 付款方式：iTunes礼品卡
	 */
	const PAYMENT_PROVIDER_ITUNES_GIFT_CARD = 'iTunesGiftCard';

	/**
	 * 付款方式：Paytm
	 */
	const PAYMENT_PROVIDER_PAYTM = 'Paytm';

	/**
	 * 付款方式：其它
	 */
	const PAYMENT_PROVIDER_OTHER = 'Other';

	protected $fillable = [
		'coin',
		'type',
		'country_code',
		'currency_code',
		'payment_provider',
		'price',
		'margin',
		'min_price',
		'min_amount',
		'max_amount',
		'payment_window_minutes',
		'message',
    'title',
    'coin_area',
    'qibao',
    'contact',
    'shou_huo_address',
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new CounterObserver());
	}

	/**
	 * 所属会员
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 货币与美元的汇率
	 */
	public function exchangeRate()
	{
		return $this->belongsTo(ExchangeRate::class, 'currency_code');
	}

	/**
	 * 只有钱包可用余额达充足。
	 */
	public function scopeBalanceEnough($query)
	{
		$query->whereExists(function ($query) {
			$query->select('id')->from('wallets')->whereRaw('`wallets`.`user_id` = `counters`.`user_id`')->whereRaw('`wallets`.`coin` = `counters`.`coin`');
			$query->where(function ($query) {
				$query->where('wallets.coin', Counter::COIN_BTC)->where('wallets.balance_available', '>=', C('min_show_amount_btc'));
				$query->orWhere('wallets.coin', Counter::COIN_LTC)->where('wallets.balance_available', '>=', C('min_show_amount_ltc'));
                $query->orWhere('wallets.coin', Counter::COIN_TUZI)->where('wallets.balance_available', '>=', C('min_show_amount_tuzi'));
			});
		});
		return $query;
	}
}
