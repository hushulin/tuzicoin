<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 市场价
 *
 * @author  
 *
 */
class MarketPrice extends Model
{

	protected $fillable = [
		'market',
		'coin',
		'currency',
		'last',
		'buy',
		'sell'
	];
}
