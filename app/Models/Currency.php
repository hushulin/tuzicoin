<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 货币
 *
 * @author  
 *
 */
class Currency extends Model
{

	protected $primaryKey = 'code';

	protected $keyType = 'string';

	public $incrementing = false;

	public $timestamps = false;

	public function getNameAttribute()
	{
		$language = session()->get('language');
		$field = strtolower('name_' . $language);
		foreach ($this->attributes as $attribute => $value) {
			if (strtolower($attribute) == $field) {
				return $value;
			}
		}
	}
}
