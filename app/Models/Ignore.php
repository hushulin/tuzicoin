<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\IgnoreObserver;

/**
 * 黑名单
 *
 * @author  
 *
 */
class Ignore extends Model
{

	protected $fillable = [
		'source_id',
		'target_id'
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new IgnoreObserver());
	}

	public function source()
	{
		return $this->belongsTo(User::class);
	}

	public function target()
	{
		return $this->belongsTo(User::class);
	}
}
