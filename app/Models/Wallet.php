<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 钱包
 *
 * @author  
 *
 */
class Wallet extends Model
{
    /**
     * 币种：兔子币
     */
    const COIN_TUZI = 'TUZI';

	/**
	 * 币种：比特币
	 */
	const COIN_BTC = 'BTC';

	/**
	 * 币种：莱特币
	 */
	const COIN_LTC = 'LTC';

	/**
	 * 所属用户
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 总资产
	 */
	public function getBalanceTotalAttribute()
	{
		return @$this->attributes['balance_available'] + @$this->attributes['balance_locked'];
	}
}
