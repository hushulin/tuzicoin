<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Observers\PasswordEncryptionObserver;
use Lock;

/**
 * 用户
 *
 * @author  
 *
 */
class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [
		'username',
		'country_code',
		'mobile',
		'email',
		'password',
		'mobile_verified'
	];

	protected $visible = [
		'id',
		'username',
		'country_code',
		'authentication_id',
		'email_verified',
		'mobile_verified',
		'trust_count',
		'trade_count',
		'rating',
		'vague_volume',
		'avatar',
		'online'
	];

	protected $casts = [
		'email_verified' => 'boolean',
		'mobile_verified' => 'boolean',
		'avatar' => 'array',
		'register_ips' => 'collection'
	];

	protected $appends = [
		'online'
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new PasswordEncryptionObserver());
	}

	/**
	 * 短信频道的路由通知
	 *
	 * @return string
	 */
	public function routeNotificationForSms()
	{
		return $this->country->area_code . $this->mobile;
	}

	/**
	 * 用户接收的通知广播
	 *
	 * @return array
	 */
	public function receivesBroadcastNotificationsOn()
	{
		return 'user.' . $this->getKey();
	}

	/**
	 * 钱包列表
	 */
	public function wallets()
	{
		return $this->hasMany(Wallet::class);
	}

	/**
	 * 取得指定币种的钱包
	 *
	 * @return \App\Models\Wallet
	 */
	public function scopeWallet($query, $coin)
	{
		static $cache = [];
		$key = $coin . '-' . $this->getKey();
		if (! array_key_exists($key, $cache)) {
			// 取得钱包。
			$lock_key = 'User@wallet:' . $this->getKey();
			try {
				Lock::acquire($lock_key);
				$wallet = $this->wallets()->where('coin', $coin)->first();
				// 若没有钱包则创建。
				if (is_null($wallet)) {
					$wallet = new Wallet();
					$wallet->user()->associate($this);
					$wallet->coin = $coin;
					$wallet->save();
				}
			} finally {
				Lock::release($lock_key);
			}
			$cache[$key] = $wallet;
		}
		return $cache[$key];
	}

	/**
	 * 钱包地址列表
	 */
	public function addresses()
	{
		return $this->hasMany(Address::class);
	}

	/**
	 * 取得指定钱包地址
	 *
	 * @return \App\Models\Address
	 */
	public function scopeAddress($query, $coin)
	{
		static $cache = [];
		$key = $coin . '-' . $this->getKey();
		if (! array_key_exists($key, $cache)) {
			// 取得地址。
			$lock_key = 'User@address:' . $this->getKey();
			try {
				Lock::acquire($lock_key);
				$address = $this->addresses()->where('coin', $coin)->latest(static::UPDATED_AT)->first();
				// 若没有地址则分配一个。
				if (is_null($address)) {
					Lock::granule('Address@alloc', function () use (& $address, $coin) {
						$address = Address::where('user_id', 0)->where('coin', $coin)->oldest('id')->first();
						if (! is_null($address)) {
							$address->user()->associate($this);
							$address->save();
						}
					});
				}
			} finally {
				Lock::release($lock_key);
			}
			$cache[$key] = $address;
		}
		return $cache[$key];
	}

	/**
	 * 所属国家
	 */
	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	/**
	 * 身份验证信息
	 */
	public function authentication()
	{
		return $this->belongsTo(Authentication::class);
	}

	/**
	 * 交易记录
	 */
	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}

	/**
	 * 提现列表
	 */
	public function withdraws()
	{
		return $this->hasMany(Withdraw::class);
	}

	/**
	 * 存款列表
	 */
	public function deposits()
	{
		return $this->hasMany(Deposit::class);
	}

	/**
	 * 柜台列表
	 */
	public function counters()
	{
		return $this->hasMany(Counter::class);
	}

	/**
	 * 登陆日志
	 */
	public function userLoginLogs()
	{
		return $this->hasMany(UserLoginLog::class);
	}

	/**
	 * 信任列表
	 */
	public function trusts()
	{
		return $this->hasMany(Trust::class, 'source_id');
	}

	/**
	 * 黑名单
	 */
	public function ignores()
	{
		return $this->hasMany(Ignore::class, 'source_id');
	}

	/**
	 * 是否在线
	 */
	public function getOnlineAttribute()
	{
		return array_search($this->getKey(), online_users()) !== false;
	}

	/**
	 * 累计交易次数
	 */
	public function getTradeCountAttribute()
	{
		return @$this->attributes['buy_trade_count'] + @$this->attributes['sell_trade_count'];
	}

	/**
	 * 累计交易量（BTC）
	 */
	public function getVolumeBtcAttribute()
	{
		return @$this->attributes['buy_volume_btc'] + @$this->attributes['sell_volume_btc'];
	}

	/**
	 * 累计交易量（LTC）
	 */
	public function getVolumeLtcAttribute()
	{
		return @$this->attributes['buy_volume_ltc'] + @$this->attributes['sell_volume_ltc'];
	}

	/**
	 * 模糊的累计交易量（BTC）
	 */
	public function getVagueVolumeBtcAttribute()
	{
		// 暂定12个级别。
		if ($this->volume_btc < 0.5) {
			return '0-0.5';
		} elseif ($this->volume_btc < 1) {
			return '0.5-1';
		} elseif ($this->volume_btc < 2) {
			return '1-2';
		} elseif ($this->volume_btc < 5) {
			return '2-5';
		} elseif ($this->volume_btc < 10) {
			return '5-10';
		} elseif ($this->volume_btc < 20) {
			return '10-20';
		} elseif ($this->volume_btc < 50) {
			return '20-50';
		} elseif ($this->volume_btc < 100) {
			return '50-100';
		} elseif ($this->volume_btc < 200) {
			return '100-200';
		} elseif ($this->volume_btc < 500) {
			return '200-500';
		} elseif ($this->volume_btc < 1000) {
			return '500-1000';
		} else {
			return '1000+';
		}
	}

	/**
	 * 模糊的累计交易量（LTC）
	 */
	public function getVagueVolumeLtcAttribute()
	{
		// 暂定12个级别。
		if ($this->volume_ltc < 0.5) {
			return '0-0.5';
		} elseif ($this->volume_ltc < 1) {
			return '0.5-1';
		} elseif ($this->volume_ltc < 2) {
			return '1-2';
		} elseif ($this->volume_ltc < 5) {
			return '2-5';
		} elseif ($this->volume_ltc < 10) {
			return '5-10';
		} elseif ($this->volume_ltc < 20) {
			return '10-20';
		} elseif ($this->volume_ltc < 50) {
			return '20-50';
		} elseif ($this->volume_ltc < 100) {
			return '50-100';
		} elseif ($this->volume_ltc < 200) {
			return '100-200';
		} elseif ($this->volume_ltc < 500) {
			return '200-500';
		} elseif ($this->volume_ltc < 1000) {
			return '500-1000';
		} else {
			return '1000+';
		}
	}

	/**
	 * 好评率
	 */
	public function getRatingAttribute()
	{
		$total_feedback_count = @$this->attributes['positive_feedback_count'] + @$this->attributes['neutral_feedback_count'] + @$this->attributes['negative_feedback_count'];
		if ($total_feedback_count > 0) {
			return (int) ceil(@$this->attributes['positive_feedback_count'] / $total_feedback_count * 100);
		}
		return 0;
	}

	/**
	 * 隐藏非自己的手机号
	 */
/*	public function getMobileAttribute()
	{
		if (\Auth::guard('web')->id() == $this->id) {
			return @$this->attributes['mobile'];
		}
		return preg_replace('/\d{4}$/i', '****', @$this->attributes['mobile']);
	}*/
}
