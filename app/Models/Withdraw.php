<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\WithdrawObserver;

/**
 * 提现单
 *
 * @author  
 *
 */
class Withdraw extends Model
{

	/**
	 * 币种：比特币
	 */
	const COIN_BTC = 'BTC';

	/**
	 * 币种：莱特币
	 */
	const COIN_LTC = 'LTC';

	/**
	 * 状态：待处理
	 */
	const STATUS_PENDING = 'Pending';

	/**
	 * 状态：成功
	 */
	const STATUS_SUCCESS = 'Success';

	/**
	 * 状态：失败
	 */
	const STATUS_FAILURE = 'Failure';

	public static function boot()
	{
		parent::boot();

		static::observe(new WithdrawObserver());
	}

	/**
	 * 所属用户
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 所属钱包
	 */
	public function wallet()
	{
		return $this->belongsTo(Wallet::class);
	}

	/**
	 * 提现成功产生的交易
	 */
	public function transaction()
	{
		return $this->belongsTo(Transaction::class);
	}
}
