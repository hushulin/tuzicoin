<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\DepositObserver;

/**
 * 存款单
 *
 * @author  
 *
 */
class Deposit extends Model
{

	/**
	 * 币种：比特币
	 */
	const COIN_BTC = 'BTC';

	/**
	 * 币种：莱特币
	 */
	const COIN_LTC = 'LTC';

	protected $fillable = [
		'coin',
		'txid'
	];

	public static function boot()
	{
		parent::boot();

		static::observe(new DepositObserver());
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
