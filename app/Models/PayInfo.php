<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayInfo extends Model
{
    //
    protected $table = 'user_pay_info';
    public $timestamps = false;
    protected $fillable = ['pay_info'];
}
