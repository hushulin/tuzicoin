<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 法币汇率
 *
 * @author  
 *
 */
class ExchangeRate extends Model
{

	protected $primaryKey = 'name';

	protected $keyType = 'string';

	public $incrementing = false;

	protected $fillable = [
		'name',
		'price'
	];
}
