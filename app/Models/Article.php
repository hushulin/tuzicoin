<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 文章
 *
 * @author  
 *
 */
class Article extends Model
{

	public function admin()
	{
		return $this->belongsTo(Admin::class);
	}

	public function getTitleAttribute()
	{
		$language = session()->get('language');
		$field = strtolower('title_' . $language);
		foreach ($this->attributes as $attribute => $value) {
			if (strtolower($attribute) == $field && ! empty($value)) {
				return $value;
			}
		}
		foreach ([
			'en',
			'zh-CN',
			'zh-HK'
		] as $language) {
			$value = $this->getAttribute('title_' . $language);
			if (! empty($value)) {
				return $value;
			}
		}
	}

	public function getContentAttribute()
	{
		$language = session()->get('language');
		$field = strtolower('content_' . $language);
		foreach ($this->attributes as $attribute => $value) {
			if (strtolower($attribute) == $field && ! empty($value)) {
				return $value;
			}
		}
		foreach ([
			'en',
			'zh-CN',
			'zh-HK'
		] as $language) {
			$value = $this->getAttribute('content_' . $language);
			if (! empty($value)) {
				return $value;
			}
		}
	}
}
