<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Observers\TransactionObserver;

/**
 * 交易记录
 *
 * @author  
 *
 */
class Transaction extends Model
{

	/**
	 * 币种：比特币
	 */
	const COIN_BTC = 'BTC';

	/**
	 * 币种：莱特币
	 */
	const COIN_LTC = 'LTC';

	/**
	 * 类型：网络转入
	 */
	const TYPE_NETWORK_IN = 'NetworkIn';

	/**
	 * 类型：网络转出
	 */
	const TYPE_NETWORK_OUT = 'NetworkOut';

	/**
	 * 类型：交易买入
	 */
	const TYPE_TRADE_IN = 'TradeIn';

	/**
	 * 类型：交易卖出
	 */
	const TYPE_TRADE_OUT = 'TradeOut';

	/**
	 * 类型：平台转入
	 */
	const TYPE_PLATFORM_IN = 'PlatformIn';

	/**
	 * 类型：平台转出
	 */
	const TYPE_PLATFORM_OUT = 'PlatformOut';

	public static function boot()
	{
		parent::boot();

		static::observe(new TransactionObserver());
	}

	/**
	 * 所属用户
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 所属钱包
	 */
	public function wallet()
	{
		return $this->belongsTo(Wallet::class);
	}

	/**
	 * 目标用户
	 */
	public function target()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 相关交易（平台转出对应平台转入）
	 */
	public function relatedTransaction()
	{
		return $this->belongsTo(Transaction::class);
	}
}
