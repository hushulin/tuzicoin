<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * 钱包使用 Blockchain 提供服务
 *
 * @author  
 *
 */
class WalletServiceProvider extends ServiceProvider
{

	protected $defer = true;

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
