<?php
namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * 注册事件
 *
 * @author
 *
 */
class EventServiceProvider extends ServiceProvider
{

	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'Illuminate\Auth\Events\Registered' => [
			'App\Listeners\LogRegisteredUser'
		],
		'Illuminate\Auth\Events\Login' => [
			'App\Listeners\LogSuccessfulLogin'
		],
		'App\Events\OrderCreated' => [
			'App\Listeners\NotifyOrderCreated',
			'App\Listeners\OrderCreatedMessage'
		],
		'App\Events\OrderStatusUpdated' => [
			'App\Listeners\OrderStatusUpdatedMessage',
			'App\Listeners\OrderStatusConfirmed',
			'App\Listeners\OrderStatusRevoked',
			//'App\Listeners\NotifyOrderPaid'
		],
		'App\Events\OrderFeedback' => [
			'App\Listeners\UserFeedbackRating',
			'App\Listeners\OrderFeedbackMessage'
		],
		'App\Events\MessageCreated' => [
			'App\Listeners\NotifyMessageCreated'
		]
	];

	/**
     * 要注册的订阅者类.
     *
     * @var array
     */
    protected $subscribe = [
         'App\Listeners\BizWorkFlow',
    ];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();

		//
	}
}
