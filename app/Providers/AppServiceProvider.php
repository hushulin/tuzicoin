<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Validator\CustomValidator;
use Validator;

/**
 * 注册自定义验证器
 *
 * @author  
 *
 */
class AppServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
        // 注册自定义验证器扩展。
		Validator::resolver(function ($translator, $data, $rules, $messages, $customAttributes) {
			return new CustomValidator($translator, $data, $rules, $messages, $customAttributes);
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
