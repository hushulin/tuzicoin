<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;

class MessageHaveRead extends Notification implements ShouldQueue
{
	use Queueable;

	protected $messages;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($messages)
	{
		if ($messages instanceof Collection || $messages instanceof EloquentCollection) {
			$this->messages = $messages;
		} elseif (is_array($messages)) {
			$this->messages = collect($messages);
		} else {
			$this->messages = collect([
				$messages
			]);
		}
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return [
			'broadcast'
		];
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return BroadcastMessage
	 */
	public function toBroadcast($notifiable)
	{
		return new BroadcastMessage([
			'ids' => $this->messages->pluck('id')
		]);
	}

	/**
	 * Get the channels the event should broadcast on.
	 *
	 * @return array
	 */
	public function broadcastOn()
	{
		$channels = [];
		foreach ($this->messages->pluck('order_id')->unique() as $order_id) {
			$channels[] = new PrivateChannel('order.' . $order_id);
		}
		return $channels;
	}
}
