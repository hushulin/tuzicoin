<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Notifications\Messages\MailMessage;
use App\Channels\Messages\SmsMessage;
use App\Channels\AliyunSmsChannel;
use App\Channels\SmsChineseChannel;
use App\Models\Order;

class OrderCreated extends Notification implements ShouldQueue
{
	use Queueable;

	protected $order;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return [
			//AliyunSmsChannel::class
			SmsChineseChannel::class
		];
	}

	/**
	 * 获取通知的国内短信展示方式
	 *
	 * @param  mixed  $notifiable
	 * @return \App\Channels\Messages\VerificationMessage
	 */
	public function toSms($notifiable)
	{
		return new SmsMessage('notifications.order.created', [
			'order_id' => $this->order->id,
			'amount' => floatval($this->order->amount) . ' ' . $this->order->counter->currency_code
		]);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [];
	}
}
