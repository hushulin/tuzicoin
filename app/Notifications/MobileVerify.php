<?php
namespace App\Notifications;

use Illuminate\Notifications\Notification;
use App\Channels\AliyunSmsChannel;
use App\Channels\RuantongChannel;
use App\Channels\Messages\SmsMessage;

/**
 * 手机验证
 *
 * @author  
 *
 */
class MobileVerify extends Notification
{

	protected $code;

	protected $minutes;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct($code, $minutes = 5)
	{
		$this->code = $code;
		$this->minutes = $minutes;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return [
			AliyunSmsChannel::class
            //RuantongChannel::class
		];
	}

	/**
	 * 获取通知的国内短信展示方式
	 *
	 * @param  mixed  $notifiable
	 * @return \App\Channels\Messages\VerificationMessage
	 */
	public function toSms($notifiable)
	{
		return new SmsMessage('notifications.verify', [
			'code' => $this->code,
			'minutes' => $this->minutes
		]);
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [];
	}
}
