<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Models\Message;

class NewMessage extends Notification implements ShouldQueue
{
	use Queueable;

	protected $message;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Message $message)
	{
		$this->message = $message;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return [
			'broadcast'
		];
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return BroadcastMessage
	 */
	public function toBroadcast($notifiable)
	{
		$title = $this->message->source_id ? $this->message->source->username : '';
		$content = $this->message->content;
		switch ($this->message->type) {
			case Message::TYPE_SYSTEM:
				$title = __($this->message->extra['title']);
				$content = __($content);
				break;
			case Message::TYPE_IMAGE:
				$content = '[' . __('Image') . ']';
				break;
		}

		return new BroadcastMessage([
			'title' => $title,
			'content' => $content,
			'icon' => '',
			'url' => route('order.detail', [
				'id' => $this->message->order_id
			])
		]);
	}
}
