<?php
namespace App\Listeners;

use App\Events\MessageCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\NewMessage;
use App\Models\Message;
use Auth;

class NotifyMessageCreated
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  MessageCreated  $event
	 * @return void
	 */
	public function handle(MessageCreated $event)
	{
		$message = $event->message;
		if ($message->target_id != Auth::id()) {
			//$message->target->notify(new NewMessage($message));
		}
	}
}
