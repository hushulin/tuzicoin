<?php
namespace App\Listeners;

use App\Events\OrderFeedback;
use App\Models\Order;

/**
 * 处理订单中的用户评论
 *
 * @author  
 *
 */
class UserFeedbackRating
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderFeedback  $event
	 * @return void
	 */
	public function handle(OrderFeedback $event)
	{
		$order = $event->order;

		// 买家评论。
		if ($order->isDirty('buyer_feedback')) {
			switch ($order->buyer_feedback) {
				case Order::FEEDBACK_POSITIVE:
					$order->seller()->increment('positive_feedback_count');
					break;
				case Order::FEEDBACK_NEUTRAL:
					$order->seller()->increment('neutral_feedback_count');
					break;
				case Order::FEEDBACK_NEGATIVE:
					$order->seller()->increment('negative_feedback_count');
					break;
			}
		}
		// 卖家评论。
		if ($order->isDirty('seller_feedback')) {
			switch ($order->seller_feedback) {
				case Order::FEEDBACK_POSITIVE:
					$order->buyer()->increment('positive_feedback_count');
					break;
				case Order::FEEDBACK_NEUTRAL:
					$order->buyer()->increment('neutral_feedback_count');
					break;
				case Order::FEEDBACK_NEGATIVE:
					$order->buyer()->increment('negative_feedback_count');
					break;
			}
		}
	}
}
