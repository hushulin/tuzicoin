<?php
namespace App\Listeners;

use App\Events\OrderCreated;
use App\Models\Message;

class OrderCreatedMessage
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderCreated  $event
	 * @return void
	 */
	public function handle(OrderCreated $event)
	{
		$order = $event->order;

		//  发送订单创建系统消息。

		// 发给买家。
		$message = new Message();
		$message->order()->associate($order);
		$message->source_id = 0;
		$message->target_id = $message->order->buyer_id;
		$message->type = Message::TYPE_SYSTEM;
		$message->content = 'The trade has been created, waiting for buyer to pay.';
		$message->extra = [
			'title' => 'The trade has been created'
		];
		$message->save();

		// 发给卖家。
		$message = new Message();
		$message->order()->associate($order);
		$message->source_id = 0;
		$message->target_id = $message->order->seller_id;
		$message->type = Message::TYPE_SYSTEM;
		$message->content = 'The trade has been created, waiting for buyer to pay.';
		$message->extra = [
			'title' => 'The trade has been created'
		];
		$message->save();
	}
}
