<?php
namespace App\Listeners;

use App\Events\OrderStatusUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\OrderPaid;
use App\Models\Order;

/**
 * 发送订单已付款通知
 *
 * @author  
 *
 */
class NotifyOrderPaid implements ShouldQueue
{
	use InteractsWithQueue;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderStatusUpdated  $event
	 * @return void
	 */
	public function handle(OrderStatusUpdated $event)
	{
		$order = $event->order;

		// 订单状态为已付款。
		if ($order->status !== Order::STATUS_UNCONFIRMED) {
			return;
		}

		$order->seller->notify(new OrderPaid($order));
	}
}
