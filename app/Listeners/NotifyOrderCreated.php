<?php
namespace App\Listeners;

use App\Events\OrderCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\OrderCreated as OrderCreatedNotification;

class NotifyOrderCreated implements ShouldQueue
{
	use InteractsWithQueue;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderCreated  $event
	 * @return void
	 */
	public function handle(OrderCreated $event)
	{
		//$event->order->counter->user->notify(new OrderCreatedNotification($event->order));
	}
}
