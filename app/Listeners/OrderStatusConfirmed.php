<?php
namespace App\Listeners;

use App\Events\OrderStatusUpdated;
use App\Models\Transaction;
use App\Models\Order;

/**
 * 订单已确认
 *
 * @author  
 *
 */
class OrderStatusConfirmed
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderStatusUpdated  $event
	 * @return void
	 */
	public function handle(OrderStatusUpdated $event)
	{
		$order = $event->order;

		// 订单状态为已确认。
		if ($order->status !== Order::STATUS_CONFIRMED) {
			return;
		}

		// 取得卖家与买家的钱包。
		$seller_wallet = $order->seller->wallet($order->counter->coin);
		$buyer_wallet = $order->buyer->wallet($order->counter->coin);

		// 创建卖出交易。
		$sell_transaction = new Transaction();
		$sell_transaction->wallet()->associate($seller_wallet);
		$sell_transaction->amount = $order->quantity;
		$sell_transaction->type = Transaction::TYPE_TRADE_OUT; // 交易卖出。
		$sell_transaction->target()->associate($order->buyer);
		$sell_transaction->currency_code = $order->counter->currency_code;
		$sell_transaction->price = $order->price;
		if ($order->counter->user_id == $order->seller_id) { // 手续费由柜台方支付。
			$sell_transaction->fee = $order->fee;
		}
		$sell_transaction->save();

		// 创建买入交易。
		$buy_transaction = new Transaction();
		$buy_transaction->wallet()->associate($buyer_wallet);
		$buy_transaction->amount = $order->quantity;
		$buy_transaction->type = Transaction::TYPE_TRADE_IN; // 交易买入。
		$buy_transaction->target()->associate($order->seller);
		$buy_transaction->currency_code = $order->counter->currency_code;
		$buy_transaction->price = $order->price;
		$buy_transaction->relatedTransaction()->associate($sell_transaction);
		if ($order->counter->user_id == $order->buyer_id) { // 手续费由柜台方支付。
			$buy_transaction->fee = $order->fee;
		}
		$buy_transaction->save();

		// 关联交易。
		$sell_transaction->relatedTransaction()->associate($buy_transaction);
		$sell_transaction->save();

		// 扣除卖家冻结余额到买家可用余额。
		$seller_wallet->decrement('balance_locked', $order->quantity + $sell_transaction->fee);
		$buyer_wallet->increment('balance_available', $order->quantity - $buy_transaction->fee);

		// 增加买卖双方的累计交易次数。
		$order->seller()->increment('sell_trade_count');
		$order->buyer()->increment('buy_trade_count');

		// 增加买卖双方的累计交易量。
		$order->seller()->increment('sell_volume_' . strtolower($order->counter->coin), $order->quantity + $sell_transaction->fee);
		$order->buyer()->increment('buy_volume_' . strtolower($order->counter->coin), $order->quantity - $buy_transaction->fee);
	}
}
