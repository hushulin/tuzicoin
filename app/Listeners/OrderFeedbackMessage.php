<?php
namespace App\Listeners;

use App\Events\OrderFeedback;
use App\Models\Message;

/**
 * 订单评价消息通知
 *
 * @author  
 *
 */
class OrderFeedbackMessage
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderFeedback  $event
	 * @return void
	 */
	public function handle(OrderFeedback $event)
	{
		$order = $event->order;

		if ($order->buyer_feedback && $order->seller_feedback) {
			// 双方已评价，交易完成。

			// 发给买家。
			$message = new Message();
			$message->order()->associate($order);
			$message->source_id = 0;
			$message->target_id = $message->order->buyer_id;
			$message->type = Message::TYPE_SYSTEM;
			$message->content = 'Two sides have commented, the trade has finished.';
			$message->extra = [
				'title' => 'Transaction completed'
			];
			$message->save();

			// 发给卖家。
			$message = new Message();
			$message->order()->associate($order);
			$message->source_id = 0;
			$message->target_id = $message->order->seller_id;
			$message->type = Message::TYPE_SYSTEM;
			$message->content = 'Two sides have commented, the trade has finished.';
			$message->extra = [
				'title' => 'Transaction completed'
			];
			$message->save();
		} else {
			// 等待另一方评价。

			// 买家评论。
			if ($order->isDirty('buyer_feedback')) {

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Buyer has posted a comment, please comment each other.';
				$message->extra = [
					'title' => 'Buyer has posted a comment'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Buyer has posted a comment, please comment each other.';
				$message->extra = [
					'title' => 'Buyer has posted a comment'
				];
				$message->save();
			}

			// 卖家评论。
			if ($order->isDirty('seller_feedback')) {

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Seller has posted a comment, please comment each other.';
				$message->extra = [
					'title' => 'Seller has posted a comment'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Seller has posted a comment, please comment each other.';
				$message->extra = [
					'title' => 'Seller has posted a comment'
				];
				$message->save();
			}
		}
	}
}
