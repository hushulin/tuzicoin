<?php
namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

/**
 * 记录用户注册IP
 *
 * @author  
 *
 */
class LogRegisteredUser
{

	protected $request;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Handle the event.
	 *
	 * @param  Registered  $event
	 * @return void
	 */
	public function handle(Registered $event)
	{
		$event->user->register_ips = $this->request->getClientIps();
		$event->user->save();
	}
}
