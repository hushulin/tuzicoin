<?php
namespace App\Listeners;

use App\Events\OrderStatusUpdated;
use App\Models\Order;

/**
 * 订单已撤销
 *
 * @author  
 *
 */
class OrderStatusRevoked
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderStatusUpdated  $event
	 * @return void
	 */
	public function handle(OrderStatusUpdated $event)
	{
		$order = $event->order;

		// 订单状态已撤销或已过期。
		if ($order->status !== Order::STATUS_REVOKED && $order->status !== Order::STATUS_EXPIRED) {
			return;
		}

		// 解冻可用余额。
        $seller_wallet = $order->seller->wallet($order->counter->coin);
        $seller_wallet->decrement('balance_locked', $order->quantity + $order->fee);
        $seller_wallet->increment('balance_available', $order->quantity + $order->fee);
	}
}
