<?php
namespace App\Listeners;

use App\Events\OrderStatusUpdated;
use App\Models\Message;
use App\Models\Order;

/**
 * 订单状态变更消息
 *
 * @author  
 *
 */
class OrderStatusUpdatedMessage
{

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  OrderStatusUpdated  $event
	 * @return void
	 */
	public function handle(OrderStatusUpdated $event)
	{
		$order = $event->order;

		switch ($order->status) {
			case Order::STATUS_EXPIRED: // 订单过期。

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Buyer has not paid within the required time, the trade has expired.';
				$message->extra = [
					'title' => 'The trade has expired'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Buyer has not paid within the required time, the trade has expired.';
				$message->extra = [
					'title' => 'The trade has expired'
				];
				$message->save();

				break;
			case Order::STATUS_WAIT_PAYMENT: // 已过期订单从新开启。

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'The trade has been created, waiting for buyer to pay.';
				$message->extra = [
					'title' => 'The trade has been restarted'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'The trade has been created, waiting for buyer to pay.';
				$message->extra = [
					'title' => 'The trade has been restarted'
				];
				$message->save();

				break;
			case Order::STATUS_REVOKED: // 订单被取消。

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'The trade has been canncelled by buyer.';
				$message->extra = [
					'title' => 'The trade has been canncelled'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'The trade has been canncelled by buyer.';
				$message->extra = [
					'title' => 'The trade has been canncelled'
				];
				$message->save();

				break;
			case Order::STATUS_UNCONFIRMED: // 买家已付款。

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Buyer has paid, please seller make sure that have received the money and release the :coin to buyer.';
				$message->extra = [
					'title' => 'Buyer has paid'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Buyer has paid, please seller make sure that have received the money and release the :coin to buyer.';
				$message->extra = [
					'title' => 'Buyer has paid'
				];
				$message->save();

				break;
			case Order::STATUS_CONFIRMED: // 已确认。

				// 发给买家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->buyer_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Seller has released the :coin, Please comment on each other.';
				$message->extra = [
					'title' => 'Seller has released the :coin'
				];
				$message->save();

				// 发给卖家。
				$message = new Message();
				$message->order()->associate($order);
				$message->source_id = 0;
				$message->target_id = $message->order->seller_id;
				$message->type = Message::TYPE_SYSTEM;
				$message->content = 'Seller has released the :coin, Please comment on each other.';
				$message->extra = [
					'title' => 'Seller has released the :coin'
				];
				$message->save();

				break;
		}
	}
}
