<?php

namespace App\Listeners;

use Log;

class BizWorkFlow
{

    public function onStart($event)
    {
        // Log::info($event->getSubject()->getId());
        Log::info($event->getName());
    }

    /**
     * 为订阅者注册监听器.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'workflow.leave',
            'App\Listeners\BizWorkFlow@onStart'
        );

        // $events->listen(
        //     'Illuminate\Auth\Events\Logout',
        //     'App\Listeners\BizWorkFlow@onUserLogout'
        // );
    }

}
