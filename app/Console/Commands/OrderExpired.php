<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use Carbon\Carbon;

class OrderExpired extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'order:expired';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '标记已过期的订单状态。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// 找出所有已过期但状态未变更的订单。
		foreach (Order::where('status', Order::STATUS_WAIT_PAYMENT)->where('expires_at', '<', Carbon::now())->cursor() as $order) {
			// 修改订单状态。
			$order->status = Order::STATUS_EXPIRED;
			$order->save();
		}
	}
}
