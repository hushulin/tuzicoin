<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Routing\Router;

class RouteJson extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'route:json';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '生成前端命名的路由列表JSON文件到资源文件目录，用于前端JS引入使用。';

	/**
	 * The router instance.
	 *
	 * @var \Illuminate\Routing\Router
	 */
	protected $router;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Router $router)
	{
		parent::__construct();

		$this->router = $router;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$routes = collect();
		foreach ($this->router->getRoutes() as $route) {
			if ($route->getName() && ! preg_match('/^Root/i', $route->getName()) && ! preg_match('/^_/', $route->uri())) {
				$routes->put($route->getName(), [
					'method' => $route->methods(),
					'uri' => $route->uri()
				]);
			}
		}

		if ($routes->isEmpty()) {
			return $this->error("Your application doesn't have any routes.");
		}

		$filename = resource_path('assets/js/routes.json');
		file_put_contents($filename, $routes->toJson());

		$this->info('Successfully output to file: ' . $filename);
	}
}
