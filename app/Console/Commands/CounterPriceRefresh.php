<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Counter;
use Carbon\Carbon;

class CounterPriceRefresh extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'counter:price-refresh';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '根据最新的市场价和柜台的溢价，刷新柜台的价格，每10分钟执行一次。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$now = Carbon::now();
		foreach (Counter::where('updated_at', '<', $now->subMinutes(10))->oldest('updated_at')->cursor() as $counter) {
			$counter->touch();
		}
	}
}
