<?php
namespace App\Console\Commands\Deposit;

use Illuminate\Console\Command;
use App\Models\Address;
use App\Models\Deposit;
use Curl\Curl;
use DB;

/**
 * 根据 Blockchain.info 的API，监控用户的比特币存款状态。
 *
 * @author  
 *
 */
class BitcoinMonitor extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'deposit:monitor-btc';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '根据 Blockchain.info 的API，监控用户的比特币存款状态。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// 遍历所有比特币钱包地址。
		Address::oldest('id')->where('user_id', '!=', 0)->where('coin', Address::COIN_BTC)->chunk(100, function ($addresses) {
			$this->depositTxs($addresses);
		});
	}

	/**
	 * 刷新钱包地址的存款信息。
	 */
	protected function depositTxs($addresses)
	{
		// 取出用户的比特币存款地址列表。
		$address = $addresses->pluck('address');

		// 在 blockchain.info 上查询收款记录。
		$txs = $this->getTxs($address);

		// 检查每笔收款是否有对应的存款单。
		$addresses->each(function ($item) use ($txs) {
			if (array_key_exists($item->address, $txs)) {
				// 刷新每一笔交易对应的存款单。
				foreach ($txs[$item->address] as $txid => $tx) {
					DB::transaction(function () use ($item, $txid, $tx) {
						$deposit = Deposit::firstOrNew([
							'coin' => $item->coin,
							'txid' => $txid
						]);
						$deposit->user_id = $item->user_id;
						$deposit->amount = $tx['value'];
						$deposit->confirmations = $tx['confirmations'];
						$deposit->save();
					});
				}
			}
		});
	}

	/**
	 * 在 blockchain.info 上查询收款记录。
	 */
	protected function getTxs($address)
	{
		// 在 blockchain.info 上查询收款记录。
		$curl = new Curl();
		$curl->setUserAgent(config('app.name') . '+(+' . config('app.url') . ')');
		$curl->get('https://blockchain.info/zh-cn/multiaddr?active=' . $address->implode('|'));
		if ($curl->error) {
			$this->error($curl->error_message);
			$curl->close();
		} else {
			$json = json_decode($curl->response);
			$curl->close();

			// 格式化结果。
			$txs = [];
			foreach ($json->txs as $item) {
				foreach ($item->out as $out) {
					if (! array_key_exists($out->addr, $txs)) {
						$txs[$out->addr] = [];
					}

					// 计算确认数。
					$confirmations = 0;
					// 存在双花时，确认数为零。
					if (! $item->double_spend) {
						// 已确认交易才有确认数。
						if (isset($item->block_height)) {
							$confirmations = $json->info->latest_block->height - $item->block_height + 1;
						}
					}

					$txs[$out->addr][$item->hash] = [
						'time' => $item->time,
						'value' => $out->value / $json->info->conversion,
						'confirmations' => $confirmations
					];
				}
			}
			$txs = array_only($txs, $address->all());
			return $txs;
		}
	}
}
