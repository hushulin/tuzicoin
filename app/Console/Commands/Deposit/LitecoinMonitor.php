<?php
namespace App\Console\Commands\Deposit;

use Illuminate\Console\Command;
use App\Models\Address;
use App\Models\Deposit;
use Curl\Curl;
use DB;

/**
 * 根据 okcoin.cn 的API，监控用户的莱特币存款状态。
 *
 * @link https://www.okcoin.cn/about/block_api.html
 * @author  
 *
 */
class LitecoinMonitor extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'deposit:monitor-ltc';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '根据 okcoin.cn 的API，监控用户的莱特币存款状态。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// 遍历所有钱包地址。
		Address::oldest('id')->where('user_id', '!=', 0)->where('coin', Address::COIN_LTC)->chunk(100, function ($addresses) {
			$this->depositTxs($addresses);
		});
	}

	/**
	 * 刷新钱包地址的存款信息。
	 */
	protected function depositTxs($addresses)
	{
		// 取出用户的存款地址列表。
		$address = $addresses->pluck('address');

		// 获取地址的收款记录。
		$txs = $this->getTxs($address);

		// 检查每笔收款是否有对应的存款单。
		$addresses->each(function ($item) use ($txs) {
			if (array_key_exists($item->address, $txs)) {
				// 刷新每一笔交易对应的存款单。
				foreach ($txs[$item->address] as $txid => $tx) {
					DB::transaction(function () use ($item, $txid, $tx) {
						$deposit = Deposit::firstOrNew([
							'coin' => $item->coin,
							'txid' => $txid
						]);
						$deposit->user_id = $item->user_id;
						$deposit->amount = $tx['value'];
						$deposit->confirmations = $tx['confirmations'];
						$deposit->save();
					});
				}
			}
		});
	}

	/**
	 * 在 okcoin.cn 上查询收款记录。
	 */
	protected function getTxs($addresses)
	{
		$txs = [];
		foreach ($addresses as $address) {
			$curl = new Curl();
			$curl->setUserAgent(config('app.name') . '+(+' . config('app.url') . ')');
			$curl->get('http://block.okcoin.cn/api/v1/address.do?address=' . $address . '&symbol=LTC&size=50');
			if ($curl->error) {
				$this->error($curl->error_message);
				$curl->close();
			} else {
				$json = json_decode($curl->response);
				$curl->close();

				// 格式化结果。
				foreach ($json as $item) {
					foreach ($item->vout as $out) {
						foreach ($out->scriptPubKey->addresses as $addr) {
							if (! array_key_exists($addr, $txs)) {
								$txs[$addr] = [];
							}

							$txs[$addr][$item->txid] = [
								'time' => $item->received_time / 1000,
								'value' => $out->value / 100000000,
								'confirmations' => $item->confirmations
							];
						}
					}
				}
			}
		}
		$txs = array_only($txs, $addresses->all());
		return $txs;
	}
}
