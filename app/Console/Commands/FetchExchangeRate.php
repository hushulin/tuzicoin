<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ExchangeRate;
use DOMDocument;
use Curl\Curl;

/**
 * 从雅虎的API获取各个货币的汇率信息
 *
 * @author  
 *
 */
class FetchExchangeRate extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'fetch:exchange-rate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '抓取各个货币的汇率信息。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$curl = new Curl();
		$curl->setUserAgent('Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko');
		$curl->get('https://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote');
		if ($curl->error) {
			$this->error($curl->error_message);
			$curl->close();
		} else {
			$dom = new DOMDocument();
			$dom->loadXML($curl->response);
			$curl->close();
			foreach ($dom->documentElement->getElementsByTagName('resource') as $resource) {
				$attributes = [];
				foreach ($resource->getElementsByTagName('field') as $field) {
					$attributes[$field->getAttribute('name')] = trim($field->nodeValue);
				}
				if ($attributes['name'] !== 'USD') {
					@list ($_, $name) = explode('/', $attributes['name']);
					if (! $name) {
						continue;
					}
					$attributes['name'] = $name;
				}
				$attributes = array_only($attributes, [
					'name',
					'price'
				]);
				ExchangeRate::updateOrCreate(array_only($attributes, [
					'name'
				]), $attributes);
				$this->line($attributes);
			}
		}
	}
}
