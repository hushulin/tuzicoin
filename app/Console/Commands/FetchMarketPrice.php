<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\MarketPrice;
use Exception;
use Curl\Curl;

/**
 * 从各大市场获取数字货币的最新市场价数据
 *
 * @author  
 *
 */
class FetchMarketPrice extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'fetch:market-price';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '从各大市场获取数字货币的最新市场价数据。';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->bitfinex();
		$this->coinbase();
		$this->okcoin();
		$this->bitstamp();
		$this->itbit();
		$this->gdax();
		$this->hitbtc();
		$this->bitstar();
	}

	/**
	 * Bitfinex
	 */
	protected function bitfinex()
	{
		try {
			$json = $this->http('https://api.bitfinex.com/v1/pubticker/BTCUSD');
			$data = [
				'market' => 'Bitfinex',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['last_price'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}

		try {
			$json = $this->http('https://api.bitfinex.com/v1/pubticker/LTCUSD');
			$data = [
				'market' => 'Bitfinex',
				'coin' => 'LTC',
				'currency' => 'USD',
				'last' => $json['last_price'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * Coinbase
	 */
	protected function coinbase()
	{
		try {
			$last = array_get($this->http('https://api.coinbase.com/v2/prices/spot?currency=USD', [
				'CB-VERSION' => '2017-09-23'
			]), 'data.amount');
			$buy = array_get($this->http('https://api.coinbase.com/v2/prices/buy?currency=USD', [
				'CB-VERSION' => '2017-09-23'
			]), 'data.amount');
			$sell = array_get($this->http('https://api.coinbase.com/v2/prices/sell?currency=USD', [
				'CB-VERSION' => '2017-09-23'
			]), 'data.amount');
			$data = [
				'market' => 'Coinbase',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $last,
				'buy' => $buy,
				'sell' => $sell
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * OKCoin国际
	 */
	protected function okcoin()
	{
		try {
			$json = $this->http('https://www.okcoin.com/api/v1/ticker.do?symbol=btc_usd');
			$json = $json['ticker'];
			$data = [
				'market' => 'OKCoin',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['buy'],
				'sell' => $json['sell']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}

		try {
			$json = $this->http('https://www.okcoin.com/api/v1/ticker.do?symbol=ltc_usd');
			$json = $json['ticker'];
			$data = [
				'market' => 'OKCoin',
				'coin' => 'LTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['buy'],
				'sell' => $json['sell']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * Bitstamp
	 */
	protected function bitstamp()
	{
		try {
			$json = $this->http('https://www.bitstamp.net/api/v2/ticker/btcusd/');
			$data = [
				'market' => 'Bitstamp',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}

		try {
			$json = $this->http('https://www.bitstamp.net/api/v2/ticker/ltcusd/');
			$data = [
				'market' => 'Bitstamp',
				'coin' => 'LTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * itBit
	 */
	protected function itbit()
	{
		try {
			$json = $this->http('https://api.itbit.com/v1/markets/XBTUSD/ticker');
			$data = [
				'market' => 'itBit',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['lastPrice'],
				'buy' => $json['low24h'],
				'sell' => $json['high24h']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * GDAX
	 */
	protected function gdax()
	{
		try {
			$json = $this->http('https://api.gdax.com/products/BTC-USD/ticker');
			$data = [
				'market' => 'GDAX',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['price'],
				'buy' => $json['bid'],
				'sell' => $json['ask']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}

		try {
			$json = $this->http('https://api.gdax.com/products/LTC-USD/ticker');
			$data = [
				'market' => 'GDAX',
				'coin' => 'LTC',
				'currency' => 'USD',
				'last' => $json['price'],
				'buy' => $json['bid'],
				'sell' => $json['ask']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * HitBTC
	 */
	protected function hitbtc()
	{
		try {
			$json = $this->http('https://api.hitbtc.com/api/2/public/ticker/BTCUSD');
			$data = [
				'market' => 'HitBTC',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}

		try {
			$json = $this->http('https://api.hitbtc.com/api/2/public/ticker/LTCUSD');
			$data = [
				'market' => 'HitBTC',
				'coin' => 'LTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/**
	 * BitStar
	 */
	protected function bitstar()
	{
		try {
			$json = $this->http('https://www.bitstar.com/api/v1/market/ticker/swap-btc-usd');
			$data = [
				'market' => 'BitStar',
				'coin' => 'BTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}

		try {
			$json = $this->http('https://www.bitstar.com/api/v1/market/ticker/swap-ltc-usd');
			$data = [
				'market' => 'BitStar',
				'coin' => 'LTC',
				'currency' => 'USD',
				'last' => $json['last'],
				'buy' => $json['low'],
				'sell' => $json['high']
			];
			$data = $this->updateOrCreate($data);
			$this->echoMarketPrice($data);
		} catch (Exception $e) {
			$this->error($e->getMessage());
		}
	}

	private function http($url, $headers = [])
	{
		$curl = new Curl();
		$curl->setOpt(CURLOPT_TIMEOUT, 10);
		foreach ($headers as $key => $value) {
			$curl->setHeader($key, $value);
		}
		$curl->get($url);
		if ($curl->error) {
			throw new Exception($curl->error_message);
		}
		$json = json_decode($curl->response, true);
		$curl->close();
		return $json;
	}

	private function updateOrCreate($data)
	{
		return MarketPrice::updateOrCreate(array_only($data, [
			'market',
			'coin',
			'currency'
		]), $data);
	}

	private function echoMarketPrice($data)
	{
		$this->table([
			'market',
			'coin',
			'currency',
			'last',
			'buy',
			'sell'
		], [
			[
				$data->market,
				$data->coin,
				$data->currency,
				$data->last,
				$data->buy,
				$data->sell
			]
		]);
	}
}
