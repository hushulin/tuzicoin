<?php
namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		Commands\Deposit\BitcoinMonitor::class,
		Commands\Deposit\LitecoinMonitor::class,
		Commands\FetchMarketPrice::class,
		Commands\FetchExchangeRate::class,
		Commands\OrderExpired::class,
		Commands\CounterPriceRefresh::class,
		Commands\RouteJson::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command(Commands\Deposit\BitcoinMonitor::class)->withoutOverlapping();
		$schedule->command(Commands\Deposit\LitecoinMonitor::class)->withoutOverlapping();
		$schedule->command(Commands\FetchMarketPrice::class)->withoutOverlapping();
		$schedule->command(Commands\FetchExchangeRate::class)->withoutOverlapping();
		$schedule->command(Commands\OrderExpired::class)->withoutOverlapping();
		$schedule->command(Commands\CounterPriceRefresh::class)->withoutOverlapping();
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
