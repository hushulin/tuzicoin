<?php
namespace App\Channels;

use Illuminate\Notifications\Notification;
use Aliyun\Core\Config;
use Aliyun\Core\Profile\DefaultProfile;
use Aliyun\Core\DefaultAcsClient;
use Aliyun\Api\Sms\Request\V20170525\SendSmsRequest;
use RuntimeException;

/**
 * 阿里云短信服务
 *
 * @link https://dysms.console.aliyun.com/dysms.htm
 * @author  
 *
 */
class SmsChineseChannel
{

	protected $acsClient;

	protected $signName;

	protected $templateCode;

	public function __construct()
	{
	}

	/**
	 * 发送给定通知
	 *
	 * @param  mixed  $notifiable
	 * @param  \Illuminate\Notifications\Notification  $notification
	 * @return void
	 */
	public function send($notifiable, Notification $notification)
	{
		if (! $to = $notifiable->routeNotificationFor('sms')) {
			return;
		}
		// 只能发送国内短信。
		if (! preg_match('/^86(\d+)/', $to, $matches)) {
			throw new RuntimeException('Can only be sent to China mobile phone number.');
		}
		$to = $matches[1];
		$message = urlencode($notification->toSms($notifiable)->content);
        $url = "http://utf8.api.smschinese.cn/?Uid=chchina2017&Key=8f5a07055da7ad30dca3&smsMob={$to}&smsText={$message}";
		$ch=curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);//用PHP取回的URL地址（值将被作为字符串）
        curl_setopt($ch,CURLOPT_TIMEOUT,5);//30秒超时限制
        $data = curl_exec($ch);//抓取URL并把他传递给浏览器
        curl_close($ch);//释放资源
        return $data;
	}
}
