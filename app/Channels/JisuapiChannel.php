<?php
namespace App\Channels;

use Illuminate\Notifications\Notification;
use Curl\Curl;

/**
 * 极速数据
 *
 * @link https://www.jisuapi.com/api/sms
 * @author  
 *
 */
class JisuapiChannel
{

	protected $curl;

	protected $appkey;

	protected $signature;

	protected $user_agent = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

	public function __construct(Curl $curl)
	{
		$this->curl = $curl;
		$this->curl->setUserAgent($this->user_agent);

		$this->appkey = config('services.jisuapi.appkey');
		$this->signature = config('services.jisuapi.signature');
	}

	/**
	 * 发送给定通知
	 *
	 * @param  mixed  $notifiable
	 * @param  \Illuminate\Notifications\Notification  $notification
	 * @return void
	 */
	public function send($notifiable, Notification $notification)
	{
		if (! $to = $notifiable->routeNotificationFor('sms')) {
			return;
		}

		// 只能发送国内短信。
		if (! preg_match('/^86(\d+)/', $to, $matches)) {
			return;
		}
		$to = $matches[1];

		$message = $notification->toSms($notifiable);

		// 接口地址与请求参数。
		$gateway = 'http://api.jisuapi.com/sms/send';
		$data = [
			'appkey' => $this->appkey,
			'mobile' => $to,
			'content' => $message->content . $this->signature
		];

		// 发送请求。
		$this->curl->post($gateway, $data);

		// 处理结果。
		$json = @json_decode($this->curl->response, true);

		$result = null;

		if ($this->curl->error || @$json['status'] !== '0') {
			// 失败返回错误信息。
			logger()->error('Jisuapi sms send fail', [
				'url' => $gateway,
				'data' => $data,
				'error_code' => $this->curl->error_code,
				'response' => $this->curl->response
			]);

			$msg = @$json['msg'];
			if ($this->curl->error) {
				$msg = $this->curl->error_code . ': ' . ($this->curl->error_message ?: '未知错误。');
			}
			$result = (string) $msg;
		} else {
			// 成功返回成功条数。
			$result = (int) $json['result']['count'];
		}

		// 返回结果。
		return $result;
	}
}
