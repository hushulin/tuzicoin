<?php
namespace App\Channels;

use Illuminate\Notifications\Notification;
use Aliyun\Core\Config;
use Aliyun\Core\Profile\DefaultProfile;
use Aliyun\Core\DefaultAcsClient;
use Aliyun\Api\Sms\Request\V20170525\SendSmsRequest;
use RuntimeException;

/**
 * 阿里云短信服务
 *
 * @link https://dysms.console.aliyun.com/dysms.htm
 * @author  
 *
 */
class AliyunSmsChannel
{

	protected $acsClient;

	protected $signName;

	protected $templateCode;

	public function __construct()
	{
		$accessKeyId = config('LTAIN2NZSOBAdOkR');
		$accessKeySecret = config('nUrxgARFqmRYkNHCKJhZ9oMwasoFk7');
		$this->signName = config('BTROTC');
		$this->templateCode = config('SMS_126781664');

		// 加载区域结点配置
		Config::load();

		// 暂时不支持多Region
		$region = 'cn-hangzhou';

		// 服务结点
		$endPointName = 'cn-hangzhou';

		// 初始化用户Profile实例
		$profile = DefaultProfile::getProfile($region, $accessKeyId, $accessKeySecret);

		// 增加服务结点
		DefaultProfile::addEndpoint($endPointName, $region, 'Dysmsapi', 'dysmsapi.aliyuncs.com');

		// 初始化AcsClient用于发起请求
		$this->acsClient = new DefaultAcsClient($profile);
	}

	/**
	 * 发送给定通知
	 *
	 * @param  mixed  $notifiable
	 * @param  \Illuminate\Notifications\Notification  $notification
	 * @return void
	 */
	public function send($notifiable, Notification $notification)
	{
		if (! $to = $notifiable->routeNotificationFor('sms')) {
			return;
		}

		// 只能发送国内短信。
		if (! preg_match('/^86(\d+)/', $to, $matches)) {
			throw new RuntimeException('Can only be sent to China mobile phone number.');
		}
		$to = $matches[1];

		$message = $notification->toSms($notifiable);

		// 初始化SendSmsRequest实例用于设置发送短信的参数
		$request = new SendSmsRequest();

		// 必填，设置雉短信接收号码
		$request->setPhoneNumbers($to);

		// 必填，设置签名名称
		$request->setSignName($this->signName);

		// 必填，设置模板CODE
		$request->setTemplateCode($this->templateCode[$message->view]);

		// 可选，设置模板参数
		$request->setTemplateParam(json_encode($message->data));

		// 可选，设置流水号
		// $request->setOutId($outId);

		// 发起访问请求
		try {
			$acsResponse = $this->acsClient->getAcsResponse($request);
		} catch (ServerException $e) {
			$acsResponse = $e->getMessage();

			// 记录错误信息。
			logger()->error('Aliyun sms send fail', [
				'message' => $e->getMessage()
			]);
		}
        //dump($acsResponse);exit;
		// 返回结果。
		return $acsResponse;
	}
}
