<?php

namespace App\Project\Plugins\ProjectPreliminary\ListAndScore;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{

	function __construct()
	{

	}

	public function index()
	{
		return view('projectpreliminary.listandscore::index');
	}
}
