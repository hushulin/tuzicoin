<?php
namespace App\Project\Plugins\ProjectPreliminary\ListAndScore;

use App\Project\Classes\PluginBase;

class Plugin extends PluginBase
{

	public $disabled = true;

	// 所有表名
	// proj_preli_allocate3ppo project_id user_id
	// proj_preli_sign_models 打分实例
	// proj_preli_sign_records 打分记录

	public function register()
	{
	}

	public function boot()
	{

	}

	// 注册工作流的地点
	public function registerProjectPlaces()
    {
        return [
        	'DemandList', // 叫做起始节点
        	'select-wait', // 等待初筛的一个状态,就是申报项目动作完成后
        	'select-score', // 打分中的一个状态,第一个专家打分触发
        	'select-score-done', // 完成打分
        	'select-access', // 通过
        	'select-deny', // 不通过
        ];
    }

	// 注册工作流的过渡
	public function registerProjectTransition()
	{
		return [
			['ProjectApply' , 'DemandList' , 'select-wait'], // 申报项目过渡
			['FirstScore' , 'select-wait' , 'select-score'], // 第一个专家打分动作,这是个隐含动作,不体现在页面
			['EndScore' , 'select-score' , 'select-score-done'], // 最后一个专家打完分,也是个隐含动作
			['SignAccess' , 'select-score-done' , 'select-access'], // 点击初筛通过按钮
			['SignDeny' , 'select-score-done' , 'select-deny'], // 点击初筛不通过按钮
		];
	}
}
