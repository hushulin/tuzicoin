<?php

namespace App\Project\Plugins\ProjectPreliminary\ListAndScore\Model;

use Illuminate\Database\Eloquent\Model;

class SignModel extends Model
{

	protected $fillable = [
		'project_id',
		'end_date',
		'statis_avg_score',
    ];

    // protected $dates = ['deleted_at'];

    protected $table = 'proj_preli_sign_models';

}
