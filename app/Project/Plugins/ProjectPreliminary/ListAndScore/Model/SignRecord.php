<?php

namespace App\Project\Plugins\ProjectPreliminary\ListAndScore\Model;

use Illuminate\Database\Eloquent\Model;

class SignRecord extends Model
{

	protected $fillable = [
		'sign_model_id',
		'user_id',
		'project_id',
		'data',
		'group_score',
		'score',
		'opinion',
		'comment',
    ];

    // protected $dates = ['deleted_at'];

    protected $table = 'proj_preli_sign_records';

}
