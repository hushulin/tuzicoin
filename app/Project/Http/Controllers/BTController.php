<?php

namespace App\Project\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Project\Models\BizTopic;
use App\Project\Misc\CellTypeConst;

use App\Project\Models\BizAcqVg;
use App\Project\Models\BizAcqAg;
use App\Project\Models\BizSellVg;
use App\Project\Models\BizSellAg;

class BTController extends Controller
{

	function __construct()
	{

	}

	public function pubform()
	{
		return view('project.btForm2');
	}

	public function publish(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'ad_area' => 'required',
			'ad_type' => 'required',
			'title' => 'required | string',

			// common
			'good_name' => 'sometimes | required',
			'price' => 'sometimes | required',
			'contact' => 'sometimes | required',
			'img' => 'sometimes | required | file',
			'notes' => 'sometimes | required | string | nullable',

			// extend
			'acq_num' => 'sometimes | required',
			'acq_addr' => 'sometimes | required',
			'stock_num' => 'sometimes | required',
			'min_limit' => 'sometimes | required',
			'max_limit' => 'sometimes | required',
			'send_addr' => 'sometimes | required',

        ]);

    	if ($validator->fails()) {
    		return response()->json([
    			'status' => 1,
    			'msg' => '参数错误',
    		]);
        }

        $ad_area = $request->input('ad_area');
		$ad_type = $request->input('ad_type');
		$title = $request->input('title');
		$creator_id = auth()->user()->id;

		// good_name,acq_num,price,contact,img,acq_addr,notes,-- BizAcqAg
		// good_name,acq_num,price,contact,img,acq_addr,notes,-- BizAcqVg
		// good_name,stock_num,price,contact,img,send_addr,notes, -- BizSellAg
		// good_name,stock_num,price,contact,min_limit,max_limit,img,send_addr,notes, -- BizSellVg
		$good_name = $request->input('good_name' , '');
		$price = $request->input('price' , '');
		$contact = $request->input('contact' , '');

		if ($request->hasFile('img')) {
			//
			$img = $request->img->store('bizimgs');
		}else {
			$img = '';
		}


		$notes = $request->input('notes' , '');
		$acq_num = $request->input('acq_num' , '');
		$acq_addr = $request->input('acq_addr' , '');
		$stock_num = $request->input('stock_num' , '');
		$min_limit = $request->input('min_limit' , '');
		$max_limit = $request->input('max_limit' , '');
		$send_addr = $request->input('send_addr' , '');

		$topic = BizTopic::create(compact('ad_area' , 'ad_type' , 'title' , 'creator_id'));

		// Misc
		$model = CellTypeConst::instance()->detectCellModel($ad_type , $ad_area);

		$cellModel = '';

		// Lib
		switch ($model) {

			case '\App\Project\Models\BizAcqVg':
				$cellModel = BizAcqVg::create(compact('good_name','acq_num','price','contact','img','acq_addr','notes'));
				break;

			case '\App\Project\Models\BizAcqAg':
				$cellModel = BizAcqAg::create(compact('good_name','acq_num','price','contact','img','acq_addr','notes'));
				break;

			case '\App\Project\Models\BizSellVg':
				$cellModel = BizSellVg::create(compact('good_name','stock_num','price','contact','min_limit','max_limit','img','send_addr','notes'));
				break;

			case '\App\Project\Models\BizSellAg':
				$cellModel = BizSellAg::create(compact('good_name','stock_num','price','contact','img','send_addr','notes'));
				break;

			default:
				// code...
				break;
		}

		if ($cellModel) {
			//
			$topic->cell_id = $cellModel->id;
			$topic->cell_type = $model;
			$topic->save();

			return response()->json([
				'status' => 0,
				'msg' => '发布成功',
				'data' => $topic->id,
			]);
		}

		// Delete topic
		$topic->delete();

		return response()->json([
            'status' => 2,
            'msg' => '发布失败：系统不支持该策略',
        ]);
	}

	public function lists(Request $request)
	{

		$validator = Validator::make($request->all(), [
			'page' => 'sometimes|required|numeric|min:1',
			'page_size' => 'sometimes|required|numeric|min:1',


        ]);

    	if ($validator->fails()) {
    		return response()->json([
    			'status' => 1,
    			'msg' => $validator->errors(),
    		]);
        }

        $page = $request->input('page' , 1) * 1;
        $page_size = $request->input('page_size' , 10) * 1;

        $offset = ($page - 1) * $page_size;

        $total = BizTopic::with('cell')->count();

		$lists = BizTopic::select('id' , 'title' , 'cell_id' , 'cell_type')->with('cell')->take($page_size)->skip($offset)->get();

		return response()->json([
			'status' => 0,
			'data' => $lists,
			'total' => $total,
		]);
	}

	public function detail(int $id)
	{
		$topic = BizTopic::findOrFail($id);
		$topic->load('cell');

		return response()->json([
			'status' => 0,
			'data' => $topic,
		]);
	}
}
