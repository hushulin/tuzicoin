<?php

namespace App\Project\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project\Classes\SysConfig\ProjectFormSchema;
use App\Project\Classes\SysConfig\FormBuilder;
// use Kris\LaravelFormBuilder\FormBuilder;

class ApplyController extends Controller
{

	private $schema;

	private $form_builder;

	function __construct(ProjectFormSchema $schema , FormBuilder $formBuilder)
	{
		$this->schema = $schema;
		$this->form_builder = $formBuilder;
	}

	public function apply(int $demand_id , Request $request)
	{
		$page_title = '申请项目';
        $page_description = '';

		// 先判断是否有当前组织
		// 没有就要求他去创建组织
		$cur_org = $this->currentOrganization();

		// if ($cur_org == NULL) {
		// 	return redirect('project/notification');
		// }

		// 再看需求目录是否有效

		// 准备组织信息

		// 准备表单字段
		$schemaData = $this->schema->grabSchemaData();

		$form = $this->form_builder->createByArray($schemaData)->renderForm();

		// 开始构造他的项目申报表单
		return view('project.apply' , compact('form' , 'page_title' , 'page_description'));
	}
}
