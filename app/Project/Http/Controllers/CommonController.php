<?php

namespace App\Project\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project\Models\Project;

class CommonController extends Controller
{

	function __construct()
	{
		# code...
	}

	public function notification(Request $request)
	{

		$page_title = '';
        $page_description = '';

        // 这边可能要高亮组织菜单

		return view('project.notification' , compact('page_title' , 'page_description'));
	}
}
