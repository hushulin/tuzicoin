<?php

namespace App\Project\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project\Models\Project;
use Illuminate\Support\Facades\Session;

class TestController extends Controller
{

	function __construct()
	{
		# code...
	}

	public function project(Request $request)
	{
		$project = Project::findOrFail(1);

		// 这里给当前位置也行
		// $project->customMark = 'a';

		$f = $project->wfInstance->getEnabledTransitions($project);
		dd($f);
	}

	public function setplan()
	{
		Session::put('current_plan_id' , 12);
		echo Session::get('current_plan_id');
	}
}
