<?php

namespace App\Project\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Project\Models\BizOrder;
use Exception;
use App\Project\Models\BizTopic;
use App\Project\Misc\CellTypeConst;
use App\Notifications\MessageHaveRead;
use App\Models\Message;
use App\Project\Misc\WorkFlowReadable;

class BizOrderController extends Controller
{

    public function __construct()
    {

    }


    public function submitOrder(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'topic_id' => 'required|numeric|min:1', // 广告id
            'quantity' => 'required|numeric|min:1', // 数量
        ]);

    	if ($validator->fails()) {
    		return response()->json([
    			'status' => 1,
    			'msg' => $validator->errors(),
    		]);
        }

        $topic_id = $request->input('topic_id' , 0);
        $quantity = $request->input('quantity' , 0);

        try {
            $topic = BizTopic::with('cell')->findOrFail($topic_id);
        } catch (Exception $e) {

            return response()->json([
                'status' => 2,
                'msg' => '广告信息不存在',
            ]);
        }

        $strategy = CellTypeConst::instance()->detectWorkFlow($topic->ad_type , $topic->ad_area);

        if ( $strategy ) {
            $results = app('App\Project\Lib\Action\Biz' . $strategy)->submitOrder($topic , $quantity);

            return response()->json($results);

        }else {
            return response()->json([
                'status' => 3,
                'msg' => '工作流策略出错',
            ]);
        }

    }

    public function detail(int $id)
    {

        $order = BizOrder::with('topic' , 'topic.cell')->findOrFail($id);

        // 消息
        $messages = $order->messages()
            ->byUser(auth()->user())
            ->with('source', 'target')
            ->latest()
            ->take(1000)
            ->get()
            ->reverse()
            ->values();

        //  标记当前用户的所有消息为已读。
        if (! $messages->isEmpty()) {
            // 通知发送人对方已读。
            $notify_messages = $messages->filter(function ($message) {
                return ! $message->is_read;
            });
            if (! $notify_messages->isEmpty()) {
                Message::whereIn('id', $notify_messages->pluck('id'))->update([
                    'is_read' => true
                ]);
                $notify_messages = $notify_messages->filter(function ($message) {
                    return $message->type != Message::TYPE_SYSTEM;
                });
                if (! $notify_messages->isEmpty()) {
                    $notify_messages->first()->source->notify(new MessageHaveRead($notify_messages));
                }
            }
        }

        // 操作按钮等
        $enables = WorkFlowReadable::instance()->readEnables($order);

        return response()->json([
            'status' => 0,
            'data' => compact('order' , 'messages' , 'enables'),
        ]);
    }

    public function operateOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|numeric|min:1', // 订单id
            'code' => 'required|string', // 数量
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 1,
                'msg' => $validator->errors(),
            ]);
        }

        $order_id = $request->input('order_id' , 0);
        $code = $request->input('code' , '');

        try {
            // 这里收到的order:
            // 是个已经确定好哪个工作流策略的
            $order = BizOrder::with('topic' , 'topic.cell')->findOrFail($order_id);
        } catch (Exception $e) {

            return response()->json([
                'status' => 2,
                'msg' => '订单信息不存在',
            ]);
        }

        try {

            $arr_code = explode('-', $code);

            if ( $order->wfInstance->can($order , $arr_code[2]) ) {

                // 提前确定
                $enables = WorkFlowReadable::instance()->readEnables($order);

                $to = '';
                foreach ($enables as $value) {
                    if ( $value['code'] == $code) {
                        $to = $value['to'];
                    }
                }

                $order->wfInstance->apply($order , $arr_code[2]);

                event('workflow.' . $code);

                // 保存一下订单信息

                // 之前：
                // 要锁定金额
                // 要取消锁定
                // 要释放到对方账户


                BizOrder::where('id' , $order_id)->update(['place' => $to]);

                return response()->json([
                    'status' => 0,
                    'msg' => '订单操作成功',
                ]);
            }else {

                return response()->json([
                    'status' => 3,
                    'msg' => '订单操作不合法',
                ]);
            }

        } catch (Exception $e) {

            return response()->json([
                'status' => 4,
                'msg' => '订单操作失败:' . $e->getMessage(),
            ]);
        }

    }

    public function detail2(int $id)
    {
    	$order = BizOrder::with('topic' , 'topic.cell')->findOrFail($id);

        var_dump($order->topic->cell->id);
        die();

    	$wf = $order->wfInstance;

    	$f = $order->wfInstance->getEnabledTransitions($order);

        var_dump($f);

        $order->wfInstance->apply($order , 'PayOrder');

        $f = $order->wfInstance->getEnabledTransitions($order);

        // 当前
        event('workflow.leave' , [$order->wfInstance]);
        var_dump($f);
		// dd($f);

    	// dd($wf->getPlaces());
    }

    public function TestWorkFlow()
    {
        $order = BizOrder::findOrFail(64);

        return response()->json($order->wfInstance->getEnabledTransitions($order));
    }
}
