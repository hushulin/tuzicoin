<?php

Route::middleware('web' , 'auth')->prefix('biz')->name('biz.')->namespace('App\Project\Http\Controllers')->group(function ()
{
	Route::post('bt/publish', 'BTController@publish');
	Route::post('bizorder/submit', 'BizOrderController@submitOrder');
	Route::post('bizorder/operate', 'BizOrderController@operateOrder');
	Route::get('bizorder/{id}', 'BizOrderController@detail')->where('id' , '\d+');
	Route::get('bt/pubform', 'BTController@pubform')->name('bt.pubform');
});


Route::middleware('web')->prefix('biz')->name('biz.')->namespace('App\Project\Http\Controllers')->group(function ()
{

	Route::get('bt/lists', 'BTController@lists');
	Route::get('bt/detail/{id}', 'BTController@detail');
	Route::get('bizorder/testworkflow', 'BizOrderController@TestWorkFlow');
});

// 后台路由组
// 扩展自：/routes/root.php
Route::middleware('web' , 'auth:root' , 'permission:root' , 'audit:root')->name('Root')->prefix('tzcroot')->namespace('App\Project\Http\Controllers')->group(function ()
{

	//
	// 广告列表
	Route::get('advert/lists', 'RootAdvertController@lists')->name('AdvertList');
});
