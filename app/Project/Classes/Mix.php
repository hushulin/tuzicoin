<?php
namespace App\Project\Classes;

class Mix
{

	use Concerns\Singleton;

	public function showName($key)
	{
		if ( isset( $this->maps()[$key][0] ) ) {
			return $this->maps()[$key][0];
		}

		return $key;
	}

	public function showColor($key)
	{
		if ( isset( $this->maps()[$key][1] ) ) {
			return $this->maps()[$key][1];
		}

		return 'default';
	}

	protected function maps()
	{
		return [

			/**
			 * 初筛项目
			 */
			// 起始位置
			'DemandList' => ['需求目录' , ''],

			// 项目初筛小节点
			'select' => ['初筛项目' , ''],
			'select-wait' => ['等待初筛',''],
			'select-score' => ['打分中' , ''],
			'select-score-done' => ['完成打分' , ''],
			'select-access' => ['初筛通过' , ''],
			'select-deny' => ['初筛不通过' , ''],

			// 动作
			'ProjectApply' => ['申请项目' , ''],
			'FirstScore' => ['首个专家打分' , ''],
			'EndScore' => ['最后一个专家打分' , ''],
			'SignAccess' => ['初筛通过' , ''],
			'SignDeny' => ['初筛不通过' , ''],

			/**
			 * 尽调项目
			 */
			'due-miss-doc-desc' => ['未添加材料说明' , ''],
			'due-miss-doc' => ['未提交材料' , ''],
			'due-doc-submitted' => ['已提交材料' , ''],
			'due-offline-surve' => ['启动线下调查' , ''],
			'due-duereport-done' => ['完成尽调报告' , ''],
			'due-access' => ['尽调通过' , ''],
			'due-deny' => ['尽调不通过' , ''],

			'DueDiligence' => ['进入尽调' , ''],
			'AddDocDesc' => ['3PPO添加材料说明' , ''], // 隐含动作
			'AddDoc' => ['添加材料' , ''], // 隐含动作
			'GoOfflineSurve' => ['进入线下调查' , ''], // 显现动作按钮
			'FinishReport' => ['3PPM提交报告' , ''], // 隐含动作
			'DueSignAccess' => ['尽调通过' , ''], // 显现动作按钮
			'DueSignDeny' => ['尽调不通过' , ''], // 显现动作按钮
			'DueSignAccessFast' => ['尽调通过' , ''], // 显现动作按钮
			'DueSignDenyFast' => ['尽调不通过' , ''], // 显现动作按钮

			/**
			 * 立项评审
			 */
			'review-wait' => ['等待评审' , ''], //
			'review-ing' => ['评审中' , ''], //
			'review-done' => ['完成评审' , ''], //
			'review-access' => ['评审通过' , ''], //
			'review-deny' => ['评审不通过' , ''], //


			'ProjectReview' => ['进入立项评审' , ''], // 按钮动作
			'SignReviewScoreFirst' => ['第一个专家开始评审' , ''], // 隐含的,事件触发
			'SignReviewScoreEnd' => ['最后一个专家结束评审' , ''], // 隐含的,事件触发
			'ActionReviewAccess' => ['评审通过' , ''], // 按钮动作
			'ActionReviewDeny' => ['评审不通过' , ''], // 按钮动作

			/**
			 * 优化项目
			 */
			'optimize-wait' => ['等待优化' , ''], //
			'optimize-ing' => ['优化中' , ''], //
			'optimize-done' => ['完成优化' , ''], //

			'ProjectOptimization' => ['启动项目优化' , ''], // 从评审通过状态 过渡 到 optimize-wait
			'ProjectOptimization2' => ['启动项目优化' , ''], // 从评审不通过状态 过渡 到 optimize-wait
			'CheckOptimizeIng' => ['优化中' , ''], // 从表单的redio判断
			'CheckOptimizeDone' => ['完成优化' , ''], // 从表单的redio判断

			/**
			 * 签约项目
			 */
			'proj-sign-wait' => ['准备签约' , ''],

			'ProjectSign' => ['进入签约' , ''], // 从最短路线来点击 的按钮
			'ProjectSign2' => ['进入签约' , ''], // 从 评审通过 来点击 的按钮

			/**
			 * 项目监测 大阶段 supervise
			 */
		];
	}
}
