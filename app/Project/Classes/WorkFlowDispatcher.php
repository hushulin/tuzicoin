<?php
namespace App\Project\Classes;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class WorkFlowDispatcher implements EventDispatcherInterface
{

	protected $events;

    /**
     * summary
     */
    public function __construct()
    {
    	$this->events = app('events');
    }

    public function dispatch($eventName, $event = null)
    {
    	event($eventName , $event);
    }

	public function addListener($eventName, $listener, $priority = 0)
	{

	}

	public function addSubscriber($subscriber)
	{

	}

	public function removeListener($eventName, $listener)
	{

	}

	public function removeSubscriber($subscriber)
	{

	}

	public function getListeners($eventName = null)
	{

	}

	public function getListenerPriority($eventName, $listener)
	{

	}
	public function hasListeners($eventName = null)
	{

	}
}
