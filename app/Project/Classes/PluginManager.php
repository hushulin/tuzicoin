<?php
namespace App\Project\Classes;

use App;
use File;
use Config;
use View;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class PluginManager
{
	use Concerns\Singleton;

	protected $app;

	protected $registered = false;

	protected $booted = false;

	protected $plugins;

	protected $pathMap = [];

	protected function init()
	{
		$this->bindContainerObjects();

		$this->loadPlugins();

	}

	public function bindContainerObjects()
    {
        $this->app = App::make('app');
    }

	public function loadPlugins()
	{
		$this->plugins = [];

        /**
         * Locate all plugins and binds them to the container
         */
        foreach ($this->getPluginNamespaces() as $namespace => $path) {
            $this->loadPlugin($namespace, $path);
        }

        return $this->plugins;
	}

	public function getPluginNamespaces()
    {
        $classNames = [];

        foreach ($this->getVendorAndPluginNames() as $vendorName => $vendorList) {
            foreach ($vendorList as $pluginName => $pluginPath) {
                $namespace = '\\'.$vendorName.'\\'.$pluginName;
                $namespace = Str::normalizeClassName($namespace);
                $classNames[$namespace] = $pluginPath;
            }
        }

        return $classNames;
    }

    public function getVendorAndPluginNames()
    {
        $plugins = [];

        $dirPath = base_path('app/Project/Plugins');

        if (!File::isDirectory($dirPath)) {
            return $plugins;
        }

        $it = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirPath, RecursiveDirectoryIterator::FOLLOW_SYMLINKS)
        );
        $it->setMaxDepth(2);
        $it->rewind();

        while ($it->valid()) {
            if (($it->getDepth() > 1) && $it->isFile() && (strtolower($it->getFilename()) == "plugin.php")) {
                $filePath = dirname($it->getPathname());
                $pluginName = basename($filePath);
                $vendorName = basename(dirname($filePath));
                $plugins[$vendorName][$pluginName] = $filePath;
            }

            $it->next();
        }

        return $plugins;
    }

	public function loadPlugin($namespace , $path)
	{
		// Fixed namespace
		$namespace_full = '\App\Project\Plugins' . $namespace;

		$className = $namespace_full.'\Plugin';
        $classPath = $path.'/Plugin.php';

        // Autoloader failed?
        if (!class_exists($className)) {
            include_once $classPath;
        }

        // Not a valid plugin!
        if (!class_exists($className)) {
            return;
        }

        $classObj = new $className($this->app);
        $classId = $this->getIdentifier($namespace);

        $this->plugins[$classId] = $classObj;
        $this->pathMap[$classId] = $path;

        return $classObj;
	}

	public function getIdentifier($namespace)
    {
        $namespace = Str::normalizeClassName($namespace);
        if (strpos($namespace, '\\') === null) {
            return $namespace;
        }

        $parts = explode('\\', $namespace);
        $slice = array_slice($parts, 1, 2);
        $namespace = implode('.', $slice);
        return $namespace;
    }

	public function getPlugins()
	{
		$all = $this->plugins;

		$valid = [];

		foreach ($all as $key => $value) {
			if (!$value->disabled) {
				$valid[$key] = $value;
			}
		}

		return $valid;
	}

	public function registerAll()
	{
		if ($this->registered) {
            return;
        }

        foreach ($this->plugins as $pluginId => $plugin) {
            $this->registerPlugin($plugin, $pluginId);
        }

        $this->registered = true;
	}

	public function registerPlugin($plugin , $pluginId)
	{
		if (!$pluginId) {
            $pluginId = $this->getIdentifier($plugin);
        }

        if (!$plugin) {
            return;
        }

        $pluginPath = $this->getPluginPath($pluginId);
        $pluginNamespace = strtolower($pluginId);

        $plugin->register();

        /*
         * Register views path
         */
        $viewsPath = $pluginPath . '/views';
        if (File::isDirectory($viewsPath)) {
            View::addNamespace($pluginNamespace, $viewsPath);
        }

        /*
         * Add routes, if available
         */
        $routesFile = $pluginPath . '/routes.php';
        if (File::exists($routesFile)) {
            require $routesFile;
        }

	}

	public function getPluginPath($id)
    {
        $classId = $this->getIdentifier($id);
        if (!isset($this->pathMap[$classId])) {
            return null;
        }

        return str_replace('\\', '/', $this->pathMap[$classId]);
    }

	public function bootAll()
	{
		if ($this->booted) {
            return;
        }

        foreach ($this->plugins as $plugin) {
            $this->bootPlugin($plugin);
        }

        $this->booted = true;
	}

	public function bootPlugin($plugin)
	{
		if (!$plugin || $plugin->disabled) {
            return;
        }

        $plugin->boot();

	}
}
