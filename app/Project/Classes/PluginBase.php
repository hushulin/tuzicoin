<?php
namespace App\Project\Classes;

use Illuminate\Support\ServiceProvider as ServiceProviderBase;

class PluginBase extends ServiceProviderBase
{

	public $disabled = false;

	public function pluginDetails()
    {
        $thisClass = get_class($this);

        $configuration = $this->getConfigurationFromYaml(sprintf('Plugin configuration file plugin.yaml is not '.
            'found for the plugin class %s. Create the file or override pluginDetails() '.
            'method in the plugin class.', $thisClass));

        if (!array_key_exists('plugin', $configuration)) {
            throw new SystemException(sprintf(
                'The plugin configuration file plugin.yaml should contain the "plugin" section: %s.', $thisClass)
            );
        }

        return $configuration['plugin'];
    }

	public function register()
    {
    }

    public function boot()
    {
    }

    public function registerIndex()
    {
    	# code...
    }

    public function registerMenu()
    {
    	# code...
    }

    public function registerFormSettings()
    {
    	# code...
    }

    public function registerProjectPlaces()
    {
        return [];
    }

    public function registerProjectTransition()
    {
        return [];
    }
}
