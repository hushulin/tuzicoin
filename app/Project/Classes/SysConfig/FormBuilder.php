<?php
namespace App\Project\Classes\SysConfig;

use Kris\LaravelFormBuilder\FormBuilder as BaseFormBuilder;
use Kris\LaravelFormBuilder\Form as BaseForm;

class FormBuilder
{

	private $builder;

	private $form;
	
	function __construct(BaseFormBuilder $builder , BaseForm $form)
	{
		$this->builder = $builder;
		$this->form = $form;

        // 是不是该加载自定义
        // 在配置文件里加载
	}

    // Unsupported field type [checkbox1]. Available types are: text, email, url, tel, search, password, hidden, number, date, file, image, color, datetime-local, month, range, time, week, select, textarea, button, buttongroup, submit, reset, radio, checkbox, choice, form, entity, collection, repeated, static
    // http://kristijanhusak.github.io/laravel-form-builder/form/basics.html#form-options
	public function createByArray($config)
	{

        $generated = $this->generate($config);

		return $this->builder->createByArray($generated
            ,[
            'method' => 'POST',
            'url' => url('song/store')
        ]);
	}

    protected function generate(array $config = [])
    {
        $generated = [];

        foreach ($config as $key => $value) {
            
            if ( empty($value['type']) ) {
                continue ;
            }

            switch ( $value['type'] ) {
                case 'string':

                    $generated[] = [
                        'type' => 'text',
                        'name' => $value['id'] ?? '',
                        'label' => $value['name'] ?? '',
                    ];
                    break;

                case 'date':

                    $generated[] = [
                        'type' => 'date',
                        'name' => $value['id'] ?? '',
                        'label' => $value['name'] ?? '',
                    ];
                    break;

                case 'table':
                    $generated[] = [
                        'type' => 'table',
                        'name' => $value['id'] ?? '',
                        'label' => $value['name'] ?? '',
                        'fields' => $this->generate($value['fields']) ?? [],
                    ];

                    break;
                
                default:
                    continue ;
                    break;
            }
        }

        return $generated;
    }
}
