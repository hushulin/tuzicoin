<?php
namespace App\Project\Classes\SysConfig;

class ProjectFormSchema
{
	
	function __construct()
	{
		# code...
	}

	public function grabSchemaData()
	{

		$data = [
			[
				'type' => 'string',
				'name' => '申报单位名称',
				'id' => 'shen-bao-dan-wei-ming-cheng',
			],
			[
				'type' => 'date',
				'name' => '有效日期',
				'id' => 'you-xiao-ri-qi',
			],
			[
				'type' => 'table',
				'name' => '过往经验',
				'id' => 'guo-wang-jing-yan',
				'fields' => [
					[
						'type' => 'string',
						'name' => '申报单位名称',
						'id' => 'shen-bao-dan-wei-ming-cheng',
					],
					[
						'type' => 'date',
						'name' => '有效日期',
						'id' => 'you-xiao-ri-qi',
					],
				],
			],
		];

		return $data;
	}
}
