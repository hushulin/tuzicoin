<?php
namespace App\Project\Classes\SysConfig\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class Table extends FormField
{
	
	protected function getTemplate()
    {
        // At first it tries to load config variable,
        // and if fails falls back to loading view
        // resources/views/fields/datetime.blade.php
        return 'fields.table';
    }

    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        $options['somedata'] = 'This is some data for view';

        return parent::render($options, $showLabel, $showField, $showError);
    }
}
