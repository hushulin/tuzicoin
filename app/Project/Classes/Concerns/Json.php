<?php

namespace App\Project\Classes\Concerns;

trait Json
{
	public function imArray($json , $options = 1)
	{
		$array = json_decode($json , $options);

		if (JSON_ERROR_NONE !== json_last_error()) {
            return [];
        }

        return $array;
	}

	public function imJson(array $data , $options = 0)
	{
		$json = json_encode($data, $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            return '';
        }

        return $json;
	}
}
