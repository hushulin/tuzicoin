<?php
namespace App\Project\Lib\Action;

use App\Project\Lib\Contracts\WorkFlowInterface;
use App\Project\Models\BizTopic;
use App\Project\Models\BizOrder;

class BizAcqWorkFlow implements WorkFlowInterface
{
    /**
     * summary
     */
    public function __construct()
    {

    }

    public function submitOrder($topic , $quantity)
    {

    	// 不管是下单还是拍下，都是waitPay 状态
        $place = 'waitPay';

        if ( $this->detectLimitQuantity($topic , $quantity) ) {

        	// buyer & seller define buyer auth()->user() seller topic creator
	        $order = BizOrder::create([

	            'buyer_id' => auth()->user()->id,
	            'seller_id' => $topic->creator_id,

	            'topic_id' => $topic->id,
	            'place' => $place,

	            'quantity' => $quantity,

	            // redundant
	            'price' => $topic->cell->price,
	            'amount' => $topic->cell->price * $quantity,
	        ]);

	        return [
	        	'status' => 0,
	        	'data' => $order->id,
	        ];
        }else {

        	return [
        		'status' => 4,
        		'msg' => '拍下数量超过',
        	];
        }


    }

    protected function detectLimitQuantity($topic , $quantity)
    {
    	$total = BizOrder::where('topic_id' , $topic->id)->sum('quantity');
    	return ($total + $quantity) <= $topic->cell->acq_num;
    }
}
