<?php
namespace App\Project\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * Advert object publish adv
 */
class BizSellVg extends Model
{
    protected $table = 'biz_sell_vgs';

	protected $fillable = [
		'good_name',
		'stock_num',
		'price',
		'contact',
		'min_limit',
		'max_limit',
		'img',
		'send_addr',
		'notes',
	];

	// public $timestamps = true;

	// protected $appends = [

	// ];

    public function topic()
    {
    	return $this->morphMany('App\Project\Models\BizTopic', 'cell');
    }
}
