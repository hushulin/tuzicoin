<?php
namespace App\Project\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * Advert object publish adv
 */
class BizTopic extends Model
{
    protected $table = 'biz_topics';

	protected $fillable = [
        'ad_area',
        'ad_type',
        'title',
        'creator_id',
	];

	// public $timestamps = true;

	// protected $appends = [

	// ];

    /**
     * Get all of the owning commentable models.
     * cell: sell/acq , ag/vg
     */
    public function cell()
    {
        return $this->morphTo();
    }
}
