<?php
namespace App\Project\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * Advert object publish adv
 */
class BizSellAg extends Model
{
    protected $table = 'biz_sell_ags';

	protected $fillable = [
		'good_name',
		'stock_num',
		'price',
		'contact',
		'img',
		'send_addr',
		'notes',
	];

	// public $timestamps = true;

	// protected $appends = [

	// ];

    public function topic()
    {
    	return $this->morphMany('App\Project\Models\BizTopic', 'cell');
    }
}
