<?php
namespace App\Project\Models;

use Illuminate\Database\Eloquent\Model;
use App\Project\Models\WorkFlow\InitWorkFlowInstance;
use App\Models\User;
use App\Models\Message;

/**
 *
 * Advert object publish adv
 */
class BizOrder extends Model
{

	use Traits\WorkFlow;

    protected $table = 'orders';

	protected $fillable = [
		'buyer_id',
		'seller_id',
		'topic_id',
		'place',
		'quantity',
		'price',
		'amount',
	];

	public $timestamps = true;

	// protected $appends = [

	// ];

	protected $casts = [
		'expires_at' => 'datetime',
		'buyer_id' => 'integer',
		'seller_id' => 'integer'
	];

	// 需要在项目抓取数据库数据集后做一些操作
	protected $dispatchesEvents = [
		'retrieved' => InitWorkFlowInstance::class,
    ];

    public function topic()
    {
    	return $this->belongsTo(BizTopic::class);
    }

    /**
	 * 买家
	 */
	public function buyer()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 卖家
	 */
	public function seller()
	{
		return $this->belongsTo(User::class);
	}

	/**
	 * 消息列表
	 */
	public function messages()
	{
		return $this->hasMany(Message::class , 'order_id');
	}

	/**
	 * 所属用户的订单
	 */
	public function scopeByUser($query, $user)
	{
		return $query->where('buyer_id', $user->id)->orWhere('seller_id', $user->id);
	}
}
