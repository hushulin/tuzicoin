<?php
namespace App\Project\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * Advert object publish adv
 */
class BizAcqVg extends Model
{
    protected $table = 'biz_acq_vgs';

	protected $fillable = [
		'good_name',
		'acq_num',
		'price',
		'contact',
		'img',
		'acq_addr',
		'notes',
	];

	public $timestamps = true;

	// protected $appends = [

	// ];

    public function topic()
    {
    	return $this->morphMany('App\Project\Models\BizTopic', 'cell');
    }
}
