<?php
namespace App\Project\Models\Traits;

use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\Workflow as BaseWorkFlow;
use Symfony\Component\Workflow\MarkingStore\SingleStateMarkingStore;
use App\Project\Classes\PluginManager;
use App\Project\Classes\Mix;
use App\Project\Classes\WorkFlowDispatcher;

trait WorkFlow {

	public $wfInstance;

	public function makeWorkFlowInstance($blueprint)
	{
		$definitionBuilder = new DefinitionBuilder();

		$definitionBuilder = $definitionBuilder->addPlaces($this->grabOrderdPlace($blueprint));

		foreach ($this->grabBaseTransition($blueprint) as $key => $transition) {
			$definitionBuilder = $definitionBuilder->addTransition(new Transition(...$transition));
		}

		$definition = $definitionBuilder->build();

		$marking = new SingleStateMarkingStore('customMark');

		$workflow = new BaseWorkFlow($definition, $marking , null , $blueprint);

		// 这个时候给当前位置也是可以的
		// 从数据库模型中读当前地点赋值
		$this->customMark = $this->place;

		$this->wfInstance = $workflow;

	}

	protected function grabOrderdPlace($blueprint)
	{
		switch ($blueprint) {
			case 'AcqWorkFlow':
				$places = [
					'auction',
					'waitPay',
					'waitDisp',
					'waitRecv',
					'done',
					'cancel',
				];
				break;

			case 'SellWorkFlow':
				$places = [
					'start', //
					'waitPay',
					'waitDisp',
					'waitRecv',
					'done',
					'cancel',
				];
				break;

			default:
				$places = [
					// 暂无内置的地点
				];
				break;
		}


		// 从插件里收集
		$plugins = PluginManager::instance()->getPlugins();

		foreach ($plugins as $key => $plugin) {

			$plugin_places = (array)$plugin->registerProjectPlaces();

			$places = array_merge($places , $plugin_places);
		}

		return $places;
	}

	protected function grabBaseTransition($blueprint)
	{

		switch ($blueprint) {
			case 'AcqWorkFlow':
				$transitions = [
					['AuctionOrder' , 'auction' , 'waitPay'],

					['AcqerPayOrder' , 'waitPay' , 'waitDisp'],
					['CancelOn_waitPay' , 'waitPay' , 'cancel'],

					['SellerSendGoods' , 'waitDisp' , 'waitRecv'],
					['CancelOn_waitDisp' , 'waitDisp' , 'cancel'],

					['AcqerGetGoods' , 'waitRecv' , 'done'],
					['CancelOn_waitRecv' , 'waitRecv' , 'cancel'],
				];
				break;

			case 'SellWorkFlow':
				$transitions = [
					['SubmitOrder' , 'start' , 'waitPay'],

					['PayOrder' , 'waitPay' , 'waitDisp'],
					['CancelOn_waitPay' , 'waitPay' , 'cancel'],

					['SendGoods' , 'waitDisp' , 'waitRecv'],
					['CancelOn_waitDisp' , 'waitDisp' , 'cancel'],

					['GetGoods' , 'waitRecv' , 'done'],
					['CancelOn_waitRecv' , 'waitRecv' , 'cancel'],
				];
				break;

			default:
				$transitions = [
					// 暂无内置的过渡
				];
				break;
		}


		// 也尝试从插件里收集
		$plugins = PluginManager::instance()->getPlugins();

		foreach ($plugins as $key => $plugin) {

			$plugin_transitions = (array)$plugin->registerProjectTransition();

			$transitions = array_merge($transitions , $plugin_transitions);
		}

		return $transitions;
	}

}
