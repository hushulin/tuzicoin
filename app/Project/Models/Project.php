<?php
namespace App\Project\Models;

use Illuminate\Database\Eloquent\Model;
use App\Project\Models\WorkFlow\InitWorkFlowInstance;
use App\Models\Plan;

class Project extends Model
{

	use Traits\WorkFlow;

	// 需要在项目抓取数据库数据集后做一些操作
	protected $dispatchesEvents = [
		'retrieved' => InitWorkFlowInstance::class,
    ];

    // 废弃
    // 项目上面是需求目录,需求目录上面才是计划
    public function plan()
    {
    	return $this->belongsTo(Plan::class , 'plan_id');
    }

    // 需求目录
    public function demand()
    {
    	return $this->belongsTo(Demand::class , 'demand_id');
    }
}
