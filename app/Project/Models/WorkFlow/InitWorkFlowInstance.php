<?php
namespace App\Project\Models\WorkFlow;

use App\Project\Models\BizOrder;
use App\Project\Models\BizTopic;
use Illuminate\Support\Facades\Log;
use App\Project\Misc\CellTypeConst;
use Exception;

class InitWorkFlowInstance
{

	function __construct(BizOrder $order)
	{

		// 这里来的实例是一个一个的实例

		// Log::info($order);
		try {

			$topic = BizTopic::findOrFail($order->topic_id);

			$workflow = CellTypeConst::instance()->detectWorkFlow($topic->ad_type , $topic->ad_area);

		} catch (Exception $e) {
			$workflow = CellTypeConst::instance()->detectWorkFlow();
		}


		$order->makeWorkFlowInstance($workflow);


		// workflow不能转string
		// Log::info($order->wfInstance);

		// 非数据库字段不会出现在这里
		// Log::info($order);
	}
}
