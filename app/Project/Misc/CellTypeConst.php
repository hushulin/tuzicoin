<?php
namespace App\Project\Misc;

/**
 * Cell Type Constanst
 */
class CellTypeConst
{

    use \App\Project\Classes\Concerns\Singleton;

    protected $models;

    protected $workFlow;

    protected function init()
    {

    	// ad_type 0 acq | 1 sell
    	// ad_area 0 vitual | 1 actual
    	$this->models = [
    		'0-0' => '\App\Project\Models\BizAcqVg',
    		'0-1' => '\App\Project\Models\BizAcqAg',
    		'1-0' => '\App\Project\Models\BizSellVg',
    		'1-1' => '\App\Project\Models\BizSellAg',
    	];

        $this->workFlow = [
            '0-0' => 'AcqWorkFlow',
            '0-1' => 'AcqWorkFlow',
            '1-0' => 'SellWorkFlow',
            '1-1' => 'SellWorkFlow',
        ];
    }


    public function detectCellModel($ad_type = 0, $ad_area = 0)
    {

    	$key = (int)$ad_type . '-' . (int)$ad_area;

    	if (array_key_exists($key, $this->models)) {
            return $this->models[$key];
        } else {
            return '';
        }
    }

    public function detectWorkFlow($ad_type = 0 , $ad_area = 0)
    {
        $key = (int)$ad_type . '-' . (int)$ad_area;

        if (array_key_exists($key, $this->workFlow)) {
            return $this->workFlow[$key];
        } else {
            return '';
        }
    }
}
