<?php
namespace App\Project\Misc;

/**
 * Cell Type Constanst
 */
class WorkFlowReadable
{

    use \App\Project\Classes\Concerns\Singleton;

    protected function init()
    {
        $this->express = [

            'seller-sellworkflow-start' => '出售',
            'seller-sellworkflow-waitPay' => '待支付',
            'seller-sellworkflow-waitDisp' => '待发货',
            'seller-sellworkflow-waitRecv' => '待收货',
            'seller-sellworkflow-done' => '完成',
            'seller-sellworkflow-cancel' => '取消',
            'seller-sellworkflow-SubmitOrder' => '下单',
            'seller-sellworkflow-PayOrder' => '等待买家支付',
            'seller-sellworkflow-CancelOn_waitPay' => '取消支付',
            'seller-sellworkflow-SendGoods' => '发货',
            'seller-sellworkflow-CancelOn_waitDisp' => '取消发货',
            'seller-sellworkflow-GetGoods' => '等待买家收货',
            'seller-sellworkflow-CancelOn_waitRecv' => '取消（如果买家没有收到货要求取消）',

            'seller-acqworkflow-auction' => '求购',
            'seller-acqworkflow-waitPay' => '待支付',
            'seller-acqworkflow-waitDisp' => '待发货',
            'seller-acqworkflow-waitRecv' => '待收货',
            'seller-acqworkflow-done' => '完成',
            'seller-acqworkflow-cancel' => '取消',
            'seller-acqworkflow-AuctionOrder' => '拍下',
            'seller-acqworkflow-AcqerPayOrder' => '支付',
            'seller-acqworkflow-CancelOn_waitPay' => '取消支付',
            'seller-acqworkflow-SellerSendGoods' => '等待拍下者发货',
            'seller-acqworkflow-CancelOn_waitDisp' => '取消发货',
            'seller-acqworkflow-AcqerGetGoods' => '收货',
            'seller-acqworkflow-CancelOn_waitRecv' => '取消收货',

            'buyer-sellworkflow-start' => '下单',
            'buyer-sellworkflow-waitPay' => '待支付',
            'buyer-sellworkflow-waitDisp' => '待发货',
            'buyer-sellworkflow-waitRecv' => '待收货',
            'buyer-sellworkflow-done' => '完成',
            'buyer-sellworkflow-cancel' => '取消',
            'buyer-sellworkflow-SubmitOrder' => '下单',
            'buyer-sellworkflow-PayOrder' => '支付',
            'buyer-sellworkflow-CancelOn_waitPay' => '取消支付',
            'buyer-sellworkflow-SendGoods' => '等待卖家发货',
            'buyer-sellworkflow-CancelOn_waitDisp' => '取消发货',
            'buyer-sellworkflow-GetGoods' => '收货',
            'buyer-sellworkflow-CancelOn_waitRecv' => '取消收货',

            'buyer-acqworkflow-auction' => '求购',
            'buyer-acqworkflow-waitPay' => '待支付',
            'buyer-acqworkflow-waitDisp' => '待发货',
            'buyer-acqworkflow-waitRecv' => '待收货',
            'buyer-acqworkflow-done' => '完成',
            'buyer-acqworkflow-cancel' => '取消',
            'buyer-acqworkflow-AuctionOrder' => '拍下',
            'buyer-acqworkflow-AcqerPayOrder' => '等待求购者支付',
            'buyer-acqworkflow-CancelOn_waitPay' => '取消支付',
            'buyer-acqworkflow-SellerSendGoods' => '发货',
            'buyer-acqworkflow-CancelOn_waitDisp' => '取消发货',
            'buyer-acqworkflow-AcqerGetGoods' => '等待求购者收货',
            'buyer-acqworkflow-CancelOn_waitRecv' => '取消收货',


        ];

        $this->can = [

            'seller-sellworkflow-SubmitOrder' => false,
            'seller-sellworkflow-PayOrder' => false,
            'seller-sellworkflow-CancelOn_waitPay' => false,
            'seller-sellworkflow-SendGoods' => true,
            'seller-sellworkflow-CancelOn_waitDisp' => true,
            'seller-sellworkflow-GetGoods' => false,
            'seller-sellworkflow-CancelOn_waitRecv' => true,

            'seller-acqworkflow-AuctionOrder' => false,
            'seller-acqworkflow-AcqerPayOrder' => true,
            'seller-acqworkflow-CancelOn_waitPay' => true,
            'seller-acqworkflow-SellerSendGoods' => false,
            'seller-acqworkflow-CancelOn_waitDisp' => false,
            'seller-acqworkflow-AcqerGetGoods' => true,
            'seller-acqworkflow-CancelOn_waitRecv' => true,

            'buyer-sellworkflow-SubmitOrder' => true,
            'buyer-sellworkflow-PayOrder' => true,
            'buyer-sellworkflow-CancelOn_waitPay' => true,
            'buyer-sellworkflow-SendGoods' => false,
            'buyer-sellworkflow-CancelOn_waitDisp' => false,
            'buyer-sellworkflow-GetGoods' => true,
            'buyer-sellworkflow-CancelOn_waitRecv' => true,

            'buyer-acqworkflow-AuctionOrder' => true,
            'buyer-acqworkflow-AcqerPayOrder' => false,
            'buyer-acqworkflow-CancelOn_waitPay' => false,
            'buyer-acqworkflow-SellerSendGoods' => true,
            'buyer-acqworkflow-CancelOn_waitDisp' => true,
            'buyer-acqworkflow-AcqerGetGoods' => false,
            'buyer-acqworkflow-CancelOn_waitRecv' => false,


        ];
    }

    public function readEnables($order)
    {
        $enables = $order->wfInstance->getEnabledTransitions($order);

        $is_seller = auth()->user()->id == $order->seller_id;

        $ret = [];
        foreach ($enables as $key => $value) {
            $ret[$key]['name'] = $value->getName();
            $ret[$key]['from'] = $value->getFroms()[0];
            $ret[$key]['to'] = $value->getTos()[0];

            if ( $is_seller ) {
                if ( $order->wfInstance->getName() == 'SellWorkFlow' ) {

                    $ret[$key]['do'] = $this->getDo('seller' , 'sellworkflow' , $ret[$key]['name']);
                    $ret[$key]['code'] = $this->getCode('seller' , 'sellworkflow' , $ret[$key]['name']);
                    $ret[$key]['name_zh'] = $this->getZh('seller' , 'sellworkflow' , $ret[$key]['name']);
                    $ret[$key]['from_zh'] = $this->getZh('seller' , 'sellworkflow' , $ret[$key]['from']);
                    $ret[$key]['to_zh'] = $this->getZh('seller' , 'sellworkflow' , $ret[$key]['to']);
                    continue;
                }

                if ( $order->wfInstance->getName() == 'AcqWorkFlow' ) {

                    $ret[$key]['do'] = $this->getDo('seller' , 'acqworkflow' , $ret[$key]['name']);
                    $ret[$key]['code'] = $this->getCode('seller' , 'acqworkflow' , $ret[$key]['name']);
                    $ret[$key]['name_zh'] = $this->getZh('seller' , 'acqworkflow' , $ret[$key]['name']);
                    $ret[$key]['from_zh'] = $this->getZh('seller' , 'acqworkflow' , $ret[$key]['from']);
                    $ret[$key]['to_zh'] = $this->getZh('seller' , 'acqworkflow' , $ret[$key]['to']);

                    continue;
                }

            }else {

                if ( $order->wfInstance->getName() == 'SellWorkFlow' ) {

                    $ret[$key]['do'] = $this->getDo('buyer' , 'sellworkflow' , $ret[$key]['name']);
                    $ret[$key]['code'] = $this->getCode('buyer' , 'sellworkflow' , $ret[$key]['name']);
                    $ret[$key]['name_zh'] = $this->getZh('buyer' , 'sellworkflow' , $ret[$key]['name']);
                    $ret[$key]['from_zh'] = $this->getZh('buyer' , 'sellworkflow' , $ret[$key]['from']);
                    $ret[$key]['to_zh'] = $this->getZh('buyer' , 'sellworkflow' , $ret[$key]['to']);

                    continue;
                }

                if ( $order->wfInstance->getName() == 'AcqWorkFlow' ) {

                    $ret[$key]['do'] = $this->getDo('buyer' , 'acqworkflow' , $ret[$key]['name']);
                    $ret[$key]['code'] = $this->getCode('buyer' , 'acqworkflow' , $ret[$key]['name']);
                    $ret[$key]['name_zh'] = $this->getZh('buyer' , 'acqworkflow' , $ret[$key]['name']);
                    $ret[$key]['from_zh'] = $this->getZh('buyer' , 'acqworkflow' , $ret[$key]['from']);
                    $ret[$key]['to_zh'] = $this->getZh('buyer' , 'acqworkflow' , $ret[$key]['to']);

                    continue;
                }
            }
        }

        return $ret;
    }

    protected function getDo($one , $two , $three)
    {
        $key = $one . '-' . $two . '-' . $three;

        if (array_key_exists($key, $this->can)) {
            return $this->can[$key];
        } else {
            return false;
        }
    }
    protected function getCode($one , $two , $three)
    {
        return $one . '-' . $two . '-' . $three;
    }

    protected function getZh($one , $two , $three)
    {
        $key = $one . '-' . $two . '-' . $three;

        if (array_key_exists($key, $this->express)) {
            return $this->express[$key];
        } else {
            return '';
        }
    }
}
