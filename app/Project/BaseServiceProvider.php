<?php
namespace App\Project;

use Illuminate\Support\ServiceProvider;
use App\Project\Classes\PluginManager;

class BaseServiceProvider extends ServiceProvider
{

	/**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->setupRoutes();
        $this->setupMenus();

        PluginManager::instance()->bootAll();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        PluginManager::instance()->registerAll();

        $plugins = PluginManager::instance()->getPlugins();

    }

    protected function setupRoutes()
    {
        require base_path('app/Project/Http/routes.php');
    }

    protected function setupMenus()
    {
        // Empty
        return ;
    }
}
