<?php
namespace App\Validator;

use Illuminate\Validation\Validator;
use PragmaRX\Google2FA\Google2FA;
use cszchen\citizenid\Parser;
use App\Models\User;
use Cache;
use Hash;
use Auth;

/**
 * 自定义验证器
 *
 * @author  
 *
 */
class CustomValidator extends Validator
{

	/**
	 * 检查是否是有效的比特币地址
	 * 通过验证字符串是否能正确的进行 Base58Check 解码
	 */
	public function validateBase58check($attribute, $value, $parameters)
	{
		return check_address($value);
	}

	/**
	 * 检查当前登录会员的密码
	 */
	public function validatePassword($attribute, $value, $parameters)
	{
		if (Auth::guest()) {
			return false;
		}
		return Hash::check($value, Auth::user()->password);
	}

	/**
	 * 检查当前登录会员的二级密码
	 */
	public function validateSecondaryPassword($attribute, $value, $parameters)
	{
		if (Auth::guest()) {
			return false;
		}
		return Hash::check($value, Auth::user()->secondary_password);
	}

	/**
	 * 检查当前登录会员的谷歌二步验证码
	 */
	public function validateGoogle2fa($attribute, $value, $parameters)
	{
		if (Auth::guest()) {
			return false;
		}

		// 未启用则直接通过验证。
		if (! Auth::user()->google2fa_secret) {
			return true;
		}

		return (new Google2FA())->verifyKey(Auth::user()->google2fa_secret, $value);
	}

	/**
	 * 检查操作验证码
	 */
	public function validateOpcode($attribute, $value, $parameters)
	{
		$this->requireParameterCount(1, $parameters, 'opcode');

		$key = Auth::id() . '-' . $parameters[0];
		$code = Cache::get($key);
		Cache::forget($key);
		return $value === $code;
	}

	/**
	 * 检查短信验证码是否正确
	 */
	public function validateSmsverify($attribute, $value, $parameters)
	{
		$mobile = 'mobile';
		$country_code = 'country_code';
		if (isset($parameters[0])) {
			$mobile = $parameters[0];
		}
		if (isset($parameters[1])) {
			$country_code = $parameters[1];
		}
		if (! preg_match('/^[*#0-9]+$/', $mobile)) {
			$mobile = array_get($this->data, $mobile);
		}
		if (! preg_match('/^[a-z]{2}$/i', $country_code)) {
			$country_code = array_get($this->data, $country_code);
		}

		// 允许调试模式下，使用手机号后六位直接过验证。
		if (config('app.debug') && $value == substr($mobile, - 6)) {
			return true;
		}

		// 从缓存中取出短信验证码。
		$key = 'Verify@smscode:' . $country_code . '-' . $mobile;
		$sms = Cache::get($key);

		// 取得校验结果。
		$result = $sms && $sms['code'] === $value;

		// 处理失效。
		Cache::forget($key);

		// 返回结果。
		return $result;
	}

	/**
	 * 检查邮件验证码是否正确
	 */
	public function validateEmailverify($attribute, $value, $parameters)
	{
		$email = 'email';
		if (isset($parameters[0])) {
			$email = $parameters[0];
		}
		if (array_key_exists($email, $this->data)) {
			$email = array_get($this->data, $email);
		}

		// 允许调试模式下，使用邮箱前六位直接过验证。
		if (config('app.debug') && $value == substr($email, 0, 6)) {
			return true;
		}

		// 从缓存中取出短信验证码。
		$key = 'Verify@emailcode:' . md5($email);
		$email = Cache::get($key);

		// 取得校验结果。
		$result = $email && $email['code'] === $value;

		// 处理失效。
		Cache::forget($key);

		// 返回结果。
		return $result;
	}

	/**
	 * 身份证号码格式是否正确
	 */
	public function validateCitizenid($attribute, $value, $parameters)
	{
		$parser = new Parser();
		$parser->setId($value);
		return $parser->isValidate();
	}

	/**
	 * 检查字段值是否大于另一个字段值
	 */
	public function validateGt($attribute, $value, $parameters)
	{
		$this->requireParameterCount(1, $parameters, 'egt');

		return $value > array_get($this->data, $parameters[0]);
	}

	public function replaceGt($message, $attribute, $rule, $parameters)
	{
		return str_replace(':other', $this->getDisplayableAttribute($parameters[0]), $message);
	}
}