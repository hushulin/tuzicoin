# 比特币场外交易（OTC）平台

演示地址: [https://btcotc.latrell.me](https://btcotc.latrell.me)


注意：阿里云与极速API的短信接口，都只能发送国内短信。

### 前端获取路由地址

命令 `php artisan route:json` 会将公开路由表生成一个JSON文件放到资源目录，以供前端代码使用 `route` 函数获取路由URI配置。
当后台修改了路由表后，若前端需要调用后端路由地址，需要运行该命令刷新前端的路由表JSON。