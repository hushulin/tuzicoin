<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory;
use Illuminate\Http\UploadedFile;
use App\Models\User;

class TuziTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testBTPublish()
    {
    	$faker = Factory::create();

        $user = User::findOrFail(272705);

    	$response = $this->actingAs($user)->json('POST' , '/biz/bt/publish' , [
    		'ad_area' => $faker->boolean(), // 0 , 1
    		'ad_type' => $faker->boolean(), // 0 , 1
    		'title' => $faker->title,

    		'good_name' => $faker->title,
			'price' => $faker->randomFloat(2),
			'contact' => $faker->address,
			'img' => UploadedFile::fake()->image('avatar.jpg'),
			'notes' => $faker->text,
    	]);

        $response->assertStatus(200);
    }

    public function testBTLists()
    {
    	$response = $this->json('GET' , '/biz/bt/lists' , [
    		'page' => 1,
    		'page_size' => 2,
    	]);

    	$response->assertStatus(200);
    	$response->assertJsonStructure(['status' , 'data' , 'total']);
    }

    // 测试订单下单/拍下
    public function testBizOrderSubmit()
    {

        $user = User::findOrFail(272705);

        $response = $this->actingAs($user)->json('POST' , '/biz/bizorder/submit' , [
            'topic_id' => 1,
            'quantity' => 10,
        ]);

        $response->assertStatus(200);

    }

    // 测试订单操作
    public function testBizOrderOperate()
    {

        $user = User::findOrFail(272705);

        $response = $this->actingAs($user)->json('POST' , '/biz/bizorder/operate' , [
            'order_id' => 68,
            'code' => 'seller-sellworkflow-GetGoods',
        ]);

        var_dump($response);

        $response->assertStatus(200);

    }


    // 测试工作流引擎的完备性
    public function testWorkFlowAvailable()
    {
        $response = $this->json('GET' , '/biz/bizorder/testworkflow');

        $response->assertStatus(200);
    }

    public function testOrderDetail()
    {

        $user = User::findOrFail(272705);

        $response = $this->actingAs($user)->json('GET' , '/biz/bizorder/68');

        var_dump($response);

        $response->assertStatus(200);
    }

}
