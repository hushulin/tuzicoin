<?php
use Illuminate\Database\Seeder;

/**
 * 抓取市场价
 *
 * @author Latrell Chan
 *
 */
class MarketPricesTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Artisan::call('fetch:market-price');
	}
}
