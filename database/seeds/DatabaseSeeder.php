<?php
use Illuminate\Database\Seeder;

/**
 * 填充初始数据
 *
 * @author Latrell Chan
 *
 */
class DatabaseSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(CountriesTableSeeder::class);
		$this->call(CurrenciesTableSeeder::class);
		$this->call(ConfigsTableSeeder::class);
		$this->call(AdminsTableSeeder::class);
		$this->call(MarketPricesTableSeeder::class);
		$this->call(ExchangeRatesTableSeeder::class);
		$this->call(AddressesTableSeeder::class);
		//$this->call(UsersTableSeeder::class);
	}
}
