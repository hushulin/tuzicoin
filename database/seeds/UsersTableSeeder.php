<?php
use Illuminate\Database\Seeder;

use App\Models\User;

/**
 * 测试用户账号
 *
 * @author Latrell Chan
 *
 */
class UsersTableSeeder extends Seeder
{

	public function run()
	{
		factory(User::class, 10)->create();
	}
}
