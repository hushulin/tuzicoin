<?php
use Illuminate\Database\Seeder;

/**
 * 抓取法币汇率
 *
 * @author Latrell Chan
 *
 */
class ExchangeRatesTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Artisan::call('fetch:exchange-rate');
	}
}
