<?php
use Illuminate\Database\Seeder;
use App\Models\Address;

/**
 * 钱包地址库
 *
 * @author Latrell Chan
 *
 */
class AddressesTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// 比特币地址。
		$data = [
		];
		foreach ($data as $address) {
			Address::create([
				'coin' => Address::COIN_BTC,
				'address' => $address
			]);
		}

		// 莱特币地址。
		$data = [
		];
		foreach ($data as $address) {
			Address::create([
				'coin' => Address::COIN_LTC,
				'address' => $address
			]);
		}
	}
}
