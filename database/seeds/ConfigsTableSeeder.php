<?php
use Illuminate\Database\Seeder;
use App\Models\Config;

/**
 * 系统配置
 *
 * @author Latrell Chan
 *
 */
class ConfigsTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Config::truncate();

		$data = [
			[
				'type' => Config::TYPE_MULTIPLE_OPTION,
				'key' => 'coins',
				'value' => [
					'BTC',
					'LTC'
				],
				'name' => '币种列表',
				'description' => '系统启用哪些币种交易。',
				'rules' => 'required',
				'options' => [
					'BTC' => 'BTC',
					'LTC' => 'LTC'
				]
			],
			[
				'key' => 'confirmations_btc',
				'value' => '1',
				'name' => '入账确认数（BTC）',
				'unit' => '个确认',
				'description' => '收到比特币网络多少个确认后再入账。',
				'rules' => 'required|numeric|min:0'
			],
			[
				'key' => 'confirmations_ltc',
				'value' => '1',
				'name' => '入账确认数（LTC）',
				'unit' => '个确认',
				'description' => '收到莱特币网络多少个确认后再入账。',
				'rules' => 'required|numeric|min:0'
			],
			[
				'key' => 'network_out_fee_btc',
				'value' => '0.001',
				'name' => '网络转出手续费（BTC）',
				'unit' => 'BTC',
				'description' => '从钱包发送BTC到平台外地址时收取的手续费。',
				'rules' => 'required|numeric|min:0'
			],
			[
				'key' => 'network_out_fee_ltc',
				'value' => '0.001',
				'name' => '网络转出手续费（LTC）',
				'unit' => 'LTC',
				'description' => '从钱包发送LTC到平台外地址时收取的手续费。',
				'rules' => 'required|numeric|min:0'
			],
			[
				'key' => 'blockchain_view_btc',
				'value' => 'https://btc.com/%s',
				'name' => '区块链查看（BTC）',
				'description' => '用于查看比特币区块链信息的第三方平台地址。https://example.com/tx/%s',
				'rules' => 'required|url'
			],
			[
				'key' => 'blockchain_view_ltc',
				'value' => 'http://qukuai.com/search/ltc/tx/%s',
				'name' => '区块链查看（LTC）',
				'description' => '用于查看莱特币区块链信息的第三方平台地址。https://example.com/tx/%s',
				'rules' => 'required|url'
			],
			[
				'key' => 'min_show_amount_btc',
				'value' => '0.05',
				'name' => '柜台展示余额限制（BTC）',
				'description' => '广告列表里要求钱包至少要有多少比特币可用余额才给予展示。若配置为零，则相当于关闭该限制。',
				'unit' => 'BTC',
				'rules' => 'required|numeric|min:0'
			],
			[
				'key' => 'min_show_amount_ltc',
				'value' => '0.05',
				'name' => '柜台展示余额限制（LTC）',
				'description' => '广告列表里要求钱包至少要有多少莱特币可用余额才给予展示。若配置为零，则相当于关闭该限制。',
				'unit' => 'LTC',
				'rules' => 'required|numeric|min:0'
			],
			[
				'key' => 'counter_fee',
				'value' => '0.7',
				'name' => '柜台手续费',
				'unit' => '%',
				'description' => '比如手续费 0.7% ，购买 1 BTC 后，实际到账 0.993 BTC 。若是出售，则需要扣除账户 1.007 BTC 的可用余额。',
				'rules' => 'required|numeric|between:0,100'
			],
			[
				'key' => 'min_amount',
				'value' => '20',
				'name' => '最小限额',
				'unit' => '法币',
				'description' => '创建柜台时的最低最小限额限制，单位是法币，比如CNY或USD等。',
				'rules' => 'required|numeric|min:1'
			]
		];
		foreach ($data as $weight => $attributes) {
			Config::create(array_merge($attributes, [
				'weight' => $weight
			]));
		}
	}
}
