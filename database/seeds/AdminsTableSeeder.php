<?php
use Illuminate\Database\Seeder;
use App\Models\Admin;

/**
 * 初始管理员账号
 *
 * @author Latrell Chan
 *
 */
class AdminsTableSeeder extends Seeder
{

	public function run()
	{
		// 用户。
		$admin = new Admin();
		$admin->username = 'root';
		$admin->password = '123456';
		$admin->save();
	}
}
