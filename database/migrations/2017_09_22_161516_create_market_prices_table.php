<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 最新比特币市场价
 *
 * @author Latrell Chan
 *
 */
class CreateMarketPricesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('market_prices', function (Blueprint $table) {
			$table->comment = '最新比特币市场价';
			$table->increments('id');
			$table->string('market', 50)->comment('市场');
			$table->string('coin', 3)->comment('币种');
			$table->string('currency', 3)->comment('货币');
			$table->decimal('last', 15, 2)->default(0)->comment('最新价');
			$table->decimal('buy', 15, 2)->default(0)->comment('最低要价(买一价)');
			$table->decimal('sell', 15, 2)->default(0)->comment('最高出价(卖一价)');
			$table->timestamps();
			$table->unique([
				'market',
				'coin',
				'currency'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('market_prices');
	}
}
