<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Deposit;

/**
 * 存款
 *
 * @author Latrell Chan
 *
 */
class CreateDepositsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deposits', function (Blueprint $table) {
			$table->comment = '存款';
			$table->increments('id');
			$table->enum('coin', [
				Deposit::COIN_BTC,
				Deposit::COIN_LTC
			])->comment('币种');
			$table->string('txid', 64)->comment('所在区块Hash');
			$table->unsignedInteger('user_id')->index()->comment('所属用户');
			$table->decimal('amount', 15, 8)->default(0)->comment('金额');
			$table->unsignedInteger('confirmations')->default(0)->comment('确认数');
			$table->timestamps();
			$table->unique([
				'coin',
				'txid'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('deposits');
	}
}
