<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIgnoresTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ignores', function (Blueprint $table) {
			$table->comment = '黑名单';
			$table->increments('id');
			$table->unsignedInteger('source_id')->index()->comment('来源用户');
			$table->unsignedInteger('target_id')->index()->comment('目标用户');
			$table->integer('trade_count')->default(0)->comment('交易次数');
			$table->timestamps();
			$table->unique([
				'source_id',
				'target_id'
			]);
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('ignores');
	}
}
