<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Counter;

/**
 * 柜台
 *
 * @author Latrell Chan
 *
 */
class CreateCountersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('counters', function (Blueprint $table) {
			$table->comment = '柜台';
			$table->increments('id');
			$table->unsignedInteger('user_id')->index()->comment('所属会员');
			$table->enum('coin', [
				Counter::COIN_BTC,
				Counter::COIN_LTC
			])->comment('币种');
			$table->enum('status', [
				Counter::STATUS_OPEN,
				Counter::STATUS_CLOSED
			])->default(Counter::STATUS_OPEN)->index()->comment('状态');
			$table->enum('type', [
				Counter::TYPE_ONLINE_BUY,
				Counter::TYPE_ONLINE_SELL
			])->index()->comment('类型');
			$table->char('country_code', 2)->index()->comment('国家');
			$table->char('currency_code', 3)->index()->comment('货币');
			$table->enum('payment_provider', [
				Counter::PAYMENT_PROVIDER_CASH_DEPOSIT,
				Counter::PAYMENT_PROVIDER_NATIONAL_BANK,
				Counter::PAYMENT_PROVIDER_ALIPAY,
				Counter::PAYMENT_PROVIDER_WECHAT_PAY,
				Counter::PAYMENT_PROVIDER_ITUNES_GIFT_CARD,
				Counter::PAYMENT_PROVIDER_PAYTM,
				Counter::PAYMENT_PROVIDER_OTHER
			])->index()->comment('收款方式');
			$table->decimal('price', 15, 2)->comment('价格');
			$table->decimal('margin', 4, 2)->default(0)->comment('溢价');
			$table->integer('min_price')->default(0)->comment('最低价');
			$table->integer('min_amount')->comment('最小限额');
			$table->integer('max_amount')->comment('最大限额');
			$table->integer('payment_window_minutes')->comment('付款期限（分钟）');
			$table->text('message')->comment('介绍信息');
			$table->timestamps();
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('counters');
	}
}
