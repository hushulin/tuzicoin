<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Message;

/**
 * 聊天消息
 *
 * @author Latrell Chan
 *
 */
class CreateMessagesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function (Blueprint $table) {
			$table->comment = '聊天消息';
			$table->increments('id');
			$table->unsignedInteger('order_id')->index()->comment('订单');
			$table->unsignedInteger('source_id')->index()->comment('发送者');
			$table->unsignedInteger('target_id')->index()->comment('接收者');
			$table->enum('type', [
				Message::TYPE_SYSTEM,
				Message::TYPE_TEXT,
				Message::TYPE_IMAGE
			])->index()->comment('类型');
			$table->boolean('is_read')->default(false)->index()->comment('是否已读');
			$table->text('content')->nullable()->comment('正文');
			$table->text('extra')->nullable()->comment('拓展信息');
			$table->timestamps();
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('messages');
	}
}
