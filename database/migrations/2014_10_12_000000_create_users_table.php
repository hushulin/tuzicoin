<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 用户
 *
 * @author Latrell Chan
 *
 */
class CreateUsersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->comment = '用户';
			$table->increments('id')->comment('编号');
			$table->string('username', 100)->unique()->comment('用户名');
			$table->char('country_code', 2)->comment('国家代码');
			$table->string('mobile', 100)->comment('手机');
			$table->string('email', 100)->nullable()->unique()->comment('电子邮箱');
			$table->string('password', 60)->comment('密码');
			$table->string('secondary_password')->default('')->comment('二级密码');
			$table->unsignedInteger('authentication_id')->default(0)->comment('身份验证');
			$table->boolean('email_verified')->default(false)->comment('邮件验证');
			$table->boolean('mobile_verified')->default(false)->comment('手机验证');
			$table->string('google2fa_secret', 16)->default('')->comment('谷歌二次验证');
			$table->integer('trust_count')->default(0)->comment('信任人数');
			$table->integer('buy_trade_count')->default(0)->comment('累计购买交易次数');
			$table->integer('sell_trade_count')->default(0)->comment('累计出售交易次数');
			$table->decimal('buy_volume_btc', 15, 8)->default(0)->comment('累计购买交易量（BTC）');
			$table->decimal('buy_volume_ltc', 15, 8)->default(0)->comment('累计购买交易量（LTC）');
			$table->decimal('sell_volume_btc', 15, 8)->default(0)->comment('累计出售交易量（BTC）');
			$table->decimal('sell_volume_ltc', 15, 8)->default(0)->comment('累计出售交易量（LTC）');
			$table->integer('positive_feedback_count')->default(0)->comment('好评数');
			$table->integer('neutral_feedback_count')->default(0)->comment('中评数');
			$table->integer('negative_feedback_count')->default(0)->comment('差评数');
			$table->text('avatar')->nullable()->comment('头像');
			$table->text('introduction')->nullable()->comment('简介');
			$table->rememberToken()->comment('记住密码的Token');
			$table->string('register_ips')->default('')->comment('注册IP');
			$table->timestamps();
			$table->unique([
				'mobile',
				'country_code'
			]);
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
