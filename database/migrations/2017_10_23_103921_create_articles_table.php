<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function (Blueprint $table) {
			$table->comment = '文章';
			$table->increments('id');
			$table->unsignedInteger('admin_id')->comment('发布者');
			$table->string('title_en')->default('')->comment('标题（英文）');
			$table->text('content_en')->nullable()->comment('内容（英文）');
			$table->string('title_zh-CN')->default('')->comment('标题（简体中文）');
			$table->text('content_zh-CN')->nullable()->comment('内容（简体中文）');
			$table->string('title_zh-HK')->default('')->comment('标题（繁体中文）');
			$table->text('content_zh-HK')->nullable()->comment('内容（繁体中文）');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('articles');
	}
}
