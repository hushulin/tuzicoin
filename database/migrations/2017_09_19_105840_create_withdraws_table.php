<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Withdraw;

/**
 * 现申请
 *
 * @author Latrell Chan
 *
 */
class CreateWithdrawsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('withdraws', function (Blueprint $table) {
			$table->comment = '提现申请';
			$table->increments('id');
			$table->unsignedInteger('user_id')->comment('所属用户');
			$table->unsignedInteger('wallet_id')->comment('所属钱包');
			$table->enum('coin', [
				Withdraw::COIN_BTC,
				Withdraw::COIN_LTC
			])->comment('币种');
			$table->enum('status', [
				Withdraw::STATUS_PENDING,
				Withdraw::STATUS_SUCCESS,
				Withdraw::STATUS_FAILURE
			])->default(Withdraw::STATUS_PENDING)->comment('状态');
			$table->unsignedInteger('transaction_id')->default(0)->comment('提现成功产生的交易');
			$table->decimal('amount', 15, 8)->comment('金额');
			$table->decimal('fee', 15, 8)->default(0)->comment('手续费');
			$table->string('address', 100)->default('')->comment('目标地址');
			$table->string('remark')->default('')->comment('会员的备注');
			$table->string('reason')->default('')->comment('管理员的备注');
			$table->timestamps();
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('withdraws');
	}
}
