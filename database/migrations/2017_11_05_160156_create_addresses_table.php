<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Address;

class CreateAddressesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addresses', function (Blueprint $table) {
			$table->comment = '钱包地址';
			$table->increments('id');
			$table->enum('coin', [
				Address::COIN_BTC,
				Address::COIN_LTC
			])->comment('币种');
			$table->string('address', 100)->comment('地址');
			$table->unsignedInteger('user_id')->default(0)->comment('所属用户');
			$table->timestamps();
			$table->unique([
				'coin',
				'address'
			]);
			$table->index([
				'user_id',
				'coin'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('addresses');
	}
}
