<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Transaction;

/**
 * 交易记录
 *
 * @author Latrell Chan
 *
 */
class CreateTransactionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function (Blueprint $table) {
			$table->comment = '交易记录';
			$table->increments('id');
			$table->unsignedInteger('user_id')->comment('所属用户');
			$table->unsignedInteger('wallet_id')->comment('所属钱包');
			$table->enum('coin', [
				Transaction::COIN_BTC,
				Transaction::COIN_LTC
			])->comment('币种');
			$table->enum('type', [
				Transaction::TYPE_NETWORK_IN,
				Transaction::TYPE_NETWORK_OUT,
				Transaction::TYPE_TRADE_IN,
				Transaction::TYPE_TRADE_OUT,
				Transaction::TYPE_PLATFORM_IN,
				Transaction::TYPE_PLATFORM_OUT
			])->comment('类型');
			$table->decimal('amount', 15, 8)->comment('金额');
			$table->decimal('fee', 15, 8)->default(0)->comment('手续费');
			$table->string('txid', 64)->default('')->comment('交易所在区块Hash');
			$table->string('address', 100)->default('')->comment('转入或转出地址');
			$table->unsignedInteger('related_transaction_id')->default(0)->comment('相关交易的ID（平台转出对应平台转入）');
			$table->unsignedInteger('target_id')->default(0)->comment('目标用户');
			$table->decimal('price', 15, 2)->default(0)->comment('价格');
			$table->string('currency_code')->default('')->comment('货币');
			$table->string('remark')->default('')->comment('会员的备注');
			$table->timestamps();
			$table->index([
				'user_id',
				'coin',
				'type'
			]);
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('transactions');
	}
}
