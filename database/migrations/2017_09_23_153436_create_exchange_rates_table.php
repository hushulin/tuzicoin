<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * 各个货币的汇率信息
 *
 * @author Latrell Chan
 *
 */
class CreateExchangeRatesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exchange_rates', function (Blueprint $table) {
			$table->comment = '美元兑各个货币的汇率信息';
			$table->string('name', 3)->primary()->comment('货币');
			$table->decimal('price', 15, 6)->comment('价格');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('exchange_rates');
	}
}
