<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Wallet;

class CreateWalletsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wallets', function (Blueprint $table) {
			$table->comment = '钱包';
			$table->increments('id');
			$table->unsignedInteger('user_id')->comment('所属用户');
			$table->enum('coin', [
				Wallet::COIN_BTC,
				Wallet::COIN_LTC
			])->comment('币种');
			$table->decimal('balance_available', 15, 8)->default(0)->comment('可用余额');
			$table->decimal('balance_locked', 15, 8)->default(0)->comment('冻结资产');
			$table->timestamps();
			$table->unique([
				'user_id',
				'coin'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('wallets');
	}
}
