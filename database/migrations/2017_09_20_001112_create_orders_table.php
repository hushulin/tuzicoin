<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;

/**
 * 订单
 *
 * @author Latrell Chan
 *
 */
class CreateOrdersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function (Blueprint $table) {
			$table->comment = '订单';
			$table->increments('id');
			$table->unsignedInteger('counter_id')->index()->comment('柜台');
			$table->unsignedInteger('buyer_id')->index()->comment('买家');
			$table->unsignedInteger('seller_id')->index()->comment('卖家');
			$table->enum('status', [
				Order::STATUS_WAIT_PAYMENT,
				Order::STATUS_UNCONFIRMED,
				Order::STATUS_CONFIRMED,
				Order::STATUS_REVOKED,
				Order::STATUS_EXPIRED
			])->default(Order::STATUS_WAIT_PAYMENT)->index()->comment('状态');
			$table->decimal('price', 15, 2)->comment('价格（法币）');
			$table->decimal('amount', 15, 2)->comment('金额（法币）');
			$table->decimal('quantity', 15, 8)->comment('数量（数字币）');
			$table->decimal('fee', 15, 8)->default(0)->comment('手续费（数字币）');
			$table->enum('buyer_feedback', [
				Order::FEEDBACK_POSITIVE,
				Order::FEEDBACK_NEUTRAL,
				Order::FEEDBACK_NEGATIVE
			])->nullable()->comment('买家评论');
			$table->enum('seller_feedback', [
				Order::FEEDBACK_POSITIVE,
				Order::FEEDBACK_NEUTRAL,
				Order::FEEDBACK_NEGATIVE
			])->nullable()->comment('卖家评论');
			$table->dateTime('expires_at')->index()->comment('过期时间');
			$table->timestamps();
			$table->index([
				'status',
				'expires_at'
			]);
		});

		// 打乱初始ID。
		DB::unprepared('ALTER TABLE `users` AUTO_INCREMENT = ' . mt_rand(100000, 999999) . ';');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('orders');
	}
}
