// 导入路由列表。
let routes = require('./routes.json');
//console.log('routes', routes);

function route (name, parameters = {}, absolute = true) {
	let route = routes[name];
	let url = '/' + route.uri;
	
	// 替换URI中的参数。
	let filter_params = [];
	let prev_k = null;
	for (let k in parameters) {
		if ( ! prev_k) {
			let temp = url.split('{' + k + '}');
			url = {};
			url[k] = temp;
		} else {
			for (let i in url[prev_k]) {
				let temp = url[prev_k][i].split('{' + k + '}');
				url[prev_k][i] = {};
				url[prev_k][i][k] = temp;
			}
		}
		prev_k = k;
	}
	url = (function replacements (url){
		if (typeof url == 'string'){
			return url;
		} else {
			for (let k in url) {
				let arr = [];
				for (let i in url[k]) {
					if(typeof url[k][i] == 'string'){
						break;
					}
					let temp = {};
					temp[i] = url[k][i];
					arr.push(replacements(temp));
				}
				if(arr.length){
					url[k] = arr;
				}
				return url[k].join(parameters[k]);
			}
		}
	})(url);
	
	// TODO: 加上请求参数。
	
	return url;
}

window.route = route;

exports = module.exports = {
		route: route
}

exports.install = function (Vue, options) {
	Vue.prototype.route = route;
};