// 'use strict';
// /**
//  * 柜台部分的交互。
//  */
//
// // 计算最终成交。
// $('.container.buy').on('change keyup', '#input-quantity', function(){
// 	let quantity = parseFloat($('#input-quantity').val());
// 	let fee = parseFloat($('#input-final_deal').data('fee'));
// 	let value = quantity - quantity * fee / 100;
// 	if( ! isNaN(value)){
// 		value = parseFloat(value).toFixed(8);
// 		$('#input-final_deal').val(value);
// 	}
// });
// $('.container.sell').on('change keyup', '#input-quantity', function(){
// 	let quantity = parseFloat($('#input-quantity').val());
// 	let fee = parseFloat($('#input-final_deal').data('fee'));
// 	let value = quantity + quantity * fee / 100;
// 	if( ! isNaN(value)){
// 		value = parseFloat(value).toFixed(8);
// 		$('#input-final_deal').val(value);
// 	}
// });
// $('#input-quantity').trigger('change');
//
// // 切换柜台类型。
// $('#CreateCounterForm, #EditCounterForm').find('[name="type"]').on('change click', function(){
// 	let type = $('#CreateCounterForm, #EditCounterForm').find('[name="type"]:checked').val();
// 	switch(type){
// 		case 'OnlineBuy':
// 			$('#form-group-min_price').hide();
// 			$('#form-group-payment_window_minutes').show();
// 			break;
// 		case 'OnlineSell':
// 			$('#form-group-min_price').show();
// 			$('#form-group-payment_window_minutes').hide();
// 			break;
// 	}
// }).trigger('change');
//
// // 汇率。
// let exchange_rates = {};
//
// // 计算溢价后的价格。
// $('#input-margin').on('change keyup', function(){
// 	let coin = $('#CreateCounterForm, #EditCounterForm').find('[name="coin"]:checked').val();
// 	let coin_usd = window[coin.toLowerCase() + '_usd'];
// 	let margin = parseFloat($(this).val());
// 	let currency = $('#input-currency_code').val();
// 	let price = coin_usd * exchange_rates[currency];
// 	if( ! isNaN(margin)){
// 		price *= 1 + margin / 100;
// 	}
// 	$('#input-price').val(price.toFixed(2));
// });
//
// // 切换货币。
// $('#input-currency_code').on('change', function(){
// 	let currency = $(this).val();
// 	$('#' + [
//  		'input-text-price-addon_after',
// 		'input-text-min_price-addon_after',
// 		'input-text-min_amount-addon_after',
// 		'input-text-max_amount-addon_after'
// 	].join(', #')).text(currency);
//
// 	// 若没有汇率数据，从服务器获取。
// 	if (typeof exchange_rates[currency] == 'undefined') {
// 		$.ajax({
// 			url: route('exchange-rate'),
// 			data: {
// 				name: currency
// 			},
// 			success: function(result){
// 				exchange_rates[currency] = parseFloat(result.data);
// 				$('#input-margin').trigger('change'); // 重新计算价格。
// 			}
// 		});
// 	} else {
// 		$('#input-margin').trigger('change'); // 重新计算价格。
// 	}
// });
// // 切换币种。
// // $('#CreateCounterForm, #EditCounterForm').find('[name="coin"]').on('change click', function(){
// // 	$('#input-currency_code').trigger('change');
// // });
//
// // 初始化后触发一次事件。
// $(function(){
// 	if($('#input-price').length){
// 		$('#input-currency_code').trigger('change');
// 	}
// });
