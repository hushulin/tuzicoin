'use strict';

var modal = require('./modal');

// 冷却倒计时。
function CoolDown($btn, end_time){
	var text = $btn.text();
	$btn.attr('disabled', true);
	function leftTimeTimer($btn, end_time){
		var now = Math.ceil(new Date().getTime() / 1000);
		var left_time = end_time - now;
		if(left_time > 0){
			$btn.text(left_time + 's');
			setTimeout(function(){
				leftTimeTimer($btn, end_time);
			}, 1000);
		}else{
			$btn.removeAttr('disabled');
			$btn.text(text);
		}
	}
	leftTimeTimer($btn, Math.ceil(new Date().getTime() / 1000) +  end_time);
}

$.fn.sendVerificationMobile = function(country_code, mobile){
	var $btn = this;
	$btn.on('click', function(){
		$btn.attr('disabled', true);
		$.ajax({
			type: 'POST',
			url: route('verify.mobile'),
			data: {
				country_code: $('[name="' + country_code + '"]').val(),
				mobile: $('[name="' + mobile + '"]').val()
			},
			success: function(result){
				new CoolDown($btn, parseInt(result.data));
			},
			complete: function(xhr, ts){
				if(ts != 'success'){
					$btn.removeAttr('disabled');
				}
			}
		});
	});
}

$.fn.sendVerificationEmail = function(email){
	var $btn = this;
	$btn.on('click', function(){
		$btn.attr('disabled', true);
		$.ajax({
			type: 'POST',
			url: route('verify.email'),
			data: {
				email: $('[name="' + email + '"]').val()
			},
			success: function(result){
				new CoolDown($btn, parseInt(result.data));
			},
			complete: function(xhr, ts){
				if(ts != 'success'){
					$btn.removeAttr('disabled');
				}
			}
		});
	});
}
