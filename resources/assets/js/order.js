// 换算金额与数量。

var $amount = $('#CreateOrder [name="amount"]');
var $quantity = $('#CreateOrder [name="quantity"]');
var $in = $('#CreateOrder [name="in"]');
var $button = $('#CreateOrder [type="submit"]');

$amount.on('change keyup blur', function(){
	var amount = parseFloat($(this).val());
	var quantity = '';
	if( ! isNaN(amount)){
		quantity = parseFloat(amount / window.price).toFixed(8);
	}
	$quantity.val(quantity);
	$in.val('amount');
});
$quantity.on('change keyup blur', function(){
	var quantity = parseFloat($(this).val());
	var amount = '';
	if( ! isNaN(quantity)){
		amount = parseFloat(quantity * window.price).toFixed(2);
	}
	$amount.val(amount);
	$in.val('quantity');
});

