// 加载所有支持的语言。
let lang = {};
lang['en'] = require('../../lang/en.json');
lang['zh-CN'] = require('../../lang/zh-CN.json');
lang['zh-HK'] = require('../../lang/zh-HK.json');

exports.install = function (Vue, options) {
	
	// 取得页面的语言。
	let default_locale = document.getElementsByTagName('html')[0].lang; 
	
	Vue.prototype.__ = function (key, replace = {}, locale = null) {
		locale = locale || default_locale;
		if(typeof lang[locale] == 'undefined'){
			locale = 'en';
		}
		
		if(typeof lang[locale][key] == 'undefined'){
			return key;
		}
		
		let value = lang[locale][key];
		let prev_k = null;
		for (let k in replace) {
			if ( ! prev_k) {
				let temp = value.split(':' + k);
				value = {};
				value[k] = temp;
			} else {
				for (let i in value[prev_k]) {
					let temp = value[prev_k][i].split(':' + k);
					value[prev_k][i] = {};
					value[prev_k][i][k] = temp;
				}
			}
			prev_k = k;
		}
		value = (function replacements (value){
			if (typeof value == 'string'){
				return value;
			} else {
				for (let k in value) {
					let arr = [];
					for (let i in value[k]) {
						if(typeof value[k][i] == 'string'){
							break;
						}
						let temp = {};
						temp[i] = value[k][i];
						arr.push(replacements(temp));
					}
					if(arr.length){
						value[k] = arr;
					}
					return value[k].join(replace[k]);
				}
			}
		})(value);
		
		return value;
	};
};