'use strict';

function alert (message, title, callback) {
	if(typeof title == 'function'){
		callback = title;
		title = '';
	}
	if(title){
		$('#alertModalLabel').text(title);
	}
	$('#alertModalContent').text(message);
	var isok = false;
	$('#alertModalBtnPrimary').one('click', function(){
		isok = true;
	});
	$('#alertModal').one('hidden.bs.modal', function(){
		if(isok && typeof callback == 'function'){
			callback.call(this);
		}
	});
	$('#alertModal').modal('show');
}

window.ialert = alert;

exports = module.exports = {
	alert: alert
}