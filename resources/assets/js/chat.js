'use strict';
/**
 * 聊天功能。
 */

// 加入全站聊天频道。
window.Echo && Echo.join('chat')
	.here((users) => {
		//console.log('chat.here', users);
		for (let i in users) {
			let user = users[i];
			if (window.users && typeof window.users[user.id] != 'undefined') {
	    		window.users[user.id].online = true;
	    	}
		}
	})
    .joining((user) => {
    	//console.log('chat.joining', user);
    	if (window.users && typeof window.users[user.id] != 'undefined') {
    		window.users[user.id].online = true;
    	}
    })
    .leaving((user) => {
    	//console.log('chat.leaving', user);
    	if (window.users && typeof window.users[user.id] != 'undefined') {
    		window.users[user.id].online = false;
    	}
    });

// 用户消息通知。
if (window.auth) {
	window.Echo && Echo.private('user.' + window.auth.id)
		.notification((notification) => {
			console.log(notification);
			if ('Notification' in window) {
				window.Notification.requestPermission(function(permission){
					if (permission == 'granted') {
						let notify = new window.Notification(notification.title, {
							body: notification.content,
							icon: notification.icon,
							silent: true
						});
						if (notification.url && window.location.href != notification.url) {
							notify.onclick = function() {
					        	window.location.href = notification.url;
					        };
						}
					}
				});
			}
		});
	window.Notification.requestPermission();
}
