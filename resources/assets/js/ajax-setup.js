'use strict';
/**
 * 异步提交通用配置
 */

var modal = require('./modal');

$.ajaxSetup({
    dataType: 'json',
    headers: {
    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    statusCode: {
        401: function() {
            // modal.alert('请先登录');
        	location.href = route('login');
        },
        403: function() {
            modal.alert('You do not have permissions to perform this operation.')
        },
        404: function() {
            modal.alert('No data.');
        },
        422: function(xhq) {
        	var message = xhq.responseText;
        	if(xhq.responseJSON){
        		message = '';
        		$.each(xhq.responseJSON.errors, function(){
        			message += this.join('\n');
        		});
        	}
            modal.alert(message);
        },
        500: function() {
            modal.alert('Whoops, looks like something went wrong.');
        }
    }
});