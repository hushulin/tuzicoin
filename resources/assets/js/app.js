
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import translation from './vue-translation.js'
import route from './vue-route.js'
Vue.use(translation);
Vue.use(route);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));
Vue.component('user', require('./components/profile/user.vue'));
Vue.component('chat', require('./components/chat/chat.vue'));
Vue.component('chat-message', require('./components/chat/message.vue'));
Vue.component('chat-send', require('./components/chat/send.vue'));
Vue.component('order-status', require('./components/order/status.vue'));
Vue.component('order-tips', require('./components/order/tips.vue'));
Vue.component('order-operations', require('./components/order/operations.vue'));

const app = new Vue({
	el: '#app',
	data: {
		auth: window.auth || null,
		users: window.users || {}
	}
});

require('./ajax-setup.js');
require('./verify.js');
require('./counter.js');
// 需要登录才能访问的功能。
if (window.auth) {
	require('./order.js');
	require('./chat.js');
    require('./pay-info.js');
}
