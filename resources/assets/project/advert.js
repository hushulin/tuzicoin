import Vue from 'vue'
import App from './advert/index.vue'

new Vue({
	el: "#advert-app",
	render: h => h(App)
})
