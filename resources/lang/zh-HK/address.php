<?php
use App\Models\Address;

return [
	'coin' => [
		Address::COIN_BTC => '比特幣',
		Address::COIN_LTC => '萊特幣'
	]
];