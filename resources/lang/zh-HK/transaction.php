<?php
use App\Models\Transaction;

return [
	'type' => [
		Transaction::TYPE_NETWORK_IN => '網絡轉入',
		Transaction::TYPE_NETWORK_OUT => '網絡轉出',
		Transaction::TYPE_TRADE_IN => '交易買入',
		Transaction::TYPE_TRADE_OUT => '交易賣出',
		Transaction::TYPE_PLATFORM_IN => '平台轉入',
		Transaction::TYPE_PLATFORM_OUT => '平台轉出'
	]
];