<?php
use App\Models\Order;

return [
	'status' => [
		Order::STATUS_WAIT_PAYMENT => '待打款',
		Order::STATUS_UNCONFIRMED => '未確認',
		Order::STATUS_CONFIRMED => '已確認',
		Order::STATUS_REVOKED => '已撤銷',
		Order::STATUS_EXPIRED => '已過期'
	],
	'feedback' => [
		Order::FEEDBACK_POSITIVE => '好評',
		Order::FEEDBACK_NEUTRAL => '中評',
		Order::FEEDBACK_NEGATIVE => '差評'
	]
];