<?php
use App\Models\Wallet;

return [
	'coin' => [
		Wallet::COIN_BTC => '比特幣',
		Wallet::COIN_LTC => '萊特幣'
	]
];