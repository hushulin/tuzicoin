<?php
use App\Models\Withdraw;

return [
	'coin' => [
		Withdraw::COIN_BTC => '比特幣',
		Withdraw::COIN_LTC => '萊特幣'
	],
	'status' => [
		Withdraw::STATUS_PENDING => '待處理',
		Withdraw::STATUS_SUCCESS => '成功',
		Withdraw::STATUS_FAILURE => '失敗'
	]
];