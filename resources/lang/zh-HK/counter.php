<?php
use App\Models\Counter;

return [
	'coin' => [
		Counter::COIN_BTC => '比特幣',
		Counter::COIN_LTC => '萊特幣'
	],
	'status' => [
		Counter::STATUS_OPEN => '開放',
		Counter::STATUS_CLOSED => '關閉'
	],
	'type' => [
		Counter::TYPE_ONLINE_BUY => '在線購買',
		Counter::TYPE_ONLINE_SELL => '在線出售'
	],
	'payment_provider' => [
		Counter::PAYMENT_PROVIDER_CASH_DEPOSIT => '現金存款',
		Counter::PAYMENT_PROVIDER_NATIONAL_BANK => '銀行轉賬',
		Counter::PAYMENT_PROVIDER_ALIPAY => '支付寶',
		Counter::PAYMENT_PROVIDER_WECHAT_PAY => '微信支付',
		Counter::PAYMENT_PROVIDER_ITUNES_GIFT_CARD => 'iTunes禮品卡',
		Counter::PAYMENT_PROVIDER_PAYTM => 'Paytm',
		Counter::PAYMENT_PROVIDER_OTHER => '其它'
	]
];