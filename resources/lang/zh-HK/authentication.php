<?php
use App\Models\Authentication;

return [
	'type' => [
		Authentication::TYPE_IDENTITY_CARD => '中國大陸身份證',
		Authentication::TYPE_PASSPORT => '護照',
		Authentication::TYPE_DRIVER_LICENSE => '駕照'
	]
];