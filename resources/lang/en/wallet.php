<?php
use App\Models\Wallet;

return [
	'coin' => [
		Wallet::COIN_BTC => 'Bitcoin',
		Wallet::COIN_LTC => 'Litecoin'
	]
];