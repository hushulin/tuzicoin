<?php
use App\Models\Address;

return [
	'coin' => [
		Address::COIN_BTC => 'Bitcoin',
		Address::COIN_LTC => 'Litecoin'
	]
];