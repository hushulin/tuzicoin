<?php
use App\Models\Transaction;

return [
	'type' => [
		Transaction::TYPE_NETWORK_IN => 'Network In',
		Transaction::TYPE_NETWORK_OUT => 'Network Out',
		Transaction::TYPE_TRADE_IN => 'Trade In',
		Transaction::TYPE_TRADE_OUT => 'Trade Out',
		Transaction::TYPE_PLATFORM_IN => 'Platform In',
		Transaction::TYPE_PLATFORM_OUT => 'Platform Out'
	]
];