<?php
use App\Models\Authentication;

return [
	'type' => [
		Authentication::TYPE_IDENTITY_CARD => 'Identity card',
		Authentication::TYPE_PASSPORT => 'Passport',
		Authentication::TYPE_DRIVER_LICENSE => 'Driver license'
	]
];