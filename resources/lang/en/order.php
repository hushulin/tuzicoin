<?php
use App\Models\Order;

return [
	'status' => [
		Order::STATUS_WAIT_PAYMENT => 'WaitPayment',
		Order::STATUS_UNCONFIRMED => 'Unconfirmed',
		Order::STATUS_CONFIRMED => 'Confirmed',
		Order::STATUS_REVOKED => 'Revoked',
		Order::STATUS_EXPIRED => 'Expired'
	],
	'feedback' => [
		Order::FEEDBACK_POSITIVE => 'Positive',
		Order::FEEDBACK_NEUTRAL => 'Neutral',
		Order::FEEDBACK_NEGATIVE => 'Negative'
	]
];