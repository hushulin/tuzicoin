<?php
use App\Models\Withdraw;

return [
	'coin' => [
		Withdraw::COIN_BTC => 'Bitcoin',
		Withdraw::COIN_LTC => 'Litecoin'
	],
	'status' => [
		Withdraw::STATUS_PENDING => 'Pending',
		Withdraw::STATUS_SUCCESS => 'Success',
		Withdraw::STATUS_FAILURE => 'Failure'
	]
];