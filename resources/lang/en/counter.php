<?php
use App\Models\Counter;

return [
	'coin' => [
		Counter::COIN_BTC => 'Bitcoin',
		Counter::COIN_LTC => 'Litecoin'
	],
	'status' => [
		Counter::STATUS_OPEN => 'Ooen',
		Counter::STATUS_CLOSED => 'Closed'
	],
	'type' => [
		Counter::TYPE_ONLINE_BUY => 'Online buy',
		Counter::TYPE_ONLINE_SELL => 'Online sell'
	],
	'payment_provider' => [
		Counter::PAYMENT_PROVIDER_CASH_DEPOSIT => 'Cash deposit',
		Counter::PAYMENT_PROVIDER_NATIONAL_BANK => 'National bank',
		Counter::PAYMENT_PROVIDER_ALIPAY => 'Alipay',
		Counter::PAYMENT_PROVIDER_WECHAT_PAY => 'Wechat pay',
		Counter::PAYMENT_PROVIDER_ITUNES_GIFT_CARD => 'iTunes gift card',
		Counter::PAYMENT_PROVIDER_PAYTM => 'Paytm',
		Counter::PAYMENT_PROVIDER_OTHER => 'Other'
	]
];