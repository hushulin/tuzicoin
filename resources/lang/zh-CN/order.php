<?php
use App\Models\Order;

return [
	'status' => [
		Order::STATUS_WAIT_PAYMENT => '待打款',
		Order::STATUS_UNCONFIRMED => '未确认',
		Order::STATUS_CONFIRMED => '已确认',
		Order::STATUS_REVOKED => '已撤销',
		Order::STATUS_EXPIRED => '已过期'
	],
	'feedback' => [
		Order::FEEDBACK_POSITIVE => '好评',
		Order::FEEDBACK_NEUTRAL => '中评',
		Order::FEEDBACK_NEGATIVE => '差评'
	]
];