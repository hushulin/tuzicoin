<?php
use App\Models\Address;

return [
	'coin' => [
		Address::COIN_BTC => '比特币',
		Address::COIN_LTC => '莱特币'
	]
];