<?php
use App\Models\Counter;

return [
	'coin' => [
		Counter::COIN_BTC => '比特币',
		Counter::COIN_LTC => '莱特币'
	],
	'status' => [
		Counter::STATUS_OPEN => '开放',
		Counter::STATUS_CLOSED => '关闭'
	],
	'type' => [
		Counter::TYPE_ONLINE_BUY => '在线购买',
		Counter::TYPE_ONLINE_SELL => '在线出售'
	],
	'payment_provider' => [
		Counter::PAYMENT_PROVIDER_NATIONAL_BANK => '银行转账',
		Counter::PAYMENT_PROVIDER_ALIPAY => '支付宝',
		Counter::PAYMENT_PROVIDER_WECHAT_PAY => '微信支付',
	]
];