<?php
use App\Models\Wallet;

return [
	'coin' => [
		Wallet::COIN_BTC => '比特币',
		Wallet::COIN_LTC => '莱特币'
	]
];