<?php
use App\Models\Transaction;

return [
	'type' => [
		Transaction::TYPE_NETWORK_IN => '网络转入',
		Transaction::TYPE_NETWORK_OUT => '网络转出',
		Transaction::TYPE_TRADE_IN => '交易买入',
		Transaction::TYPE_TRADE_OUT => '交易卖出',
		Transaction::TYPE_PLATFORM_IN => '平台转入',
		Transaction::TYPE_PLATFORM_OUT => '平台转出'
	]
];