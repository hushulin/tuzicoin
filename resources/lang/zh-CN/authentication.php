<?php
use App\Models\Authentication;

return [
	'type' => [
		Authentication::TYPE_IDENTITY_CARD => '中国大陆身份证',
		Authentication::TYPE_PASSPORT => '护照',
		Authentication::TYPE_DRIVER_LICENSE => '驾照'
	]
];