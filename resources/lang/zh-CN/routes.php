<?php
return [
	'RootDashboard' => '仪表盘',

	'RootAdminList' => '管理员账号',
	'RootAdminEdit' => '编辑管理员（页面）',
	'RootAdminEditAction' => '编辑管理员（操作）',
	'RootAdminDelete' => '删除管理员',

	'RootRoleList' => '角色列表',
	'RootRoleEdit' => '编辑角色（页面）',
	'RootRoleEditAction' => '编辑角色（操作）',
	'RootRoleDelete' => '删除角色',

	'RootPermissionList' => '权限列表',
	'RootPermissionEdit' => '编辑权限（页面）',
	'RootPermissionEditAction' => '编辑权限（操作）',
	'RootPermissionDelete' => '删除权限',

	'RootChangePassowrd' => '修改密码（页面）',
	'RootChangePassowrdAction' => '修改密码（操作）',

	'RootOperationRecordList' => '操作记录列表',
	'RootOperationRecordDetail' => '操作记录详情',
	'RootOperationRecordResponseView' => '响应内容查看',

	'RootConfigEdit' => '编辑系统配置（页面）',
	'RootConfigSave' => '编辑系统配置（操作）',

	'RootUserList' => '用户列表',
	'RootUserDetail' => '用户详情',
	'RootUserLogin' => '直接登录用户账号',
	'RootUserLoginLogList' => '用户登陆日志',

	'RootBitcoinAddressList' => '比特币地址列表',
	'RootBitcoinAddressCreate' => '导入比特币地址（页面）',
	'RootBitcoinAddressSave' => '导入比特币地址（操作）',

	'RootWithdrawList' => '提现请求列表',
	'RootWithdrawCompleted' => '完成提现请求',
	'RootWithdrawRefuse' => '拒绝提现请求',

	'RootDepositList' => '存款单列表',

	'RootTransactionList' => '交易记录列表',

	'RootOrderList' => '订单列表',
	'RootCounterList' => '柜台列表',

	'RootAuthenticationList' => '验证信息列表',
	'RootAuthenticationImage' => '验证信息图片',
	'RootAuthenticationEdit' => '编辑验证信息（页面）',
	'RootAuthenticationSave' => '编辑验证信息（操作）',
	'RootAuthenticationDelete' => '删除验证信息',

	'RootMarketPriceList' => '市场价数据列表',
	'RootExchangeRateList' => '法币汇率列表',
	'RootCountryList' => '国家信息列表',
	'RootCurrencyList' => '货币信息列表',

	'RootStatisticsFlow' => '流水统计',
	'RootStatisticsUser' => '用户统计',

	'RootArticleList' => '文章列表',
	'RootArticleEdit' => '编辑文章（页面）',
	'RootArticleSave' => '编辑文章（操作）',
	'RootArticleDelete' => '删除文章'
];