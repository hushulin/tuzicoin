<?php
use App\Models\Withdraw;

return [
	'coin' => [
		Withdraw::COIN_BTC => '比特币',
		Withdraw::COIN_LTC => '莱特币'
	],
	'status' => [
		Withdraw::STATUS_PENDING => '待处理',
		Withdraw::STATUS_SUCCESS => '成功',
		Withdraw::STATUS_FAILURE => '失败'
	]
];