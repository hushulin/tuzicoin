@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Trusted by') }}</div>

	<div class="panel-body">
		@include('components.message')

		@include('trust.users', [ 'relation' => 'source' ])
	</div>
</div>
@endsection
