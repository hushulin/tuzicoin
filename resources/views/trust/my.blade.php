@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Trusting') }}</div>

	<div class="panel-body">
		@include('components.message')

		@include('trust.users', [ 'relation' => 'target' ])
	</div>
</div>
@endsection
