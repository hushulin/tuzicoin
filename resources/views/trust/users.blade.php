<div class="user-list">
	@forelse ($data as $item)
		@php
			$user = $item->$relation;
		@endphp
		<div class="user-item">
			<user :user_id="{{ $user->id }}"></user>
			@include('profile.credit')
			<div class="trade-count">跟TA交易过{{ $item->trade_count }}次</div>
		</div>
	@empty
		<p class="text-muted">{{ __('No data') }}</p>
	@endforelse
</div>
<div class="text-center">{{ $data->links() }}</div>

@push('data')
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	@foreach ($data->pluck('source', 'source_id')->merge($data->pluck('target', 'target_id'))->unique() as $user)
		window.users[{{ $user->id }}] = {!! $user !!};
	@endforeach
</script>
@endpush