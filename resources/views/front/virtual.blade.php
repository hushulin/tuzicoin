
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="uwkU18MFXRawDdBPiD5Mubwd9FKP3IpRVp58btr2">

    <title>Tuzicoin</title>

    <!-- Styles -->
    <link href="/css/app.css?id=3a1e933ff80a285bedd1" rel="stylesheet">
    <noscript>
        <link href="/css/noscript.css?id=8158b95e29594404a5f9" rel="stylesheet">
    </noscript>
</head>
<body>
<div id="app">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="profile">
                            <div class="user"><div class="avatar"><span aria-hidden="true" class="glyphicon glyphicon-user"></span> <!----> <span class="user-status"></span> <!----></div> <div class="username"><span>goget</span></div></div>                            <div class="credit">
                                <dl>
                                    <dt>交易次数</dt>
                                    <dd>6</dd>
                                </dl>
                                <dl>
                                    <dt>信任人数</dt>
                                    <dd>0</dd>
                                </dl>
                                <dl>
                                    <dt>好评度</dt>
                                    <dd>100%</dd>
                                </dl>
                                <dl>
                                    <dt>累计交易量</dt>
                                    <dd>
                                        TUZI
                                    </dd>
                                </dl>
                            </div>	</div>


                        <hr>

                        <dl class="dl-horizontal">
                            <dt>商品名称</dt>
                            <dd><strong>XXXXXXXXX</strong></dd>
                            <dt>价格</dt>
                            <dd><strong>0.10 CNY/TUZI</strong></dd>
                            <dt>交易限额</dt>
                            <dd>1-1000 CNY</dd>
                            <dt>钱包货发货地址</dt>
                            <dd>微信支付</dd>

                        </dl>
                        <form method="POST" action="" id="CreateOrder">
                            <div class="form-group">
                                <label class="control-label">你想购买多少？</label>
                                <div>
                                    <div class="input-group">
                                        <input type="tel" class="form-control" name="amount" value=""
                                               placeholder="输入您想购买的数量"
                                        >
                                        <span class="input-group-addon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group">
                                <input type="tel" class="form-control" name="quantity"
                                       value=""
                                       placeholder="价格"
                                >
                                <span class="input-group-addon"></span>
                            </div>
                            <br/>
                            <div class="form-group">
                                <button class="btn btn-primary btn-block"
                                        type="submit">立即购买</button>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body">
                        <h5>交易须知</h5>
                        <ol>
                            <li>交易前，请仔细阅读帮助页面中的交易指南和说明及《tuzicoin服务条款》。</li>
                            <li>请通过平台进行沟通约定及交易。所有绕开平台的私下交易行为若发生纠纷，与平台无关。</li>
                            <li>交易中请注意欺诈风险，收款或收货时，您必须登录账户，检查核实是否到账。</li>
                            <li>如您的订单遇到交易纠纷，请保留好交易证据进行申诉，我们将根据您提供的证据进行处理。</li>
                            <li>禁止利用本平台进行违法活动。</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">广告留言</div>
                    <div class="panel-body">
                        <div class="message">12312321</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">商品图片</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <image src="/images/banner.jpg?94f6372aa31aa0ff6bb95dba63282602" style=" width: 100%;height: 300px "/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




<!-- Scripts -->


<script src="/js/app.js?id=bd9f1f8a448ad56206e2"></script>
</body>
</html>
