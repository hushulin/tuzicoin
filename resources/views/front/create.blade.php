<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <link href="/css/app.css?id=3a1e933ff80a285bedd1" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>发布一个交易广告</h3>
                    </div>
                    <div class="panel-body">
                        <h4 style="color: red">禁止发布黄赌毒等违法广告，一经发现一律封IP。</h4>
                        <p>请您在发布广告前仔细阅读以下重要说明。</p>
                        <p>发布广告免费，硬件设备区每个广告发布需要消耗10个Tuzi。</p>
                        <p>平台对每笔成功的交易扣除0.2%费用。</p>
                        <p> Tuzicoin仅作为平台，不参与任何交易，仅为发生在平台内的广告交易提供保障，</p>
                        <p>如交易过程中发生争议，您可以保留好聊天记录截图或照片，发送工单，我们将人工核实，保障您的交易安全。</p>
                        <p>
                            所有的交易请在tuzicoin上完成，若您脱离平台私下交易，发生被骗及其他损失，平台不承担任何赔偿责任。如果有用户要求您脱离平台私下交易，您可以保留聊天记录截图并联系我们进行投诉，我们将第一时间采取措施。</p>
                        <p>风险提示：区块链代币交易具有较高的风险（价格暴涨暴跌、人为操控、团队解散、技术缺陷等因素）</p>
                        <p>请您在发布广告交易前做好充分的风险认识。</p>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="">
                            <h4>交易类型</h4>
                            <hr>
                            <div class="form-group" id="form-group-coin_area">
                                <label class="col-md-3 control-label muted">
                                    选择广告区
                                    <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                </label>
                                <div class="col-md-6">
                                    <label for="input-coin_area-TUZI" class="radio-inline">
                                        <input type="radio" name="adtype" checked value="virtual">虚拟区
                                    </label>
                                    <label for="input-coin_area-DTC" class="radio-inline" value="device">
                                        <input type="radio" name="adtype" value="device">硬件设备区
                                    </label>

                                    <p id="help-block-coin_area" class="help-block">若交易区选择错误，广告会删除</p>
                                </div>
                            </div>
                            <div class="form-group" id="form-group-type">
                                <label class="col-md-3 control-label">
                                    广告类型
                                    <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                </label>

                                <div class="col-md-6">
                                    <label for="input-type-OnlineBuy" class="radio-inline">
                                        <input type="radio" name="adkind" value="buy" checked>
                                        在线收购
                                    </label>
                                    <label for="input-type-OnlineSell" class="radio-inline">
                                        <input type="radio" name="adkind" value="sell"
                                        >
                                        在线出售
                                    </label>

                                </div>
                            </div>
                            <div class="form-group" id="form-group-title">
                                <label for="input-title" class="col-md-3 control-label">
                                    广告标题
                                    <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                </label>
                                <div class="col-md-6">
                                    <input id="input-title" type="text" class="form-control" name="title" value=""
                                           placeholder="广告标题" required>
                                    <p id="help-block-title" class="help-block">请填写简单直观的标题名称，限10字</p>
                                </div>
                            </div>
                            <h4>更多信息</h4>
                            <hr>

                            <div class='row' id="device">
                                <div class="form-group" id="form-group-number">
                                    <label for="input-number" class="col-md-3 control-label">
                                        库存数量
                                        <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input id="input-number" value="" class="form-control" placeholder="数量"
                                                   required="required">
                                            <span class="input-group-addon" id="input-text-number-addon_after">个</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="form-group-price">
                                    <label for="input-price" class="col-md-3 control-label">
                                        价格
                                        <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="tel" class="form-control" name="price" value=""
                                                   placeholder="价格">
                                            <span class="input-group-addon" id="input-text-price-addon_after">CNY</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="form-group-price">
                                    <label for="input-price" class="col-md-3 control-label">联系方式</label>
                                    <div class="col-md-6">
                                        <input type="tel" class="form-control" name="price" value=""
                                               placeholder="联系方式">
                                    </div>
                                </div>
                                <div id="shou_huo_address">
                                    <div class="form-group" id="form-group-contact">
                                        <label for="input-contact" class="col-md-3 control-label">商品图片</label>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='row' id="virtual">
                                <div class="form-group" id="form-group-number">
                                    <label for="input-number" class="col-md-3 control-label">
                                        库存数量
                                        <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input id="input-number" value="" class="form-control" placeholder="数量"
                                                   required="required">
                                            <span class="input-group-addon" id="input-text-number-addon_after">个</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="form-group-price">
                                    <label for="input-price" class="col-md-3 control-label">
                                        价格
                                        <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="tel" class="form-control" name="price" value=""
                                                   placeholder="价格">
                                            <span class="input-group-addon" id="input-text-price-addon_after">CNY</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="form-group-price">
                                    <label for="input-price" class="col-md-3 control-label">联系方式</label>
                                    <div class="col-md-6">
                                            <input type="tel" class="form-control" name="price" value=""
                                                   placeholder="联系方式">
                                    </div>
                                </div>
                                <div class="form-group" id="form-group-title">
                                    <label for="input-title" class="col-md-3 control-label">
                                        最小限额
                                        <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                    </label>
                                    <div class="col-md-6">
                                        <input id="input-title" type="text" class="form-control" name="bbbb" value=""
                                               placeholder="最小限额" required>
                                    </div>
                                </div>
                                <div class="form-group" id="form-group-title">
                                    <label for="input-title" class="col-md-3 control-label">
                                        最大限额
                                        <small class="text-danger glyphicon glyphicon-asterisk"></small>
                                    </label>
                                    <div class="col-md-6">
                                        <input id="input-title" type="text" class="form-control" name="ssss" value=""
                                               placeholder="最大限额" required>
                                    </div>
                                </div>
                                <div id="shou_huo_address">
                                    <div class="form-group" id="form-group-contact">
                                        <label for="input-contact" class="col-md-3 control-label">商品图片</label>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control">
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group" id="kindbuy">
                                <label for="input-shou_huo_addresss" class="col-md-3 control-label">收货地址或账号
                                    <small class="text-danger glyphicon glyphicon-asterisk"></small>

                                </label>
                                <div class="col-md-6">
                                        <textarea id="input-shou_huo_addresss" type="text" class="form-control"
                                                  name="shou_huo_addresss" placeholder="" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group"  id="kindsell">
                                <label for="input-shou_huo_addresss" class="col-md-3 control-label">发货地址或账号
                                    <small class="text-danger glyphicon glyphicon-asterisk"></small>

                                </label>
                                <div class="col-md-6">
                                        <textarea id="input-shou_huo_addresss" type="text" class="form-control"
                                                  name="shou_huo_addresss" placeholder="" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group" id="form-group-message">
                                <label for="input-message" class="col-md-3 control-label">广告留言</label>
                                <div class="col-md-6">
                                <textarea id="input-message" type="text" class="form-control" name="message"
                                          placeholder="xxxxxxxx"
                                          rows="3" required="required">
                                </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <input type="button" id="submit-form" class="btn btn-primary" value="立即发布"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/app.js?id=bd9f1f8a448ad56206e2"></script>
<script>
    $(document).ready(function () {
        $('#device').hide();
        $('input[type=radio][name=adtype]').change(function () {
            if (this.value == 'virtual') {
                $('#virtual').show();
                $('#device').hide();
            }
            else if (this.value == 'device') {
                $('#device').show();
                $('#virtual').hide();
            }
        });
        $('#kindsell').hide();
        $('input[type=radio][name=adkind]').change(function () {

            if (this.value == 'buy') {
                $('#kindbuy').show();
                $('#kindsell').hide();
            }
            else if (this.value == 'sell') {
                $('#kindsell').show();
                $('#kindbuy').hide();
            }
        });
    });
</script>
</body>
</html>
