@extends('layouts.app')

@section('content')
<div id="advert-app"></div>
@endsection

@push('script')
<script src="{{ mix('project/js/advert.js') }}"></script>
@endpush