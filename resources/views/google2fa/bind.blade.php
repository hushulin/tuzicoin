@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Bind Google Verification') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('google2fa.bind') }}">
			{{ csrf_field() }}

			<p>{{ __('Please scan the two-dimensional code or manually enter the key, the mobile phone generated on the dynamic verification code to fill in the following input box.') }}</p>

			@include('components.form.static', [
				'label' => __('Account name'),
				'value' => config('app.name') . ':' . Auth::user()->username
			])

			@include('components.form.image', [
				'src' => $qrcode_src
			])

			@include('components.form.static', [
				'label' => __('Key'),
				'name' => 'secret',
				'value' => $secret
			])

			@include('components.form.number', [
				'label' => __('Google Verification Code'),
				'name' => 'code',
				'placeholder' => __('Please enter a 6-digit Google Verification Code'),
				'required' => true
			])

			<p>{{ __('How to install Google Verification Code?') }}</p>
			<p>{{ __('iPhone search the App Store for the Google Authenticator download app') }}</p>
			<p>{{ __('Android search for the Google Authenticator in the Android Market, or search the Google Authenticator download app') }}</p>
			<p>{{ __('After binding, Login, Withdrawals, Payment required Google verification, ') }}<a href="#DetailedTutorialModal" data-toggle="modal">{{ __('View detailed tutorial') }}</a></p>

			<hr>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Verify and enable') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="DetailedTutorialModal" tabindex="-1" role="dialog" aria-labelledby="DetailedTutorialModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="DetailedTutorialModalLabel">{{ __('Detailed tutorial') }}</h4>
			</div>
			<div class="modal-body">
<p>Google Authenticator双重验证可以更安全的保护您的账户，未开户双重验证不影响正常的交易。</p>
<p>开户双重验证功能后，在您每次在电脑端进行登录，提现，支付操作时，系统都会提示您输入手机应用程序上显示的一次性密码，来确保您的资金安全。您可以依照下面的步骤来设置并启用这一功能：<br>
1.在您的手机上安装双重验证程序：Google Authenticator<br>
iPhone手机：在App Store中搜索Google Authenticator<br>
Android手机：在应用市场中搜索“谷歌身份验证器”，或搜索Google Authenticator下载<br>
2.安装完成后，您需要对该应用程序进行如下配置<br>
在“Google Authenticator (身份验证器)”应用程序中，点击“添加新账户 (iOS 下是 + 号)”，然后选择“扫描条形码”扫描CoinCola绑定谷歌验证器页面的二维码；<br>
如果您无法扫描成功上图的条形码，您还可以手动添加账户，并输入CoinCola绑定谷歌验证器页面的密钥<br>
3.配置完成后，手机上会显示一个 6 位数字，每隔 30 秒变化一次。这个数字即为您的双重验证密码。</p>
<p>注意事项：请勿删除此双重验证密码账户，否则会导致您无法进行账户操作，您可将密钥记录下来，如果误删，可通过手动输入密钥 ({{ $secret }}) 来恢复。输入双重验证密码，以开启或关闭双重验证功能。</p>
			</div>
		</div>
	</div>
</div>
@endsection
