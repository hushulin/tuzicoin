@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Unbind Google Verification') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('google2fa.unbind') }}">
			{{ csrf_field() }}

			@include('components.form.static', [
				'label' => __('Account name'),
				'value' => config('app.name') . ':' . Auth::user()->username
			])

			@include('components.form.number', [
				'label' => __('Google Verification Code'),
				'name' => 'code',
				'placeholder' => __('Please enter a 6-digit Google Verification Code'),
				'required' => true
			])

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Verify and relieve') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
