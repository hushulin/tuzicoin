@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('Login') }}</div>

                <div class="panel-body">
                	@include('components.message')

                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        @include('components.form.country', [
							'label' => __('Country'),
							'name' => 'country_code',
							'countries' => $countries,
							'value' => 'CN',
							'required' => true
						])

                        @include('components.form.tel', [
							'label' => __('Mobile'),
							'name' => 'mobile',
							'required' => true
						])

						@include('components.form.password', [
							'label' => __('Password'),
							'name' => 'password',
							'required' => true
						])

						@include('components.form.captcha', [
							'label' => __('Captcha'),
							'name' => 'captcha'
						])

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.reset') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
