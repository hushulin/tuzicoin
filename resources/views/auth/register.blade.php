@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('Register') }}</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

						@include('components.form.text', [
							'label' => __('Username'),
							'name' => 'username',
							'required' => true,
							'autofocus' => true
						])

						@include('components.form.country', [
							'label' => __('Country'),
							'name' => 'country_code',
							'countries' => $countries,
							'value' => 'CN',
							'required' => true
						])

                        @include('components.form.tel', [
							'label' => __('Mobile'),
							'name' => 'mobile',
							'required' => true
						])

						@include('components.form.verify.sms', [
							'label' => __('Verification'),
							'name' => 'smsverify'
						])

                        @include('components.form.password', [
							'label' => __('Password'),
							'name' => 'password',
							'required' => true
						])

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
