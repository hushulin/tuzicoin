@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Create secondary password') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('password.secondary.reset') }}">
			{{ csrf_field() }}

			<input type="hidden" name="country_code" value="{{ Auth::user()->country_code }}">
			<input type="hidden" name="mobile" value="{{ Auth::user()->mobile }}">

			@include('components.form.static', [
				'label' => __('Mobile'),
				'value' => '+' . Auth::user()->country->area_code . ' ' . Auth::user()->mobile
			])

			@include('components.form.verify.sms', [
				'label' => __('Verification'),
				'name' => 'smsverify'
			])

			@include('components.form.password', [
				'label' => __('New password'),
				'name' => 'new_password',
				'required' => true
			])

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Create') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
