@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('Reset password') }}</div>

                <div class="panel-body">
                    @include('components.message')

                    <form class="form-horizontal" method="POST" action="{{ route('password.reset') }}">
                        {{ csrf_field() }}

                        @include('components.form.country', [
							'label' => __('Country'),
							'name' => 'country_code',
							'countries' => $countries,
							'value' => 'CN',
							'required' => true
						])

                        @include('components.form.tel', [
							'label' => __('Mobile'),
							'name' => 'mobile',
							'required' => true
						])

						@include('components.form.verify.sms', [
							'label' => __('Verification'),
							'name' => 'smsverify'
						])

                        @include('components.form.password', [
							'label' => __('New password'),
							'name' => 'new_password',
							'required' => true
						])

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
