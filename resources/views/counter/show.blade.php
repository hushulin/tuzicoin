@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('components.message')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('user.profile', [ 'user' => $counter->user ])

                        <hr>

                        <dl class="dl-horizontal">
                            <dt>{{ __('Price') }}</dt>
                            <dd><strong>{{ $counter->price }} {{ $counter->currency_code }}
                                    /{{ $counter->coin }}</strong></dd>
                            <dt>{{ __('Limits') }}</dt>
                            <dd>{{ $counter->min_amount }}-{{ $counter->max_amount }} {{ $counter->currency_code }}</dd>
                            <dt>{{ __('Payment method') }}</dt>
                            <dd>{{ trans('counter.payment_provider.' . $counter->payment_provider) }}
                                @if($counter->payment_provider == 'NationalBank' && $pay_info['bank_user_name'] && $pay_info['bank_account'])
                                    <span>账户名：{{$pay_info['bank_user_name']}}</span>
                                    <span>卡号：{{$pay_info['bank_account']}}</span>
                                @endif
                                @if($counter->payment_provider == 'Alipay' && $pay_info['ali_user_name'] && $pay_info['ali_account'])
                                    <span>姓名：{{$pay_info['ali_user_name']}}</span>
                                    <span>账号：{{$pay_info['ali_account']}}</span>
                                    @if($pay_info['alipay_img'])
                                        <span>收款码 <img src="{{$pay_info['alipay_img']}}" class="img-responsive" style="width: 100px" alt=""></span>
                                    @endif
                                @endif
                                @if($counter->payment_provider == 'WechatPay' && $pay_info['wechat_user_name'] && $pay_info['wechat_account'])
                                    <span>姓名：{{$pay_info['wechat_user_name']}}</span>
                                    <span>账号：{{$pay_info['wechat_account']}}</span>
                                    @if($pay_info['wechat_img'])
                                        <span>收款码 <img src="{{$pay_info['wechat_img']}}" class="img-responsive" style="width: 100px" alt=""></span>
                                    @endif
                                @endif
                            </dd>
                            <dt>{{ __('Time of payment') }}</dt>
                            <dd>{{ $counter->payment_window_minutes }} {{ __('minutes') }}</dd>

                        </dl>
                        <form method="POST" action="{{ route('order.create') }}" id="CreateOrder">
                            {{ csrf_field() }}
                            <input type="hidden" name="counter_id" value="{{ $counter->id }}">
                            <input type="hidden" name="in" value="{{ old('in', 'quantity') }}">
                            <div class="form-group{{ $errors->hasAny('amount', 'quantity') ? ' has-error' : '' }}">
                                <label class="control-label">{{ $counter->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('How many do you wish to buy?') : __('How many do you wish to sell?') }}</label>
                                <div class="control-amount">
                                    <div class="input-group">
                                        <input type="tel" class="form-control" name="amount" value="{{ old('amount') }}"
                                               placeholder="{{ $counter->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('Enter the amount you want to buy') : __('Enter the amount you want to sell') }}"
                                               @if (Auth::id() === $counter->user_id) readonly="readonly" @endif
                                        >
                                        <span class="input-group-addon">{{ $counter->currency_code }}</span>
                                    </div>
                                    <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>
                                    <div class="input-group">
                                        <input type="tel" class="form-control" name="quantity"
                                               value="{{ old('quantity') }}"
                                               placeholder="{{ $counter->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('Enter the quantity you want to buy') : __('Enter the quantity you want to sell') }}"
                                               @if (Auth::id() === $counter->user_id) readonly="readonly" @endif
                                        >
                                        <span class="input-group-addon">{{ $counter->coin }}</span>
                                    </div>
                                </div>
                                @if ($errors->hasAny('amount', 'quantity'))
                                    <span class="help-block">
									<strong>{{ $errors->first('amount') ?: $errors->first('quantity') }}</strong>
								</span>
                                @endif
                            </div>
                            <div class="form-group">
                                @if (Auth::id() == $counter->user_id)
                                    <a class="btn btn-primary btn-block"
                                       href="{{ route('counter.edit', [ 'id' => $counter->id ]) }}"
                                       role="button">{{ __('Edit') }}</a>
                                @else
                                    <button class="btn btn-primary btn-block"
                                            type="submit">{{ $counter->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('BUY') : __('SELL') }}</button>
                                @endif
                            </div>
                        </form>
                    </div>
                    <div class="panel-body">
                        <h5>{{ __('Notice of the transaction') }}</h5>
                        @if(0)
                        <ol>
                            <li>{{ __('Please understand the transaction information before the transaction.') }}</li>
                            <li>{{ __('Please communicate through :app_name and save the relevant records.', [ 'app_name' => config('app.name') ]) }}</li>
                            <li>{{ __('If you encounter a transaction dispute, you can complain the trade.') }}</li>
                            <li>{{ __('After you initiate a transaction request, :coin is locked in custody and protected by :app_name. If you are a buyer, after making a trade request, please pay through the payment method as specified and mark the transaction as payment completed. After the seller receives the payment, the :coin will be released automatically. Please read the Terms of Service and other helping documents such as FAQs and trading tutorial before trading.', [ 'app_name' => config('app.name'), 'coin' => $counter->coin ]) }}</li>
                            <li>{{ __('Please check the user\'s evaluation and related credit information, and pay attention to the risk of fraud.') }}</li>
                            <li>{{ __('Hosting services will protect both buyer and seller. In the event of a dispute, we will evaluate all informations provided and release the managed :coin to its legal owner.', [ 'coin' => $counter->coin ]) }}</li>
                        </ol>
                        @endif
                        <ol>
                            <li>{{ __('交易前，请仔细阅读帮助页面中的交易指南和说明及《tuzicoin服务条款》。') }}</li>
                            <li>{{ __('请通过平台进行沟通约定及交易。所有绕开平台的私下交易行为若发生纠纷，与平台无关。') }}</li>
                            <li>{{ __('交易中请注意欺诈风险，收款或收货时，您必须登录账户，检查核实是否到账。') }}</li>
                            <li>{{ __('如您的订单遇到交易纠纷，请保留好交易证据进行申诉，我们将根据您提供的证据进行处理。') }}</li>
                            <li>{{ __('禁止利用本平台进行违法活动。') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('Leave message') }}</div>
                    <div class="panel-body">
                        <div class="message">{{ $counter->message }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('data')
    <script>
        window.price = {{ $counter->price }};
    </script>
@endpush