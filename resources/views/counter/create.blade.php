@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>{{ __('Create a advertisement for trade') }}</h3></div>

                    <div class="panel-body">
                        @include('components.message')
                    </div>
                    <div class="panel-body">
                        <p>请您在发布广告前仔细阅读以下重要说明。</p>

                        <p>发布广告免费，硬件设备区每个广告发布需要消耗10个Tuzi。</p>

                        <p>平台对每笔成功的交易扣除0.2%费用。</p>

                        <p> Tuzicoin仅作为平台，不参与任何交易，仅为发生在平台内的广告交易提供保障，</p>
                        <p>如交易过程中发生争议，您可以保留好聊天记录截图或照片，发送工单，我们将人工核实，保障您的交易安全。</p>

                        <p>
                            所有的交易请在tuzicoin上完成，若您脱离平台私下交易，发生被骗及其他损失，平台不承担任何赔偿责任。如果有用户要求您脱离平台私下交易，您可以保留聊天记录截图并联系我们进行投诉，我们将第一时间采取措施。</p>


                        <p>风险提示：区块链代币交易具有较高的风险（价格暴涨暴跌、人为操控、团队解散、技术缺陷等因素）</p>
                        <p>请您在发布广告交易前做好充分的风险认识。</p>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('counter.create') }}"
                              id="CreateCounterForm">
                            {{ csrf_field() }}

                            <h4>{{ __('Trade type') }}</h4>
                            <hr>


                            @include('components.form.radio', [
                                   'label' => '选择交易区',
                                   'name' => 'coin_area',
                                   'options' => [
                                       'TUZI'=>'兔子币',
                                       //'BTC'=>'比特币',
                                       //'LTC'=>'莱特币',
                                   ],
                                   'help' => '若交易区选择错误，广告会删除',
                                   'value' => 'TUZI'
                               ])
                            @include('components.form.radio', [
                                'label' => '广告类型',
                                'name' => 'type',
                                'options' => __('counter.type') ,
                                'help' => '',
                                'value' => 'OnlineBuy'
                            ])
                            @include('components.form.text', [
                                'label' => '广告标题',
                                'name' => 'title',
                                'help' =>'请填写简单直观的标题名称，限10字',
                                'required'=>true
                            ])

                            <h4>{{ __('More') }}</h4>
                            <hr>


                            @include('components.form.number', [
                                'label' => '数量',
                                'name' => 'number',
                                 'addon_after' => '个',
                                'required' => true
                            ])

                            @include('components.form.number', [
                                'label' => __('Price'),
                                'name' => 'price',
                                 'addon_after' => 'CNY',
                            ])



                            @include('components.form.number', [
                                'label' => __('Payment due time'),
                                'name' => 'payment_window_minutes',
                                'addon_after' => __('minutes'),
                                'help' => __('You promise to pay within this time(min).'),
                            ])

                            @include('components.form.number', [
                           'label' => __('Min limit'),
                           'name' => 'min_amount',
                           'addon_after' => '',
                           'help' => __('Min amount per trade.'),

                           'value'=>1
                            ])

                            @include('components.form.number', [
                                'label' => __('Max limit'),
                                'name' => 'max_amount',
                                'addon_after' => '',
                                'help' => __('Amount\'s max limit.'),
                                'required' => true,
                                'value'=>1
                            ])

                            <div id="payment_method">
                                @include('components.form.select', [
                               'label' => __('Payment methods'),
                               'name' => 'payment_provider',
                               'options' => trans('counter.payment_provider'),

                                ])
                            </div>


                            <div id="qianbao" class="">
                                @include('components.form.textarea', [
                               'label' => '钱包地址或账户',
                               'name' => 'qibao',
                               'placeholder' => '若你发布请求广告，您必须填写收币地址，以便用户向您得地址转币，请核对您的接收地址是否正确有效，若发生争议，将以处为处理依据',
                               'rows' => 3
                                ])
                            </div>


                            <div id="shou_huo_address">
                                @include('components.form.number', [
                              'label' => '联系方式',
                              'name' => 'contact',
                              'help' => '联系方式',

                               ])
                                @include('components.form.textarea', [
                               'label' => '收货地址',
                               'name' => 'shou_huo_addresss',
                               'placeholder' => '',
                               'rows' => 3
                                ])


                            </div>


                            @include('components.form.textarea', [
                                'label' => __('Leave message'),
                                'name' => 'message',
                                'placeholder' => __('Enter more infomation about this advertisement(such as payment method and preferred contact information).'),
                                'required' => true,
                                'rows' => 3
                            ])
                            <hr>

                            <div class="form-group">
                                <div class="col-md-9 col-md-offset-3">
                                    <input type="button" id="submit-form" class="btn btn-primary" value="{{ __('POST') }}"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('data')
    <script>
        window.btc_usd = {{ btc_usd() }};
        window.ltc_usd = {{ ltc_usd() }};
    </script>
@endpush

@push('script')
    <script>
        $(function () {
            $('#payment_method').hide();
            $('#shou_huo_address').hide();
            if($('input[name=coin_area]:checked').val()=='TUZI') {
                $('#payment_method').show();
            }
            $('#submit-form').click(function () {
                if($('input[name="coin_area"]:checked').val()!='TUZI') {
                    $('#payment_method').remove();
                }
                $('#CreateCounterForm').submit();
            });
        });
    </script>
@endpush