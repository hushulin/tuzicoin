@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>{{ __('Edit a advertisement for trade') }}</h3></div>

                <div class="panel-body">
                	@include('components.message')
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('counter.edit') }}" id="EditCounterForm">
                        {{ csrf_field() }}

                        <input type="hidden" name="id" value="{{ $counter->id }}">

                        <h4>{{ __('Trade type') }}</h4>
                        <hr>

                        @include('components.form.radio', [
							'label' => __('Select coin type'),
							'name' => 'coin',
							'options' => array_combine(C('coins'), C('coins')),
							'help' => __('Please choose the type of coin you want to trade.'),
							'value' => $counter->coin,
							'disabled' => true
						])

                        @include('components.form.radio', [
							'label' => __('Select counter type'),
							'name' => 'type',
							'options' => trans('counter.type'),
							'help' => __('Which type of advertisement you want to create? If you wish to sell, please comfirm that you have enough balance in your :app_name wallet.', [ 'app_name' => config('app.name') ]),
							'value' => $counter->type,
							'disabled' => true
						])

                        @include('components.form.select', [
							'label' => __('Location'),
							'name' => 'country_code',
							'options' => $countries->pluck('name', 'code')->map(function($name, $code){ return $code . ' ' . $name; }),
							'value' => $counter->country_code,
							'help' => __('Please select the country you want to trade in.'),
							'required' => true
						])

						<h4>{{ __('More') }}</h4>
						<hr>

						@include('components.form.select', [
							'label' => __('Currency'),
							'name' => 'currency_code',
							'options' => $currencies->pluck('name', 'code')->map(function($name, $code){ return $code . ' ' . $name; }),
							'value' => $counter->currency_code,
							'help' => __('Currency you want to trade with'),
							'required' => true
						])

						@include('components.form.number', [
							'label' => __('Margin'),
							'name' => 'margin',
							'value' => $counter->margin,
							'addon_after' => '%',
							'help' => __('The price is equal to Reference Price * (1 + Margin). Form -99.99 to 99.99.'),
							'required' => true
						])

						@include('components.form.number', [
							'label' => __('Price'),
							'name' => 'price',
							'addon_after' => '',
							'help' => __('The reference price will be updated in every 10 minutes based on the margin rate.'),
							'disabled' => true
						])

						@include('components.form.number', [
							'label' => __('Min price'),
							'name' => 'min_price',
							'value' => $counter->min_price ?: '',
							'addon_after' => '',
							'help' => __('If the market price is lower than the minimum price you set (reference price * (1 + Margin rate)), the displayed price will remained as your minimum price.')
						])

						@include('components.form.number', [
							'label' => __('Min limit'),
							'name' => 'min_amount',
							'value' => $counter->min_amount,
							'addon_after' => '',
							'help' => __('Min amount per trade.'),
							'required' => true
						])

						@include('components.form.number', [
							'label' => __('Max limit'),
							'name' => 'max_amount',
							'value' => $counter->max_amount,
							'addon_after' => '',
							'help' => __('Amount\'s max limit.'),
							'required' => true
						])

						@include('components.form.number', [
							'label' => __('Payment due time'),
							'name' => 'payment_window_minutes',
							'value' => $counter->payment_window_minutes,
							'addon_after' => __('minutes'),
							'help' => __('You promise to pay within this time(min).'),
						])

						@include('components.form.select', [
							'label' => __('Payment methods'),
							'name' => 'payment_provider',
							'options' => trans('counter.payment_provider'),
							'value' => $counter->payment_provider,
							'required' => true
						])

						@include('components.form.textarea', [
							'label' => __('Leave message'),
							'name' => 'message',
							'value' => $counter->message,
							'placeholder' => __('Enter more infomation about this advertisement(such as payment method and preferred contact information).'),
							'required' => true,
							'rows' => 3
						])

						<hr>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
	window.btc_usd = {{ btc_usd() }};
	window.ltc_usd = {{ ltc_usd() }};
</script>
@endpush
