@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ trans('counter.type.' . request('type')) }}</div>

	<div class="panel-body">
		@include('components.message')

		<ul class="nav nav-tabs">
			@foreach ([
				App\Models\Counter::STATUS_OPEN => 'Ongoing counter',
				App\Models\Counter::STATUS_CLOSED => 'Closed counter'
			] as $status => $text)
				<li @if (request('status') == $status) class="active" @endif >
					<a href="{{ route('counter.list', array_merge(request()->except('page'), [ 'status' => $status ])) }}">{{ __($text) }}</a>
				</li>
			@endforeach
		</ul>
	</div>
	<div class="panel-body">
		@if ($counters->isEmpty())
			<p class="text-muted">{{ __('No data') }}</p>
		@else
			<table class="table">
				<thead>
					<tr>
						<th>{{ __('ID') }}</th>
						<th>{{ __('Coin') }}</th>
						<th>{{ __('Type') }}</th>
						<th>{{ __('Country') }}</th>
						<th>{{ __('Price') }}</th>
						<th>{{ __('Margin rate') }}</th>
						<th>{{ __('Status') }}</th>
						<th>{{ __('置顶') }}</th>
						<th>{{ __('Created at') }}</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($counters as $item)
						<tr>
							<td><a href="{{ route('counter.show', [ 'id' => $item->id ]) }}">{{ $item->id }}</a></td>
							<td>{{ $item->coin }}</td>
							<td>{{ trans('counter.type.' . $item->type) }}</td>
							<td>{{ $item->country_code }}</td>
							<td>{{ $item->price }} {{ $item->currency_code }}</td>
							<td>{{ $item->margin }}%</td>
							<td>{{ trans('counter.status.' . $item->status) }}</td>
							<td>
								@if($item->zd_stime && $item->zd_etime && $item->zd_stime<date('Y-m-d H:i:s') && $item->zd_etime>date('Y-m-d H:i:s'))
								置顶（{{$item->zd_etime}}）
								@else
								未置顶
								@endif
							</td>
							<td>{{ $item->created_at }}</td>
							<td>
								@if($item->zd_stime && $item->zd_etime && $item->zd_stime<date('Y-m-d H:i:s') && $item->zd_etime>date('Y-m-d H:i:s'))
								@else
									<a class="btn btn-primary btn-sm" data-id="{{$item->id}}" data-tk="zd" role="button">{{ __('置顶') }}</a>
								@endif
								<a class="btn btn-primary btn-sm" href="{{ route('counter.edit', [ 'id' => $item->id ]) }}" role="button">{{ __('Edit') }}</a>
								@if ($item->status == App\Models\Counter::STATUS_OPEN)
									<form action="{{ route('counter.closed', [ 'id' => $item->id ]) }}" method="post" class="form-btn">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-primary btn-sm">{{ __('Close') }}</button>
									</form>
								@else
									<form action="{{ route('counter.open', [ 'id' => $item->id ]) }}" method="post" class="form-btn">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-primary btn-sm">{{ __('Reopen') }}</button>
									</form>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $counters->links() }}
		@endif
	</div>
</div>
@push('script')
<script>
    $(document).ready(function () {
        $('[data-tk=zdmodal]').remove();
        $('[data-tk=zd]').click(function () {
            var itemId = $(this).attr('data-id');
            var $modalHtml = $('<div data-tk="zdmodal">\
			<span>\
				<div class="modal fade" tabindex="-1" role="dialog">\
					<div class="modal-dialog" role="document">\
						<div class="modal-content">\
							<div class="modal-header">\
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">\
									<span aria-hidden="true">&times;</span>\
								</button>\
								<h4 class="modal-title" id="PaymentModalLabel">{{ __('置顶') }}</h4>\
							</div>\
							<div class="modal-body">{{ __('1天50TUZI，3天120TUZI') }}</div>\
							<div class="modal-body">天数：<input type="radio" name="zdday" value="1" checked="true"/>1天，&nbsp;&nbsp;<input type="radio" name="zdday" value="3"/>3天</div>\
							<div class="modal-footer">\
								<form class="form-btn" @submit.prevent="false">\
								<button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close') }}</button>\
								<button type="button" class="btn btn-primary" data-tk="zdclick">{{ __('OK') }}</button>\
								</form>\
							</div>\
						</div>\
					</div>\
				</div>\
            </span>\
            </div>');
            $modalHtml.find('.modal').modal('show');
            $modalHtml.find('[data-tk=zdclick]').click(function(){
                $modalHtml.find('.modal').modal('hide');
                $.ajax({
                    method: "POST",
                    url: "{{route('counter.zd')}}",
                    data: {id:itemId,zdday:$modalHtml.find("input[name=zdday]:checked").val()}
                }).done(function(data){
                    if(data.code==0) {
                        location.reload();
                    } else {
                        alert(data.msg);
                    }
				});
			});
            $('body').append($modalHtml);
        });
    });
</script>
@endpush
@endsection
