@extends('layouts.app')

@section('content')
<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h3 class="h1">@if(0){{ config('app.name') }}@endif</h3>
				<h1 class="h3">
					@if(0){{ __('The Leading OTC Digital Currency Marketplace') }}@endif
					{{ __('全球首家最全面的OTC信息平台') }}
				</h1>
				<div class="decs">@if(0){{ __('Welcome to the World\'s Safest OTC Digital Currency Exchange Community') }}@endif</div>
			</div>
		</div>
	</div>
</div>

@if ($notice)
	<div class="notice">
		<div class="container">
			<div class="alert alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
				<a href="{{ route('article.notice') }}?id={{$notice->id}}&type=1" class="title">{{ $notice->title }}</a>
				<a href="{{ route('article.notice') }}?id={{$notice->id}}&type=1">{{ __('More') }}</a>
			</div>
		</div>
	</div>
@endif

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center">{{ __('Free, safe, and reliable Digital Currency trading') }}</h3>
		</div>
		<div class="col-md-12">
			<p class="text-center">{{ __('Get started with :app_name', [ 'app_name' => config('app.name') ]) }} <a href="{{ route('buy') }}">{{ __('See more counters &#x2192;') }}</a></p>
			<br>
		</div>
	</div>
	<div class="row">
		@foreach ($counters as $item)
			<div class="col-md-3">
				<div class="thumbnail index-{{ $loop->iteration }}">
					@include('user.profile', [ 'user' => $item->user ])
					<div class="caption">
						<h3>{{ $item->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('Buy ' . $item->coin) : __('Sell ' . $item->coin) }}</h3>
						<p>{{ __('Price') }}: {{ $item->price }} {{ $item->currency_code }}/{{ $item->coin }}</p>
						<p>{{ __('Limits') }}: {{ $item->min_amount }}-{{ $item->max_amount }} {{ $item->currency_code }}</p>
						<p>{{ __('Payment method') }}: {{ trans('counter.payment_provider.' . $item->payment_provider) }}</p>
						<a class="btn btn-primary btn-sm" href="{{ route('counter.show', [ 'id' => $item->id ]) }}">{{ $item->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('Buy') : __('Sell') }}</a>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>

<div class="site-info">
	<div class="container">
		<div class="site-info-container">
			<div class="site-item">
				<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
				<h3>{{ __('Free Trading') }}</h3>
				@if(0)<p>{{ __(':app_name is a fast and easy P2P Digital Currency trading platform that is not involved with any third party.', [ 'app_name' => config('app.name') ]) }}</p>@endif
				<p>{{ __(':app_name 是一个不涉及第三方的OTC交易平台。', [ 'app_name' => config('app.name') ]) }}</p>
			</div>
			<div class="site-item">
				<span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
				<h3>{{ __('Safe and Reliable') }}</h3>
				<p>{{ __('Our team uses the most advanced technology such as cold storage, SSL and multiple bank-level encryption algorithm to provide you with the highest level of security.') }}</p>
			</div>
			<div class="site-item">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				<h3>{{ __('Convenient Transaction') }}</h3>
				<p>{{ __('Anywhere, anytime, with anyone, fast and secure.') }}</p>
			</div>
		</div>
	</div>
</div>
@endsection

@push('data')
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	@foreach ($counters->pluck('user', 'user_id')->unique() as $user)
		window.users[{{ $user->id }}] = {!! $user !!};
	@endforeach
</script>
@endpush