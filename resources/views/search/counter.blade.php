@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<div class="col-md-12 ">
    		<div class="text-center">
	    		<form class="form-inline">
					<div class="form-group ">
						<label class="sr-only" for="input-country_code">{{ __('Location') }}</label>

						<input type="text" class="form-control " style="width: 400px" name="keyword">
					</div>
					{{--<div class="form-group">--}}
						{{--<label class="sr-only" for="input-currency_code">{{ __('Currency') }}</label>--}}
						{{--<select class="form-control" name="currency_code" id="input-currency_code">--}}
							{{--<option value="">{{ __('Currency') }}</option>--}}
							{{--@foreach ($currencies->pluck('name', 'code')->map(function($name, $code){ return $code . ' ' . $name; }) as $value => $text)--}}
								{{--<option value="{{ $value }}" @if($value == request('currency_code')) selected="selected" @endif >{{ $text }}</option>--}}
							{{--@endforeach--}}
						{{--</select>--}}
					{{--</div>--}}
					{{--<div class="form-group">--}}
						{{--<label class="sr-only" for="input-payment_provider">{{ __('Payment methods') }}</label>--}}
						{{--<select class="form-control" name="payment_provider" id="input-payment_provider">--}}
							{{--<option value="">{{ __('Payment methods') }}</option>--}}
							{{--@foreach (trans('counter.payment_provider') as $value => $text)--}}
								{{--<option value="{{ $value }}" @if($value == request('payment_provider')) selected="selected" @endif >{{ $text }}</option>--}}
							{{--@endforeach--}}
						{{--</select>--}}
					{{--</div>--}}
					<input type="button" id="sButton" class="btn btn-primary" value="{{ __('Search') }}" />
				</form>
				<br>
			</div>
    	</div>
    	<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped table-hover">
		    			<thead>
		    				<tr>
		    					<th>{{ __('User name') }}</th>
								<th>标题</th>
		    					<th>{{ __('Payment method') }}</th>
		    					<th>{{ __('Limits') }}</th>
		    					<th>{{ __('Price') }}</th>

		    					<th></th>
		    				</tr>
		    			</thead>
		    			<tbody>
		    				@foreach ($counters as $item)
			    				<tr>
			    					<td>
			    						<user :user_id="{{ $item->user_id }}"></user>
									</td>
									<td>
										{{ $item->title }}
									</td>
			    					{{--<td>{{ __('Trades') }} {{ $item->user->trade_count }} | {{ __('Rating') }} {{ $item->user->rating }} | {{ __('Trusted') }} {{ $item->user->trust_count }}</td>--}}
			    					<td>{{ trans('counter.payment_provider.' . $item->payment_provider) }}</td>
			    					<td>{{ $item->min_amount }}-{{ $item->max_amount }} {{ $item->currency_code }}</td>
			    					<td><span class="text-success">{{ $item->price }} {{ $item->currency_code }}/{{ $item->coin }}</span></td>
			    					<td>
										<a class="btn btn-primary btn-sm" href="{{ route('counter.show', [ 'id' => $item->id ]) }}">{{ $item->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('Buy') : __('Sell') }}</a>
									</td>
			    				</tr>
			    			@endforeach
		    			</tbody>
		    		</table>
		    		<div class="text-center">{{ $counters->links() }}</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('data')
<script type='text/javascript' src="http://apps.bdimg.com/libs/jquery/1.7.1/jquery.js"></script>
<script src="http://www.gbtags.com/gb/networks/uploads/8dedafe6-e613-4a38-bd77-fdfa5181fd3a/js/jquery.countdown.js"></script>
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	@foreach ($counters->pluck('user', 'user_id')->unique() as $user)
		window.users[{{ $user->id }}] = {!! $user !!};
	@endforeach
</script>
<script>
    var countdown = {};
    countdown.wait = parseInt(sessionStorage.getItem('countdown.wait'));
    countdown.wait = countdown.wait || 0;
    countdown.time = function(btn) {
        var self = this;
        if(self.wait == 0) {
            btn.removeAttribute("disabled");
            btn.value = "搜索";
            self.wait = 40;
            sessionStorage.setItem("countdown.wait", self.wait);
            return true;
        } else {
            btn.setAttribute("disabled", true);
            btn.value = "(" + self.wait + ")";
            self.wait--;
            setTimeout(function() {
				self.time(btn)
			}, 1000);
            sessionStorage.setItem("countdown.wait", self.wait);
            return false;
        }
    }
    countdown.load = function(btn) {
        var self = this;
        if(countdown.wait>0) {
            $(btn).attr("disabled", true);
            var intervalTimer = setInterval(function () {
                if(countdown.wait==0) {
                    clearInterval(intervalTimer);
                    btn.value = "搜索";
                    $(btn).attr("disabled", false);
                } else {
                    countdown.wait = countdown.wait - 1;
                    btn.value = "(" + self.wait + ")";
                    sessionStorage.setItem("countdown.wait", countdown.wait);
                }
            },1000);
        }
        $(btn).click(function () {
            if(self.time(btn)) {
                $(btn).parent('form').submit();
			}
        });
    };
    document.getElementById("sButton").onclick = function() {
        countdown.time(this);
        return false;
    }
    $(document).ready(function(){
        countdown.load($('#sButton')[0]);
	});
</script>
@endpush