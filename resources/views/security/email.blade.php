@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Bind email') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('security.email') }}">
			{{ csrf_field() }}

			@include('components.form.email', [
				'label' => __('Email'),
				'name' => 'email'
			])

			@include('components.form.verify.email', [
				'label' => __('Verification'),
				'name' => 'emailverify'
			])

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Bind') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
