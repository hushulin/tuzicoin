@extends('layouts.setting')

@section('main')
    @push('script')
        @if($errors->has('alipay_img'))
            <script>
                $('#alipay-Modal').modal('show')
            </script>
        @endif

    @endpush
    <div class="panel panel-default">
        <div class="panel-heading">{{ __('Pay info') }}</div>

        <div class="panel-body">
            @include('components.message')
            <div class="col-lg-12 form-group">
                <div class="col-lg-6">
                    支付宝
                </div>
                <div class="col-lg-6 text-right">
                    <a href="" data-toggle="modal" data-target="#alipay-Modal" class="btn btn-default">修改</a>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="alipay-Modal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <form class="form-horizontal" id="ali-form" method="post" action="{{route('update.ali')}}"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">添加支付宝账号信息</h4>
                                </div>
                                <div class="modal-body">
                                    @include('components.form.text', [
                                                                      'label' => '姓名：',
                                                                      'name' => 'ali_user_name',
                                                                      'help' => '',
                                                                      'required' => true,
                                                                      'value'=>$pay_info['ali_user_name']??''
                                                                  ])
                                    @include('components.form.text', [
                                                                      'label' => '支付宝账户：',
                                                                      'name' => 'ali_account',
                                                                      'help' => '',
                                                                      'required' => true,
                                                                      'value'=>$pay_info['ali_account']??''
                                                                  ])
                                    @include('components.form.file', [
                                                       'label' => '支付宝收款码',
                                                       'name' => 'alipay_img',
                                                       'help' => __('支付宝收款码，大小限制为 2M'),
                                                       'accept' => "image/jpeg,image/jpg,image/png",
                                                       'value'=>$pay_info['alipay_img']??''
                                                   ])


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                    <button type="submit" class="btn btn-primary" id="ali-form-submit">保存</button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>

            <div class="col-lg-12 form-group">
                <div class="col-lg-6">
                    微信
                </div>
                <div class="col-lg-6 text-right">
                    <a href="" data-toggle="modal" data-target="#wechat-Modal" class="btn btn-default">修改</a>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="wechat-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <form class="form-horizontal" enctype="multipart/form-data" method="POST"
                              action="{{ route('update.whchat') }}">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">添加微信账号信息</h4>
                                </div>
                                <div class="modal-body">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="country_code" value="{{ Auth::user()->country_code }}">
                                    <input type="hidden" name="old_mobile" value="{{ Auth::user()->mobile }}">

                                    @include('components.form.text', [
                                                                      'label' => '姓名：',
                                                                      'name' => 'wechat_user_name',
                                                                      'help' => '',
                                                                      'required' => true,
                                                                      'value'=>$pay_info['wechat_user_name']??''
                                                                  ])
                                    @include('components.form.text', [
                                                                      'label' => '微信账户：',
                                                                      'name' => 'wechat_account',
                                                                      'help' => '',
                                                                      'required' => true,
                                                                      'value'=>$pay_info['wechat_account']??''
                                                                  ])
                                    @include('components.form.file', [
                                                       'label' => '微信收款码',
                                                       'name' => 'wechat_img',
                                                       'help' => __('微信收款码，大小限制为 2M'),
                                                       'value'=>$pay_info['wechat_img']??''
                                                   ])


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                    <button type="submit" class="btn btn-primary">保存</button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>

            <div class="col-lg-12 form-group">
                <div class="col-lg-6">
                    银行卡
                </div>
                <div class="col-lg-6 text-right">
                    <a href="" data-toggle="modal" data-target="#bank-Modal" class="btn btn-default">修改</a>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="bank-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <form class="form-horizontal" method="POST" action="{{ route('update.bank') }}">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">添加银行卡信息</h4>
                                </div>
                                <div class="modal-body">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="country_code" value="{{ Auth::user()->country_code }}">
                                    <input type="hidden" name="old_mobile" value="{{ Auth::user()->mobile }}">

                                    @include('components.form.text', [
                                                                      'label' => '姓名：',
                                                                      'name' => 'bank_user_name',
                                                                      'help' => '',
                                                                       'required' => true,
                                                                      'value'=>$pay_info['bank_user_name']??''
                                                                  ])
                                    @include('components.form.text', [
                                                                      'label' => '银行账户：',
                                                                      'name' => 'bank_account',
                                                                      'help' => '',
                                                                       'required' => true,
                                                                      'value'=>$pay_info['bank_account']??''
                                                                  ])

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                    <button type="submit" class="btn btn-primary">保存</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
