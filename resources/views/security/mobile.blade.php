@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Bind mobile') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('security.mobile') }}">
			{{ csrf_field() }}

			<input type="hidden" name="country_code" value="{{ Auth::user()->country_code }}">
			<input type="hidden" name="old_mobile" value="{{ Auth::user()->mobile }}">

			@include('components.form.static', [
				'label' => __('Old mobile'),
				'value' => '+' . Auth::user()->country->area_code . ' ' . Auth::user()->mobile
			])

			@include('components.form.verify.sms', [
				'label' => __('Old mobile verification'),
				'name' => 'old_smsverify',
				'elem_mobile' => 'old_mobile'
			])

			@include('components.form.tel', [
				'label' => __('New mobile'),
				'name' => 'new_mobile',
				'addon_before' => '+' . Auth::user()->country->area_code
			])

			@include('components.form.verify.sms', [
				'label' => __('New mobile verification'),
				'name' => 'new_smsverify',
				'elem_mobile' => 'new_mobile'
			])

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Bind') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
