@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Security settings') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal">

			<div class="form-group">
				<label class="col-md-2 control-label">{{ __('Username') }}</label>

				<div class="col-md-9">
					<p class="form-control-static">{{ Auth::user()->username }}</p>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-2 control-label">{{ __('Security level') }}</label>

				<div class="col-md-9">
					<div class="form-control-static">
						@php
							$level = 5;
							if(Auth::user()->secondary_password){
								$level += 25;
							}
							if(Auth::user()->email_verified){
								$level += 10;
							}
							if(Auth::user()->mobile_verified){
								$level += 25;
							}
							if(Auth::user()->google2fa_secret){
								$level += 30;
							}
						@endphp
						<div class="pull-left security-level">
							@if ($level > 80)
								{{ __('HIGH') }}
								@php
									$bar_color = 'success';
								@endphp
							@elseif ($level > 50)
								{{ __('MID') }}
								@php
									$bar_color = 'warning';
								@endphp
							@else
								{{ __('LOW') }}
								@php
									$bar_color = 'danger';
								@endphp
							@endif
						</div>
						<div class="progress">
							<div class="progress-bar progress-bar-{{ $bar_color }}" role="progressbar" aria-valuenow="{{ $level }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $level }}%">
								<span class="sr-only">{{ $level }}% Complete</span>
							</div>
						</div>
					</div>
				</div>
			</div>

		</form>

		<hr>

		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="security-list">
					<ul class="media-list">
						<li class="media">
							<div class="media-left">
								@php
									$color = Auth::user()->secondary_password ? 'success' : 'default';
								@endphp
								<span class="media-object label label-{{ $color }}">
									<i class="glyphicon glyphicon-credit-card" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{ __('Secondary password') }}</h4>
								{{ __('Will be used for the withdrawal of Digital Currency') }}
							</div>
							<div class="media-right">
								<div style="width: 150px; text-align: right;">
									@if (Auth::user()->secondary_password)
										<a class="btn btn-default btn-sm" href="{{ route('password.secondary.reset') }}" role="button">{{ __('Forgot') }}</a>
										<a class="btn btn-default btn-sm" href="{{ route('password.secondary') }}" role="button">{{ __('Change') }}</a>
									@else
										<a class="btn btn-default btn-sm" href="{{ route('password.secondary.create') }}" role="button">{{ __('Create') }}</a>
									@endif
								</div>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								@php
									$color = Auth::user()->email_verified ? 'danger' : 'default';
								@endphp
								<span class="media-object label label-{{ $color }}">
									<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{ __('Bind email') }}</h4>
								{{ __('Will be used for login, withdrawal of Digital Currency') }}
							</div>
							<div class="media-right">
								@if (Auth::user()->email_verified)
									<span class="label label-default">{{ __('Verified') }}</span>
								@else
									<a class="btn btn-primary btn-sm" href="{{ route('security.email') }}" role="button">{{ __('Set') }}</a>
								@endif
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								@php
									$color = Auth::user()->mobile_verified ? 'primary' : 'default';
								@endphp
								<span class="media-object label label-{{ $color }}">
									<i class="glyphicon glyphicon-phone" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{ __('Bind mobile') }}</h4>
								{{ __('Will be used for login, find password') }}
							</div>
							<div class="media-right">
								<a class="btn btn-default btn-sm" href="{{ route('security.mobile') }}" role="button">{{ __('Change') }}</a>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								<span class="media-object label label-info">
									<i class="glyphicon glyphicon-lock" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{ __('Login password') }}</h4>
								{{ __('Will be used for login') }}
							</div>
							<div class="media-right">
								<a class="btn btn-default btn-sm" href="{{ route('password.login') }}" role="button">{{ __('Change') }}</a>
							</div>
						</li>
						<li class="media">
							<div class="media-left">
								@php
									$color = Auth::user()->google2fa_secret ? 'warning' : 'default';
								@endphp
								<span class="media-object label label-{{ $color }}">
									<i class="glyphicon glyphicon-unchecked" aria-hidden="true"></i>
								</span>
							</div>
							<div class="media-body">
								<h4 class="media-heading">{{ __('Google Verification') }}</h4>
								{{ __('After binding, login, offer, pay when you need Google secondary verification') }}
							</div>
							<div class="media-right">
								@if (Auth::user()->google2fa_secret)
									<a class="btn btn-default btn-sm" href="{{ route('google2fa.unbind') }}" role="button">{{ __('Relieve') }}</a>
								@else
									<a class="btn btn-primary btn-sm" href="{{ route('google2fa.bind') }}" role="button">{{ __('Enable') }}</a>
								@endif
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection
