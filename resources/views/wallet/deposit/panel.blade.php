<dl class="dl-horizontal">
	<dt>{{ __('Receive address') }}</dt>
	<dd>{{ $address->address or '' }}</dd>
</dl>
	<dl class="dl-horizontal">
		<dt>&nbsp;</dt>
		<dd>
			<img src="{{ route('wallet.qrcode', @$address->address) }}" class="img-thumbnail qrcode">
		</dd>
	</dl>

<p>{{ __('Tips: :coin usually takes 10 minutes to receive :coin. We will receive the :coin network :confirmations confirmed after you account. In the platform of mutual transfer is real-time billing and free of charge.', [ 'coin' => $wallet->coin, 'confirmations' => C('confirmations_' . strtolower($wallet->coin)) ]) }}</p>

@if ( ! $deposits->isEmpty())
	<table class="table">
		<thead>
			<tr>
				<th>{{ __('Unconfirmed transaction') }}</th>
				<th width="160">{{ __('Quantity') }}</th>
			</tr>
		</thead>
		<tbody>
			@each('wallet.deposit.item', $deposits, 'data')
		</tbody>
	</table>
@endif
