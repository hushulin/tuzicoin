<tr>
	<td>txid: <a href="{{ sprintf(C('blockchain_view_' . strtolower($data->coin)), $data->txid) }}" target="_blank">{{ $data->txid }}</a></td>
	<td>{{ btc_format($data->amount) }} {{ $wallet->coin }}</td>
</tr>