<form class="form-horizontal" method="POST" action="{{ route('wallet.send', [ 'coin' => $wallet->coin ]) }}">
	{{ csrf_field() }}

	@include('components.form.text', [
		'label' => __('Output address'),
		'name' => 'address',
		'required' => true
	])

	@include('components.form.text', [
		'label' => __('Quantity'),
		'name' => 'quantity',
		'placeholder' => __('Available balance') . ': ' . btc_format($wallet->balance_available) . ' ' . $wallet->coin,
		'required' => true
	])

	@include('components.form.password', [
		'label' => __('Secondary password'),
		'name' => 'secondary_password',
		'required' => true,
		'disabled' => ! Auth::user()->secondary_password,
		'placeholder' => ! Auth::user()->secondary_password ? __('You have not set a password yet, go to your personal center settings.') : null
	])

	@includeWhen(Auth::user()->google2fa_secret, 'components.form.number', [
		'label' => __('Google Verification Code'),
		'name' => 'google2fa',
		'placeholder' => __('Please enter a 6-digit Google Verification Code'),
		'required' => true
	])

	@include('components.form.text', [
		'label' => __('Remark'),
		'name' => 'remark'
	])

	<div class="form-group">
		<div class="col-md-9 col-md-offset-3">
			<button type="submit" class="btn btn-primary">{{ __('Send') }}</button>
		</div>
	</div>
</form>