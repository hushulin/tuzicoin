
<p>{{ __('Tips: :coin usually takes 10 minutes to send :coin network, :coin network transfer fee is :network_out_fee :coin. :app_name will be reviewed and then broadcast to the :coin network, is being processed on behalf of the platform is being reviewed, you can handle the transaction details to see the transfer details. The mutual transfer between the platform and the :app_name user is real-time and free of charge.', [ 'app_name' => config('app.name'), 'coin' => $wallet->coin, 'network_out_fee' => C('network_out_fee_' . strtolower($wallet->coin)) ]) }}</p>

@include('wallet.withdraw.send')

@if ( ! $withdraws->isEmpty())
<table class="table">
	<thead>
		<tr>
			<th>{{ __('Processing network out') }}</th>
			<th>{{ __('Output address') }}</th>
			<th>{{ __('Quantity') }}</th>
			<th>{{ __('Remark') }}</th>
		</tr>
	</thead>
	<tbody>
		@each('wallet.withdraw.item', $withdraws, 'data')
	</tbody>
</table>
@endif
