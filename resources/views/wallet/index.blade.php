@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		@if (count(C('coins')) > 1)
			<div class="col-md-10 col-md-offset-1">
				<ul class="nav nav-tabs currency-tabs">
					@foreach (C('coins') as $coin)
						<li role="presentation" @if ($wallet->coin == $coin) class="active" @endif ><a href="{{ route('wallet', array_merge(request()->all(), [ 'coin' => $coin ])) }}">{{ $coin }}</a></li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="balance">
						<h3>{{ __('Available') }}: <strong>{{ btc_format($wallet->balance_available) }}</strong> {{ $wallet->coin }}</h3>
						<ul>
							<li>{{ __('Available balance') }}: {{ btc_format($wallet->balance_available) }} {{ $wallet->coin }}</li>
							<li>{{ __('Locked balance') }}: {{ btc_format($wallet->balance_locked) }} {{ $wallet->coin }}</li>
							<li>{{ __('Total balance') }}: {{ btc_format($wallet->balance_total) }} {{ $wallet->coin }}</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">
					<ul class="nav nav-pills">
						<li role="presentation" @if ($tab == 'deposit') class="active" @endif ><a href="{{ route('wallet', array_merge(request()->all(), [ 'tab' => 'deposit' ])) }}">{{ __('Receive ' . $wallet->coin) }}</a></li>
						<li role="presentation" @if ($tab == 'withdraw') class="active" @endif ><a href="{{ route('wallet', array_merge(request()->all(), [ 'tab' => 'withdraw' ])) }}">{{ __('Send ' . $wallet->coin) }}</a></li>
					</ul>
				</div>
				<div class="panel-body">
					@include('components.message')

					<div class="tab-content">
						<div role="tabpanel" class="tab-pane {{ $tab == 'deposit' ? 'active' : '' }}" id="deposit">
							@include('wallet.deposit.panel')
						</div>
						<div role="tabpanel" class="tab-pane {{ $tab == 'withdraw' ? 'active' : '' }}" id="withdraw">
							@include('wallet.withdraw.panel')
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-10 col-md-offset-1">
			@include('wallet.transaction.panel')
		</div>
	</div>
</div>
@endsection
