<div class="panel panel-default">
	<div class="panel-heading">{{ __('Transaction Record') }}</div>

	<div class="panel-body">
		<ul class="nav nav-tabs">
			<li @if ( ! array_key_exists(request('type'), trans('transaction.type'))) class="active" @endif >
				<a href="{{ route('wallet', request()->except('type', 'page')) }}">{{ __('All') }}</a>
			</li>
			@foreach (trans('transaction.type') as $type => $text)
				<li @if (request('type') == $type) class="active" @endif >
					<a href="{{ route('wallet', array_merge(request()->except('page'), [ 'type' => $type ])) }}">{{ $text }}</a>
				</li>
			@endforeach
		</ul>
	</div>
	<div class="panel-body">
		@if ($transactions->isEmpty())
			<p class="text-muted">{{ __('No data') }}</p>
		@else
			<table class="table">
				<thead>
					<tr>
						<th>{{ __('Date time') }}</th>
						<th>{{ __('Type') }}</th>
						<th>{{ __('Amount') }}</th>
						<th>{{ __('Description') }}</th>
					</tr>
				</thead>
				<tbody>
					@each('wallet.transaction.item', $transactions, 'data')
				</tbody>
			</table>
			{{ $transactions->links() }}
		@endif
	</div>
</div>