{{ __('Seller') }}: {{ $data->target->username }}
&nbsp;&nbsp;
{{ __('Final price') }}: {{ btc_format($data->price) }} {{ $data->currency_code }}
@if ($data->fee > 0)
	&nbsp;&nbsp;
	{{ __('Fee') }}: {{ btc_format($data->fee) }} {{ $data->coin }}
@endif