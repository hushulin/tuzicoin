<tr>
	<td>{{ $data->created_at }}</td>
	<td>{{ trans('transaction.type.' . $data->type) }}</td>
	<td>{{ btc_format($data->amount) }} {{ $data->coin }}</td>
	<td>@include('wallet.transaction.' . kebab_case($data->type))</td>
</tr>