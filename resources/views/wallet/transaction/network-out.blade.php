{{ __('Output address') }}: {{ $data->address }}<br>
@if($data->txid)
	txid: {{ $data->txid }}<br>
@endif
{{ __('Fee') }}: {{ btc_format($data->fee) }} {{ $data->coin }}<br>
@if ($data->remark)
	{{ __('Remark') }}: {{ $data->remark }}
@endif