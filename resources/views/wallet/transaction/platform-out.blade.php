{{ __('Output address') }}: {{ $data->address }}<br>
{{ __('Target user') }}: {{ $data->target->username }}<br>
@if ($data->remark)
	{{ __('Remark') }}: {{ $data->remark }}
@endif