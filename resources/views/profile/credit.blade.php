<div class="credit">
	<dl>
		<dt>{{ __('Trades') }}</dt>
		<dd>{{ $user->trade_count }}</dd>
	</dl>
	<dl>
		<dt>{{ __('Trusted') }}</dt>
		<dd>{{ $user->trust_count }}</dd>
	</dl>
	<dl>
		<dt>{{ __('Rating') }}</dt>
		<dd>{{ $user->rating }}%</dd>
	</dl>
	<dl>
		<dt>{{ __('Volume') }}</dt>
		<dd>
			@php
				$coins = C('coins');
				if (isset($counter)):
					$coins = [ $counter->coin ];
				endif;
			@endphp
			@foreach ($coins as $coin)
				{{ $user->getAttribute('vague_volume_' . strtolower($coin)) }} {{ $coin }}
				@if ( ! $loop->last) / @endif
			@endforeach
		</dd>
	</dl>
</div>