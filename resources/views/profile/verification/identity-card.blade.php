<div class="panel panel-primary">
	<div class="panel-heading">
		<h4>
			{{ __('Chinese Identity Card') }}
			@if ($authentication)
				<span class="label label-success">{{ __('Verified') }}</span>
			@else
				<span class="label label-default">{{ __('Unverified') }}</span>
			@endif
		</h4>
		{{ __('Applicable to most Chinese mainland users') }}
	</div>
	<div class="panel-body">

		@includeWhen($authentication, 'profile.verification.info')

		@if ( ! $authentication)
			<form class="form-horizontal" method="POST" action="{{ route('profile.verification.identity-card') }}">
				{{ csrf_field() }}

				@include('components.form.text', [
					'label' => __('Real name'),
					'name' => 'realname',
					'placeholder' => __('Please enter your real name')
				])

				@include('components.form.text', [
					'label' => __('ID card number'),
					'name' => 'id_number',
					'placeholder' => __('Please enter your ID card number')
				])

				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<button type="submit" class="btn btn-primary">
							{{ __('OK') }}
						</button>
					</div>
				</div>
			</form>
		@endif
	</div>
</div>