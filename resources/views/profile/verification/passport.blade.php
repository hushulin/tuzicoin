<div class="panel panel-primary">
	<div class="panel-heading">
		<h4>
			{{ __('Passport') }}
			@if ($authentication)
				<span class="label label-success">{{ __('Verified') }}</span>
			@else
				<span class="label label-default">{{ __('Unverified') }}</span>
			@endif
		</h4>
		{{ __('Applicable to mainland China Hong Kong and other foreign users') }}
	</div>
	<div class="panel-body">

		@includeWhen($authentication, 'profile.verification.info')

		@if ( ! $authentication)
			<div class="alert alert-info" role="alert">
				<h5>{{ __('Upload pictures Note') }}</h5>
				<ul>
					<li>{{ __('Your uploaded passport image is real and effective.') }}</li>
					<li>{{ __('The information of your passport image is clearly visible.') }}</li>
					<li>{{ __('Your uploaded image is not reflective or creased.') }}</li>
					<li>{{ __('Your uploaded image is a color image.') }}</li>
				</ul>
			</div>

			<form class="form-horizontal" method="POST" action="{{ route('profile.verification.passport') }}" enctype="multipart/form-data">
				{{ csrf_field() }}

				@include('components.form.file', [
					'label' => __('Upload Image'),
					'name' => 'image',
					'help' => __('Please upload your passport, the picture is less than 2M')
				])

				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<button type="submit" class="btn btn-primary">
							{{ __('Submit') }}
						</button>
					</div>
				</div>
			</form>
		@endif
	</div>
</div>