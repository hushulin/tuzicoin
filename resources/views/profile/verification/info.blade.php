<form class="form-horizontal">

	@includeWhen($authentication->realname, 'components.form.static', [
		'label' => __('Real name'),
		'value' => $authentication->realname
	])

	@includeWhen($authentication->id_number, 'components.form.static', [
		'label' => __('ID card number'),
		'value' => $authentication->id_number
	])

	@includeWhen( ! empty($authentication->extra['image']), 'components.form.image', [
		'src' => route('profile.verification.image')
	])

</form>