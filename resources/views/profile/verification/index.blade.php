@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Authentication') }}</div>

	<div class="panel-body">
		@include('components.message')

		@if ($authentication)
			<div class="alert alert-warning" role="alert">
				<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
				{{ __('You have verified your real name.') }}
			</div>
		@else
			<div class="alert alert-warning" role="alert">
				<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
				{{ __('Please complete the authentication process to protect your transactions and your legal rights! For a higher level of security protection, we provide safe and efficient real-time networking official identity authentication services. Once the authentication is successful, it can\'t be modified or de-registered. Please carefully provide accurate identity information.') }}
			</div>
		@endif

		@includeWhen( ! $authentication || $authentication->type == App\Models\Authentication::TYPE_IDENTITY_CARD, 'profile.verification.identity-card')
		@includeWhen( ! $authentication || $authentication->type == App\Models\Authentication::TYPE_PASSPORT, 'profile.verification.passport')
		@includeWhen( ! $authentication || $authentication->type == App\Models\Authentication::TYPE_DRIVER_LICENSE, 'profile.verification.driver-license')

	</div>
</div>
@endsection
