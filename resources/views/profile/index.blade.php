@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Basic information') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('profile.save') }}">
			{{ csrf_field() }}

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<user :user_id="{{ Auth::id() }}" class="avatar-edit" :show-online="false" :edit-enable="true" :link-enable="false"></user>
				</div>
			</div>

			@include('components.form.static', [
				'label' => __('UID'),
				'value' => Auth::user()->id
			])

			@include('components.form.static', [
				'label' => __('Authentication'),
				'value' => Auth::user()->authentication_id ? __('Verified') : __('Unverified')
			])

			@include('components.form.static', [
				'label' => __('Email'),
				'value' => Auth::user()->email_verified ? __('Verified') : __('Unverified')
			])

			@include('components.form.static', [
				'label' => __('Mobile'),
				'value' => Auth::user()->mobile_verified ? __('Verified') : __('Unverified')
			])

			@include('components.form.static', [
				'label' => __('Signup time'),
				'value' => Auth::user()->created_at
			])

			@include('components.form.static', [
				'label' => __('Trusted'),
				'value' => __('Trusted by :count people', [ 'count' => number_format(Auth::user()->trust_count) ])
			])

			@include('components.form.static', [
				'label' => __('Trade count'),
				'value' => number_format(Auth::user()->trade_count)
			])

			@php
				$text = [];
				foreach (C('coins') as $coin):
					$text[] = btc_format(Auth::user()->getAttribute('volume_' . strtolower($coin))) . ' ' . $coin;
				endforeach;
				$text = join(' / ', $text);
			@endphp
			@include('components.form.static', [
				'label' => __('Volume'),
				'value' => $text
			])

			@include('components.form.textarea', [
				'label' => __('Introduction'),
				'name' => 'introduction',
				'value' => Auth::user()->introduction,
				'placeholder' => __('Display your presentation on your public profile.'),
				'help' => __('Please do not fill in your :app_name registration email to avoid receiving phishing mail.', [ 'app_name' => config('app.name') ]),
				'rows' => 3
			])

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Save') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@push('data')
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	window.users[{{ Auth::user()->id }}] = {!! Auth::user() !!};
</script>
@endpush