<div class="user">
	<div class="avatar">
		<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
		<span class="user-status {{ $user->online ? 'online' : '' }}" data-id="{{ $user->id }}"></span>
	</div>
	<div class="username">
		@if (Route::is('user'))
			{{ $user->username }}
		@else
			<a href="{{ route('user', $user) }}">{{ $user->username }}</a>
		@endif
	</div>
</div>