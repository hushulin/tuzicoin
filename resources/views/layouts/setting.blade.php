@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-push-2">
			@yield('main')
		</div>
		<div class="col-md-2 col-md-pull-10">
			<div class="panel panel-default">
				<div class="panel-heading">{{ __('User profile') }}</div>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="{{ route('profile') }}">{{ __('Basic information') }}</a></li>
					<li><a href="{{ route('profile.verification') }}">{{ __('Authentication') }}</a></li>
				</ul>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">{{ __('Account security') }}</div>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="{{ route('security') }}">{{ __('Security settings') }}</a></li>
					<li><a href="{{ route('pay.info.setting') }}">{{ __('Pay info') }}</a></li>
				</ul>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">{{ __('Manage counter') }}</div>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="{{ route('counter.list', [ 'type' => App\Models\Counter::TYPE_ONLINE_BUY, 'status' => App\Models\Counter::STATUS_OPEN ]) }}">{{ __('Buying') }}</a></li>
					<li><a href="{{ route('counter.list', [ 'type' => App\Models\Counter::TYPE_ONLINE_SELL, 'status' => App\Models\Counter::STATUS_OPEN ]) }}">{{ __('Selling') }}</a></li>
				</ul>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">{{ __('Trusted') }}</div>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="{{ route('trust.by') }}">{{ __('Trusted by') }}</a></li>
					<li><a href="{{ route('trust.my') }}">{{ __('Trusting') }}</a></li>
					<li><a href="{{ route('ignore.my') }}">{{ __('Blocked') }}</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection
