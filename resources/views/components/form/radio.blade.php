{{--
	$options				选项数据
	$label					标签名
	$name					表单名
	$value					默认值
	$help					帮助文本
	$required				必填项Key列表
	$disabled				禁用项的Key列表
	$readonly			只读项的Key列表
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}" id="form-group-{{ $name }}">
	<label class="col-md-3 control-label">{{ $label }}</label>

	<div class="col-md-6">
		@foreach ($options as $option => $text)
			<label for="input-{{ $name }}-{{ $option }}" class="radio-inline">
				<input
					type="radio"
					id="input-{{ $name }}-{{ $option }}"
					name="{{ $name }}"
					value="{{ $option }}"
					@if (isset($readonly) && in_array($option, (array) $readonly)) readonly="readonly" @endif
					@if (isset($disabled) && in_array($option, (array) $disabled)) disabled="disabled" @endif
					@if (isset($required) && in_array($option, (array) $required)) required="required" @endif
					@if ($option == old($name, @$value)) checked @endif
				>
				{{ $text }}
			</label>
		@endforeach

		@if ($help ?? '')
			<p id="help-block-{{ $name }}" class="help-block">{{ $help }}</p>
		@endif
		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>