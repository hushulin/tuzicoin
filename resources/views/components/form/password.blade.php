{{--
	$label					标签名
	$name					表单名
	$value					默认值
	$placeholder		提示文本
	$help					帮助文本
	$addon_before	额外元素-前
	$addon_after		额外元素-后
	$disabled				是否禁用
	$required				是否必填
	$autofocus			是否自动获得焦点
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}" id="form-group-{{ $name }}">
	<label for="input-{{ $name }}" class="col-md-3 control-label">{{ $label }}</label>

	<div class="col-md-6">
		@if (isset($addon_after) || isset($addon_before)) <div class="input-group"> @endif
			@if (isset($addon_before))
				<span class="input-group-addon" id="input-text-{{ $name }}-addon_before">{{ $addon_before }}</span>
			@endif
			<input
				id="input-{{ $name }}"
				type="password"
				class="form-control"
				name="{{ $name }}"
				value="{{ @$value }}"
				placeholder="{{ $placeholder or $label }}"
				@if (isset($disabled) && $disabled) disabled="disabled" @endif
				@if (isset($required) && $required) required="required" @endif
				@if (isset($autofocus) && $autofocus) autofocus="autofocus" @endif
			>
			@if (isset($addon_after))
				<span class="input-group-addon" id="input-text-{{ $name }}-addon_after">{{ $addon_after }}</span>
			@endif
		@if (isset($addon_after) || isset($addon_before)) </div> @endif
		@if ($help ?? '')
			<p id="help-block-{{ $name }}" class="help-block">{{ $help }}</p>
		@endif
		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>