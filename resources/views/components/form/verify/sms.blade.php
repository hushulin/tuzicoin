{{--
	$label							标签名
	$name							表单名
	$placeholder				提示文本
	$help							帮助文本
	$autofocus					是否自动获得焦点
	$elem_country_code	国家代码编辑框Name
	$elem_mobile				手机号编辑框Name
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
	<label for="input-{{ $name }}" class="col-md-3 control-label">{{ __($label) }}</label>

	<div class="col-md-6">
		<div class="input-group">
			<input
				id="input-{{ $name }}"
				type="tel"
				class="form-control"
				name="{{ $name }}"
				placeholder="{{ $placeholder or $label }}"
				required="required"
				@if (isset($autofocus) && $autofocus) autofocus="autofocus" @endif
			>
			<span class="input-group-btn" id="sms-{{ $name }}">
				<button class="btn btn-default" type="button">{{ __('Send') }}</button>
			</span>
		</div>

		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>

@push('script')
<script>
	$('#sms-{{ $name }} .btn').sendVerificationMobile('{{ $elem_country_code or 'country_code' }}', '{{ $elem_mobile or 'mobile' }}');
</script>
@endpush