{{--
	$label							标签名
	$name							表单名
	$placeholder				提示文本
	$help							帮助文本
	$autofocus					是否自动获得焦点
	$elem_email				电子邮件编辑框Name
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
	<label for="input-{{ $name }}" class="col-md-3 control-label">{{ __($label) }}</label>

	<div class="col-md-6">
		<div class="input-group">
			<input
				id="input-{{ $name }}"
				type="tel"
				class="form-control"
				name="{{ $name }}"
				placeholder="{{ $placeholder or $label }}"
				required="required"
				@if (isset($autofocus) && $autofocus) autofocus="autofocus" @endif
			>
			<span class="input-group-btn" id="email-{{ $name }}">
				<button class="btn btn-default" type="button">{{ __('Send') }}</button>
			</span>
		</div>

		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>

@push('head')
<style>
	#email-{{ $name }} { padding: 0; overflow: hidden; cursor: pointer; }
	#email-{{ $name }} img { width: 128px; height: 34px; }
</style>
@endpush

@push('script')
<script>
	$('#email-{{ $name }} .btn').sendVerificationEmail('{{ $elem_email or 'email' }}');
</script>
@endpush