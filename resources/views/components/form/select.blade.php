{{--
	$options				选项数据
	$label					标签名
	$name					表单名
	$value					默认值
	$help					帮助文本
	$required				是否必填
	$autofocus			是否自动获得焦点
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}" id="form-group-{{ $name }}">
	<label for="input-{{ $name }}" class="col-md-3 control-label">{{ $label }}</label>

	<div class="col-md-6">
		<select
			id="input-{{ $name }}"
			class="form-control"
			name="{{ $name }}"
			@if (isset($required) && $required) required="required" @endif
			@if (isset($autofocus) && $autofocus) autofocus="autofocus" @endif
		>
			@foreach ($options as $option => $text)
				<option value="{{ $option }}"
					@if (old($name, @$value) == $option) selected @endif
				>
					{{ $text }}
				</option>
			@endforeach
		</select>
		@if ($help ?? '')
			<p id="help-block-{{ $name }}" class="help-block">{{ $help }}</p>
		@endif
		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>