{{--
	$label					标签名
	$name					表单名
	$value					值
	$help					帮助文本
--}}
<div class="form-group" @if (isset($name)) id="form-group-{{ $name }}" @endif >
	<label class="col-md-3 control-label">{{ $label or '' }}</label>

	<div class="col-md-6">
		@if (isset($name))
			<input type="hidden" name="{{ $name }}" value="{{ $value or '' }}">
		@endif
		<p class="form-control-static">{{ $value }}</p>
		@if ($help ?? '')
			<p class="help-block">{{ $help }}</p>
		@endif
	</div>
</div>