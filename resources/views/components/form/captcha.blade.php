{{--
	$label					标签名
	$name					表单名
	$placeholder		提示文本
	$help					帮助文本
	$autofocus			是否自动获得焦点
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}" id="form-group-{{ $name }}">
	<label for="input-{{ $name }}" class="col-md-3 control-label">{{ __($label) }}</label>

	<div class="col-md-6">
		<div class="input-group">
			<input
				id="input-{{ $name }}"
				type="text"
				class="form-control"
				name="{{ $name }}"
				placeholder="{{ $placeholder or $label }}"
				required="required"
				@if (isset($autofocus) && $autofocus) autofocus="autofocus" @endif
			>
			<span class="input-group-addon" id="captcha-{{ $name }}">
				<img src="{{ Captcha::url() }}">
			</span>
		</div>

		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>

@push('head')
<style>
	#captcha-{{ $name }} { padding: 0; overflow: hidden; cursor: pointer; }
	#captcha-{{ $name }} img { width: 128px; height: 34px; }
</style>
@endpush

@push('script')
<script>
	$('#captcha-{{ $name }}').on('click', function(){
		$('img', this).attr('src', '{{ Captcha::url() }}&' + Math.random());
	});
</script>
@endpush