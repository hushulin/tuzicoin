{{--
	$label					标签名
	$name					表单名
	$src						图片的src
	$value					值
--}}
<div class="form-group" @if (isset($name)) id="form-group-{{ $name }}" @endif >
	<label class="col-md-3 control-label">{{ $label or '' }}</label>

	<div class="col-md-6">
		@if (isset($name))
			<input type="hidden" name="{{ $name }}" value="{{ $value or '' }}">
		@endif
		<p class="form-control-static">
			<img src="{{ $src }}" class="img-thumbnail">
		</p>
	</div>
</div>