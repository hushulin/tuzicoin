{{--
	$label					标签名
	$name					表单名
	$help					帮助文本
	$addon_before	额外元素-前
	$addon_after		额外元素-后
	$required				是否必填
	$autofocus			是否自动获得焦点
	$data					绑定到元素的数据
	$value
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}" id="form-group-{{ $name }}">
    <label for="input-{{ $name }}" class="col-md-3 control-label">{{ $label }}</label>

    <div class="col-md-6">
        @if (isset($addon_after) || isset($addon_before))
            <div class="input-group"> @endif
                @if (isset($addon_before))
                    <span class="input-group-addon" id="input-text-{{ $name }}-addon_before">{{ $addon_before }}</span>
                @endif
                <input
                        id="input-{{ $name }}"
                        type="file"
                        class="form-control"
                        name="{{ $name }}"
                        accept = @if(!empty($accept))"{{ $accept }}"@else"image/*"@endif
                        onchange = "checkFileExt(this.value)"
                        @if (isset($required) && $required) required="required" @endif
                        @if (isset($autofocus) && $autofocus) autofocus="autofocus" @endif
                        @if (isset($data) && is_array($data))
                        @foreach ($data as $key => $value)
                        data-{{ $key }}="{{ $value }}"
                        @endforeach
                        @endif
                >
                @if (isset($addon_after))
                    <span class="input-group-addon" id="input-text-{{ $name }}-addon_after">{{ $addon_after }}</span>
                @endif
                @if (isset($addon_after) || isset($addon_before)) </div> @endif
        @if ($help ?? '')
            <p id="help-block-{{ $name }}" class="help-block">{{ $help }}</p>
        @endif
        @if ($errors->has($name))
            <span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
        @endif
        @if(isset($value))
            <img src="{{$value}}" class="img-responsive" style="width: 100px" alt="">
        @endif
    </div>
</div>

<script type="text/javascript">
    //3、（字符）检查文件上传表单控件，如果含有[jpg,jpeg,gif,png]则显示“文件类型合法”，否则“显示文件类型错误”
    function checkFileExt(filename)
    {
        return true;
        var flag = false; //状态
        var arr = ["jpg","png","gif"];
        //取出上传文件的扩展名
        var index = filename.lastIndexOf(".");
        var ext = filename.substr(index+1);
        //循环比较
        for(var i=0;i<arr.length;i++)
        {
            if(ext == arr[i])
            {
                flag = true; //一旦找到合适的，立即退出循环
                break;
            }
        }
        //条件判断
        if(flag)
        {
            document.write("文件名合法");
        }else
        {
            document.write("文件名不合法");
        }
    }
</script>