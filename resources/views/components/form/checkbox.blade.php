{{--
	$options				选项数据
	$label					标签名
	$name					表单名
	$value					默认值
	$help					帮助文本
	$required				必填项Key列表
--}}
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}" id="form-group-{{ $name }}">
	<label class="col-md-3 control-label">{{ $label }}</label>

	<div class="col-md-6">
		@foreach ($options as $option => $text)
			<label for="input-{{ $name }}-{{ $option }}" class="checkbox-inline">
				<input
					type="checkbox"
					id="input-{{ $name }}-{{ $option }}"
					name="{{ $name }}[]"
					value="{{ $option }}"
					@if (isset($required) && in_array($option, (array) $required)) required="required" @endif
					@if (in_array($option, old($name, [@$value]))) checked @endif
				>
				{{ $text }}
			</label>
		@endforeach

		@if ($errors->has($name))
			<span class="help-block">
				<strong>{{ $errors->first($name) }}</strong>
			</span>
		@endif
	</div>
</div>