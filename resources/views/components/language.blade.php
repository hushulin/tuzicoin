<div id="language-select">
	<ul>
		<li><a href="{{ route(Route::currentRouteName(), array_merge(Request::all(), [ 'language' => 'en' ])) }}">English</a></li>
		<li><a href="{{ route(Route::currentRouteName(), array_merge(Request::all(), [ 'language' => 'zh-CN' ])) }}">简体中文</a></li>
		<li><a href="{{ route(Route::currentRouteName(), array_merge(Request::all(), [ 'language' => 'zh-HK' ])) }}">繁体中文</a></li>
	</ul>
</div>