<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="alertModalLabel">{{ __('Message') }}</h4>
			</div>
			<div class="modal-body" id="alertModalContent"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal" id="alertModalBtnPrimary">{{ __('OK') }}</button>
			</div>
		</div>
	</div>
</div>