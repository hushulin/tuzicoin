@extends('layouts.setting')

@section('main')
<div class="panel panel-default">
	<div class="panel-heading">{{ __('Login password') }}</div>

	<div class="panel-body">
		@include('components.message')

		<form class="form-horizontal" method="POST" action="{{ route('password.login') }}">
			{{ csrf_field() }}

			<div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
				<label for="old_password" class="col-md-3 control-label">{{ __('Old password') }}</label>

				<div class="col-md-6">
					<input id="old_password" type="password" class="form-control" name="old_password" required>

					@if ($errors->has('old_password'))
						<span class="help-block">
							<strong>{{ $errors->first('old_password') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
				<label for="new_password" class="col-md-3 control-label">{{ __('New password') }}</label>

				<div class="col-md-6">
					<input id="new_password" type="password" class="form-control" name="new_password" required>

					@if ($errors->has('new_password'))
						<span class="help-block">
							<strong>{{ $errors->first('new_password') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
				<label for="new_password_confirmation" class="col-md-3 control-label">{{ __('Confirm new password') }}</label>

				<div class="col-md-6">
					<input id="new_password_confirmation" type="password" class="form-control" name="new_password_confirmation" required>

					@if ($errors->has('new_password_confirmation'))
						<span class="help-block">
							<strong>{{ $errors->first('new_password_confirmation') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						{{ __('Modify') }}
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
