@extends('root.layouts.master')

@section('title', '编辑文章')
@section('small-title', '编辑系统内的文章。')

@section('breadcrumb')
<li>内容管理</li>
<li><a href="{{ route('RootArticleList') }}">文章管理</a></li>
<li><a href="{{ route('RootArticleEdit', [ 'id' => $data ? $data->id : '' ]) }}">编辑文章</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">文章内容</header>
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="{{ route('RootArticleSave') }}">
				{!! csrf_field() !!}
				@if ($data)
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">#</label>
						<div class="col-lg-10">
							<input type="hidden" name="id" value="{{ $data->id }}">
							<p class="form-control-static">{{ $data->id }}</p>
						</div>
					</div>
				@endif


				<div class="form-group">
					<label class="col-sm-2 col-sm-2 control-label">文章分类</label>
					<div class="col-sm-10">
						<select class="form-control" name="type">
							@foreach ([
								'0' => '请选择',
								'1' => '重要公告',
								'2' => '帮助'
							] as $key => $text)
							<option value="{{$key}}" @if($data->type==$key)selected="true"@endif>{{ $text }}</option>
							@endforeach
						</select>
					</div>
				</div>

				@foreach ([
					'en' => '英文',
					'zh-CN' => '简体中文',
					'zh-HK' => '繁体中文'
				] as $language => $language_text)
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">标题（{{ $language_text }}）</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" name="title_{{ $language }}" value="{{ Request::old('title_' . $language, $data ? $data->getAttribute('title_' . $language) : '' ) }}">
						</div>
					</div>
				@endforeach
				@foreach ([
					'en' => '英文',
					'zh-CN' => '简体中文',
					'zh-HK' => '繁体中文'
				] as $language => $language_text)
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">内容（{{ $language_text }}）</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="content_{{ $language }}"  rows="7" id="summernote_{{ $language }}">{{ Request::old('content_' . $language, $data ? $data->getAttribute('content_' . $language) : '' ) }}</textarea>
						</div>
					</div>
				@endforeach
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">保存</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection

@push('head')
<link href="{{ asset('slicklab/js/summernote/summernote.css') }}" rel="stylesheet">
@endpush

@push('script')
<script src="{{ asset('slicklab/js/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('slicklab/js/summernote/lang/summernote-zh-CN.js') }}"></script>
<script>
	@foreach ([
		'en',
		'zh-CN',
		'zh-HK'
	] as $language)
		$('#summernote_{{ $language }}').summernote({
			lang: 'zh-CN',
			height: 500
		});
	@endforeach
</script>
@endpush
