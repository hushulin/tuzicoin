@extends('root.layouts.master')

@section('title', '文章管理')
@section('small-title', '关于我们、系统文章等内容页面。')

@section('breadcrumb')
<li>内容管理</li>
<li><a href="{{ route('RootArticleList') }}">文章管理</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2-bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                文章列表
            </header>
            <div class="tbl-head clearfix">
				<div class="btn-group">
					<a class="btn btn-primary btn-sm" href="{{ route('RootArticleEdit') }}"><i class="fa fa-plus"></i> 创建</a>
				</div>
			</div>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only" for="InputAdminId">管理员</label>
						<select id="InputAdminId" class="form-control select2-multiple" name="admin_id" style="height: 33px; min-width: 180px;">
							<option value=""></option>
						@foreach (App\Models\Admin::all() as $admin)
							<option value="{{ $admin->id }}" @if ($admin->id == Request::get('admin_id')) selected @endif >{{ $admin->username }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only">标题（模糊搜索）</label>
						<input class="form-control" type="text" name="title" value="{{ Request::get('title') }}" placeholder="标题（模糊搜索）">
					</div>
					<div class="form-group">
						<label class="sr-only">内容（模糊搜索）</label>
						<input class="form-control" type="text" name="content" value="{{ Request::get('content') }}" placeholder="内容（模糊搜索）">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th class="hidden-xs">#</th>
							<th>管理员</th>
							<th class="hidden-xs">标题</th>
							<th class="hidden-xs">内容</th>
							<th>时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr data-id="{{ $item->id }}">
							<td class="hidden-xs">{{ $item->id }}</td>
							<td>{{ $item->admin->username }}</td>
							<td class="hidden-xs">{{ str_limit($item->title, 15) }}</td>
							<td class="hidden-xs">{{ str_limit(strip_tags($item->content), 20) }}</td>
							<td>{{ $item->created_at }}</td>
							<td>
								<a class="btn btn-xs btn-info" href="{{ route('RootArticleEdit', ['id'=>$item->id]) }}"><i class="fa fa-edit"></i> 编辑</a>
								<button class="btn btn-xs btn-danger" type="button" data-role="delete"><i class="fa fa-trash-o"></i> 删除</button>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/select2.js') }}"></script>
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$(function(){
		$('#InputAdminId').select2({
			minimumResultsForSearch: 10,
			allowClear: true,
			placeholder: '管理员'
		});
	});
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');

	$('[data-role="delete"]').on('click', function(){
		var data = $(this).parents('tr').data();
		iconfirm({
			action	: '{{ route('RootArticleDelete') }}',
			body		: '确定要删除编号 ' + data.id + ' 的文章吗？',
			data		: data
		});
		return false;
	});
</script>
@endpush
