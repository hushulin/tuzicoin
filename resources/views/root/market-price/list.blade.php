@extends('root.layouts.master')

@section('title', '市场价数据')
@section('small-title', '比特币在各个交易市场中的价格数据。')

@section('breadcrumb')
<li>系统管理</li>
<li><a href="{{ route('RootMarketPriceList') }}">市场价数据</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                市场价列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">市场</label>
						<input class="form-control" type="text" name="market" value="{{ Request::get('market') }}" placeholder="市场">
					</div>
					<div class="form-group">
						<label class="sr-only">货币</label>
						<input class="form-control" type="text" name="currency" value="{{ Request::get('currency') }}" placeholder="货币">
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>市场</th>
							<th>币种</th>
							<th>货币</th>
							<th>最新价</th>
							<th>最低要价(买一价)</th>
							<th>最高出价(卖一价)</th>
							<th>数据时间</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->market }}</td>
							<td>{{ $item->coin }}</td>
							<td>{{ $item->currency }}</td>
							<td>{{ $item->last }}</td>
							<td>{{ $item->buy }}</td>
							<td>{{ $item->sell }}</td>
							<td>{{ $item->updated_at }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection
