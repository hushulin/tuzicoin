@extends('root.layouts.master')

@section('title', '系统配置')
@section('small-title', '编辑全局系统配置。')

@section('breadcrumb')
<li>系统管理</li>
<li><a href="{{ route('RootConfigEdit') }}">系统配置</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">系统配置</header>
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="{{ route('RootConfigSave') }}">
				{!! csrf_field() !!}
				@foreach ($data as $item)
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">{{ $item->name }}</label>
						<div class="col-sm-10">
							@if ($item->type == App\Models\Config::TYPE_TEXT)
								@if ($item->unit)
									<div class="input-group m-b-10">
										<input class="form-control" type="text" name="data[{{ $item->key }}]" value="{{ Request::old('data.' . $item->key, $item->value) }}">
										<span class="input-group-addon">{{ $item->unit }}</span>
									</div>
								@else
									<input class="form-control" type="text" name="data[{{ $item->key }}]" value="{{ Request::old('data.' . $item->key, $item->value) }}">
								@endif
							@elseif ($item->type == App\Models\Config::TYPE_SINGLE_OPTION)
								@foreach ($item->options as $key => $text)
									<label class="radio-custom inline radio-info">
				                        <input type="radio" name="data[{{ $item->key }}][]" value="{{ $key }}" id="{{ $item->key }}-{{ $key }}"
											@if (in_array($key, (array)Request::old('options', $item->value))) checked @endif
										>
										<label for="{{ $item->key }}-{{ $key }}">{{ $text }}</label>
				                    </label>
								@endforeach
							@elseif ($item->type == App\Models\Config::TYPE_MULTIPLE_OPTION)
								@foreach ($item->options as $key => $text)
									<label class="checkbox-custom inline check-info">
				                        <input type="checkbox" name="data[{{ $item->key }}][]" value="{{ $key }}" id="{{ $item->key }}-{{ $key }}"
											@if (in_array($key, (array)Request::old('options', $item->value))) checked @endif
										>
										<label for="{{ $item->key }}-{{ $key }}">{{ $text }}</label>
				                    </label>
								@endforeach
							@endif
							<p class="help-block">{{ $item->description }}</p>
						</div>
					</div>
				@endforeach
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">保存</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection
