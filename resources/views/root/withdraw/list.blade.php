@extends('root.layouts.master')

@section('title', '提现请求')
@section('small-title', '管理所有会员的所有提现申请。')

@section('breadcrumb')
<li>财务流水</li>
<li><a href="{{ route('RootWithdrawList') }}">提现请求</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2-bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                提现列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">会员ID</label>
						<input class="form-control" type="text" name="user_id" value="{{ Request::get('user_id') }}" placeholder="会员ID">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputCoin">币种</label>
						<select id="InputCoin" class="form-control select2-multiple" name="coin[]" multiple="multiple" size="1" style="height: 33px; min-width: 180px;">
							<option value=""></option>
						@foreach (trans('address.coin') as $coin => $text)
							<option value="{{ $coin }}" @if (in_array($coin, (array)Request::get('coin'))) selected @endif >{{ trans('address.coin.' . $coin) }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputStatus">状态</label>
						<select id="InputStatus" class="form-control select2-multiple" name="status[]" multiple="multiple" size="1" style="height: 33px; min-width: 180px;">
							<option value=""></option>
						@foreach (trans('withdraw.status') as $status => $text)
							<option value="{{ $status }}" @if (in_array($status, (array)Request::get('status'))) selected @endif >{{ trans('withdraw.status.' . $status) }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>用户名</th>
							<th>手机</th>
							<th>数量</th>
							<th>目标地址</th>
							<th>备注</th>
							<th>状态</th>
							<th>驳回原因</th>
							<th>申请时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr data-id="{{ $item->id }}">
							<td>{{ $item->id }}</td>
							<td><a href="{{ route('RootUserDetail', [ 'id' => $item->user->id ]) }}">{{ $item->user->username }}</a></td>
							<td>{{ $item->user->mobile }}</td>
							<td>{{ btc_format($item->amount) }} {{ $item->coin }}</td>
							<td>{{ $item->address }}</td>
							<td>@include('root.components.tooltip', [ 'text' => str_limit($item->remark, 10), 'title' => $item->remark ])</td>
							<td>
								@if ($item->status == App\Models\Withdraw::STATUS_SUCCESS)
									<span class="label label-success">{{ trans('withdraw.status.' . $item->status) }}</span>
								@elseif ($item->status == App\Models\Withdraw::STATUS_FAILURE)
									<span class="label label-default">{{ trans('withdraw.status.' . $item->status) }}</span>
								@elseif ($item->status == App\Models\Withdraw::STATUS_PENDING)
									@if ($item->user->balance_available < 0)
										<span class="label label-warning">异常</span>
									@else
										<span class="label label-info">{{ trans('withdraw.status.' . $item->status) }}</span>
									@endif
								@endif
							</td>
							<td>@include('root.components.tooltip', [ 'text' => str_limit($item->reason, 10), 'title' => $item->reason ])</td>
							<td>{{ $item->created_at }}</td>
							<td>
								@if ($item->status == App\Models\Withdraw::STATUS_PENDING)
									<button type="button" class="btn btn-xs btn-success" data-role="completed"><i class="fa fa-check"></i> 完成</button>
									<button type="button" class="btn btn-xs btn-danger" data-role="refuse"><i class="fa fa-times"></i> 拒绝</button>
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>

<div id="RefuseConfirmModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="RefuseConfirmModalLabel" aria-hidden="true">
	<form method="POST" class="modal-dialog" role="document" id="RefuseConfirmModalForm" action="{{ route('RootWithdrawRefuse') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="id" value="">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="RefuseConfirmModalLabel">驳回确认</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<p>确定要拒绝该提现申请吗？</p>
				</div>
				<div class="form-group">
					<label class="control-label">拒绝原因（会展示给用户）</label>
					<textarea class="form-control" rows="3" name="reason"></textarea>
					<span class="help-block" style="display: none;">请填写拒绝原因。</span>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="submit" class="btn btn-danger" data-primary="true">确定</button>
			</div>
		</div>
	</form>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/select2.js') }}"></script>
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$(function(){
		$('#InputCoin').select2({
			allowClear: true,
			placeholder: '币种'
		});
		$('#InputStatus').select2({
			allowClear: true,
			placeholder: '状态'
		});
	});
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');

	$('[data-role="completed"]').on('click', function(){
		var data = $(this).parents('tr').data();
		iconfirm({
			action	: '{{ route('RootWithdrawCompleted') }}',
			body		: '确定该提现申请已经处理完成了吗？',
			data		: data
		});
		return false;
	});
	$('[data-role="refuse"]').on('click', function(){
		var data = $(this).parents('tr').data();

		$('#RefuseConfirmModal')
			.find('[name="id"]').val(data.id).end()
			.one('hidden.bs.modal', function(){
				$(this).find('[name="reason"]').parents('.form-group').removeClass('has-error').find('.help-block').hide();
			})
			.one('shown.bs.modal', function(){
				$(this).find('[name="reason"]').focus();
			})
			.modal('show')
			.find('[name="reason"]').val('');

		return false;
	});
	$('#RefuseConfirmModalForm').on('submit', function(){
		if($(this).find('[name="reason"]').val() == ''){
			$(this).find('[name="reason"]').parents('.form-group').addClass('has-error').find('.help-block').show();
			return false;
		}
	});
</script>
@endpush