@extends('root.layouts.master')

@section('title', '编辑身份验证')
@section('small-title', '编辑身份验证信息。')

@section('breadcrumb')
<li>用户管理</li>
<li><a href="{{ route('RootAuthenticationList') }}">身份验证</a></li>
<li><a href="{{ route('RootAuthenticationEdit', [ 'id' => $data ? $data->id : '' ]) }}">编辑身份验证</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading"> 身份验证信息 </header>
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="{{ route('RootAuthenticationSave') }}">
					{!! csrf_field() !!}
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">编号</label>
						<div class="col-sm-10">
							<input type="hidden" name="id" value="{{ $data->id }}">
							<p class="form-control-static">{{ $data->id }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">类型</label>
						<div class="col-sm-10">
							<p class="form-control-static">{{ trans('authentication.type.' . $data->type) }}</p>
						</div>
					</div>
					@include('root.components.form.text', [
						'label' => '真实姓名',
						'name' => 'realname'
					])
					@include('root.components.form.text', [
						'label' => '身份证号码',
						'name' => 'id_number'
					])
					@if ( ! empty($data->extra['image']))
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">图片</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<img src="{{ route('RootAuthenticationImage', [ 'id' => $data->id ]) }}" class="img-responsive img-rounded">
							</p>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">保存</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection
