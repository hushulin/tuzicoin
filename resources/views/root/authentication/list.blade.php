@extends('root.layouts.master')

@section('title', '身份验证管理')
@section('small-title', '用户提交的身份实名验证信息。')

@section('breadcrumb')
<li>用户管理</li>
<li><a href="{{ route('RootAuthenticationList') }}">身份验证</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2-bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                验证信息列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">编号</label>
						<input class="form-control" type="text" name="id" value="{{ Request::get('id') }}" placeholder="编号">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputType">类型</label>
						<select id="InputType" class="form-control select2-multiple" name="type[]" multiple="multiple" size="1" style="height: 33px; min-width: 180px;">
							<option value=""></option>
						@foreach (trans('authentication.type') as $type => $text)
							<option value="{{ $type }}" @if (in_array($type, (array)Request::get('type'))) selected @endif >{{ trans('authentication.type.' . $type) }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>编号</th>
							<th>会员</th>
							<th>类型</th>
							<th>真实姓名</th>
							<th>身份证号码</th>
							<th>图片</th>
							<th>创建时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr data-id="{{ $item->id }}">
							<td>{{ $item->id }}</td>
							<td><a href="{{ route('RootUserDetail', [ 'id' => $item->user->id ]) }}">{{ $item->user->username }}</a></td>
							<td>{{ trans('authentication.type.' . $item->type) }}</td>
							<td>{{ $item->realname }}</td>
							<td>{{ $item->id_number }}</td>
							<td>
								@if (empty($item->extra['image'])) - @else
									<a href="{{ route('RootAuthenticationImage', [ 'id' => $item->id ]) }}" target="_blank" class="tooltips" data-placement="top" data-html="true" data-original-title="&lt;img src='{{ route('RootAuthenticationImage', [ 'id' => $item->id ]) }}' class='img-rounded' style='max-width: 150px; max-height: 150px;'&gt;">
										<img src="{{ route('RootAuthenticationImage', [ 'id' => $item->id ]) }}" class="img-rounded" style="max-width: 22px; max-height: 22px;">
									</a>
								@endif
							</td>
							<td>{{ $item->created_at }}</td>
							<td>
								<a class="btn btn-xs btn-info" href="{{ route('RootAuthenticationEdit', [ 'id'=>$item->id ]) }}"><i class="fa fa-edit"></i> 编辑</a>
								<button class="btn btn-xs btn-danger" type="button" data-role="delete"><i class="fa fa-trash-o"></i> 删除</button>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/select2.js') }}"></script>
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$(function(){
		$('#InputType').select2({
			allowClear: true,
			placeholder: '类型'
		});
	});
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');

	$('[data-role="delete"]').on('click', function(){
		var data = $(this).parents('tr').data();
		iconfirm({
			action	: '{{ route('RootAuthenticationDelete') }}',
			body		: '确定要删除编号 #' + data.id + ' 的身份验证信息吗？\n删除后用户将失去验证状态。',
			data		: data
		});
		return false;
	});
</script>
@endpush