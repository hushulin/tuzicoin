@extends('root.layouts.master')

@section('title', '法币汇率')
@section('small-title', '美元兑各个货币的汇率信息，数据来源于雅虎。')

@section('breadcrumb')
<li>系统管理</li>
<li><a href="{{ route('RootExchangeRateList') }}">法币汇率</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                汇率列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">货币代码</label>
						<input class="form-control" type="text" name="name" value="{{ Request::get('name') }}" placeholder="货币代码">
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>货币代码</th>
							<th>价格</th>
							<th>数据时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->name }}</td>
							<td>{{ $item->price }}</td>
							<td>{{ $item->updated_at }}</td>
							<td></td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection
