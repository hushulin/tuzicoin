@extends('root.layouts.master')

@section('title', '交易记录')
@section('small-title', '所有用户的交易记录信息。')

@section('breadcrumb')
<li>财务流水</li>
<li><a href="{{ route('RootTransactionList') }}">交易记录</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                交易记录列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">会员ID</label>
						<input class="form-control" type="text" name="user_id" value="{{ Request::get('user_id') }}" placeholder="会员ID">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>编号</th>
							<th>用户名</th>
							<th>类型</th>
							<th>金额</th>
							<th>其它</th>
							<th>时间</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td><a href="{{ route('RootUserDetail', [ 'id' => $item->user->id ]) }}">{{ $item->user->username }}</a></td>
							<td>{{ trans('transaction.type.' . $item->type) }}</td>
							<td>{{ btc_format($item->amount) }} {{ $item->coin }}</td>
							<td>
								@if ($item->fee > 0)
									手续费：{{ btc_format($item->fee) }} {{ $item->coin }}<br>
								@endif
								@if ( ! empty($item->txid))
									TXID：<a href="{{ sprintf(C('blockchain_view'), $item->txid) }}" target="_blank">{{ $item->txid }} <i class="fa fa-external-link"></i></a><br>
								@endif
								@if ( ! empty($item->address))
									转入或转出地址：{{ $item->address }}<br>
								@endif
								@if ($item->target_id)
									来源或目标用户：<a href="{{ route('RootUserDetail', [ 'id' => $item->target_id ]) }}">{{ $item->target->username }}</a><br>
								@endif
								@if ($item->price > 0)
									价格：{{ $item->price }} {{ $item->currency_code }}<br>
								@endif
								@if ($item->remark)
									会员的备注：{{ $item->remark }}<br>
								@endif
								@if ($item->related_transaction_id)
									相关交易：<a href="{{ route('RootTransactionList', [ 'id' => $item->related_transaction_id ]) }}">{{ trans('transaction.type.' . $item->relatedTransaction->type) }}</a><br>
								@endif
							</td>
							<td>{{ $item->created_at }}</td>
							<td></td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');
</script>
@endpush