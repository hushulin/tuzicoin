@extends('root.layouts.master')

@section('title', '订单管理')
@section('small-title', '管理已配对的交易订单。')

@section('breadcrumb')
<li>交易管理</li>
<li><a href="{{ route('RootOrderList') }}">订单管理</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2-bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                订单列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">订单编号</label>
						<input class="form-control" type="text" name="id" value="{{ Request::get('id') }}" placeholder="订单编号">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputStatus">状态</label>
						<select id="InputStatus" class="form-control select2-multiple" name="status[]" multiple="multiple" size="1" style="height: 33px; min-width: 180px;">
							<option value=""></option>
						@foreach (trans('order.status') as $status => $text)
							<option value="{{ $status }}" @if (in_array($status, (array)Request::get('status'))) selected @endif >{{ trans('order.status.' . $status) }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>编号</th>
							<th>买家</th>
							<th>卖家</th>
							<th>成交价</th>
							<th>数量</th>
							<th>手续费</th>
							<th>状态</th>
							<th>创建时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr data-id="{{ $item->id }}">
							<td>{{ $item->id }}</td>
							<td><a href="{{ route('RootUserDetail', [ 'id' => $item->buyer->id ]) }}">{{ $item->buyer->username }}</a></td>
							<td><a href="{{ route('RootUserDetail', [ 'id' => $item->seller->id ]) }}">{{ $item->seller->username }}</a></td>
							<td>{{ btc_format($item->price) }} {{ $item->counter->currency_code }}</td>
							<td>{{ btc_format($item->quantity) }} {{ $item->counter->coin }}</td>
							<td>{{ btc_format($item->fee) }} {{ $item->counter->coin }}</td>
							<td>
								@if ($item->status === App\Models\Order::STATUS_WAIT_PAYMENT)
									<span class="label label-warning">{{ trans('order.status.' . $item->status) }}</span>
								@elseif ($item->status === App\Models\Order::STATUS_UNCONFIRMED)
									<span class="label label-primary">{{ trans('order.status.' . $item->status) }}</span>
								@elseif ($item->status === App\Models\Order::STATUS_CONFIRMED)
									<span class="label label-success">{{ trans('order.status.' . $item->status) }}</span>
								@else
									<span class="label label-default">{{ trans('order.status.' . $item->status) }}</span>
								@endif
							</td>
							<td>{{ $item->created_at }}</td>
							<td>
								<a class="btn btn-xs btn-default" href="{{ route('RootOrderEdit', [ 'id' => $item->id ]) }}"><i class="fa fa-info"></i> 编辑订单</a>
								<a class="btn btn-xs btn-default" href="{{ route('RootCounterList', [ 'id' => $item->counter_id ]) }}"><i class="fa fa-info"></i> 查看柜台</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/select2.js') }}"></script>
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$(function(){
		$('#InputStatus').select2({
			allowClear: true,
			placeholder: '状态'
		});
	});
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');
</script>
@endpush