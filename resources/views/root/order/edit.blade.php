@extends('root.layouts.master')

@section('title', '编辑订单')

@section('breadcrumb')
<li>交易管理</li>
<li><a href="{{ route('RootOrderList') }}">订单</a></li>
<li><a href="{{ route('RootOrderEdit', [ 'id' => $data ? $data->id : '' ]) }}">编辑订单</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">订单信息</header>
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="{{ route('RootOrderEditAction') }}">
				{!! csrf_field() !!}
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">#</label>
						<div class="col-lg-10">
							<p class="form-control-static">{{ $data->id }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">状态</label>
						<div class="col-sm-10">
							<select class="form-control" name="status">
								<option value=""></option>
								@foreach (trans('order.status') as $status => $text)
									<option value="{{ $status }}" @if (in_array($status, [$data->status])) selected @endif >{{ trans('order.status.' . $status) }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<input type="hidden" name="id" value="{{ $data ? $data->id : '' }}">
							<button class="btn btn-primary" type="submit" @if ($data && ! sub_admin(auth()->user(), $data)) disabled="disabled" @endif >保存</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection
