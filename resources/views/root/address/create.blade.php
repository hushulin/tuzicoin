@extends('root.layouts.master')

@section('title', '导入地址')
@section('small-title', '导入钱包收款地址到系统供用户使用。')

@section('breadcrumb')
<li>财务流水</li>
<li><a href="{{ route('RootAddressList') }}">钱包地址</a></li>
<li><a href="{{ route('RootAddressCreate') }}">导入</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading"> 地址列表 </header>
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="{{ route('RootAddressSave') }}">
					{!! csrf_field() !!}
					@include('root.components.form.select', [
						'label' => '币种',
						'name' => 'coin',
						'options' => trans('address.coin'),
						'search' => false
					])
					@include('root.components.form.textarea', [
						'label' => '钱包地址',
						'placeholder' => '钱包收款地址列表，每行为一个，系统会进行去重。',
						'name' => 'address_list',
						'help' => '去重后地址数量：-',
						'rows' => 20
					])
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button class="btn btn-primary" type="submit">提交</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection

@push('script')
<script>
	var $address_helpblock = $('#help-block-address_list');
	var address_text = $address_helpblock.text();
	$('#input-textarea-address_list').on('change keyup', function(){
		var arr = $(this).val().split(/\r|\n/);
		var unique = {}, count = 0;
		for (var i in arr) {
			if(arr[i]){
				if(typeof unique[arr[i]] == 'undefined'){
					unique[arr[i]] = '';
					count ++;
				}
			}
		}
		$address_helpblock.text(address_text.replace(/-$/, count));
	}).trigger('change');
</script>
@endpush
