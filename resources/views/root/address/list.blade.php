@extends('root.layouts.master')

@section('title', '钱包地址')
@section('small-title', '管理系统中的存款钱包地址。')

@section('breadcrumb')
<li>财务流水</li>
<li><a href="{{ route('RootAddressList') }}">钱包地址</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/css/select2-bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                地址列表
            </header>
            <div class="tbl-head clearfix">
				<div class="btn-group">
					<a class="btn btn-primary btn-sm" href="{{ route('RootAddressCreate') }}"><i class="fa fa-plus"></i> 导入</a>
				</div>
			</div>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only" for="InputCoin">币种</label>
						<select id="InputCoin" class="form-control select2-multiple" name="coin[]" multiple="multiple" size="1" style="height: 33px; min-width: 180px;">
							<option value=""></option>
						@foreach (trans('address.coin') as $coin => $text)
							<option value="{{ $coin }}" @if (in_array($coin, (array)Request::get('coin'))) selected @endif >{{ trans('address.coin.' . $coin) }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label class="sr-only">地址</label>
						<input class="form-control" type="text" name="address" value="{{ Request::get('address') }}" placeholder="地址">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>币种</th>
							<th>地址</th>
							<th>分配用户</th>
							<th>创建时间</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->coin }}</td>
							<td>{{ $item->address }}</td>
							<td>
								@if ( ! $item->user_id) - @else
									<a href="{{ route('RootUserDetail', [ 'id' => $item->user->id ]) }}">{{ $item->user->username }}</a>
								@endif
							</td>
							<td>{{ $item->created_at }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/select2.js') }}"></script>
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$(function(){
		$('#InputCoin').select2({
			allowClear: true,
			placeholder: '币种'
		});
	});
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');
</script>
@endpush