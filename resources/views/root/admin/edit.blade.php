@extends('root.layouts.master')

@section('title', '编辑管理员')
@section('small-title', '创建或编辑管理员。')

@section('breadcrumb')
<li>管理人员</li>
<li><a href="{{ route('RootAdminList') }}">管理员账号</a></li>
<li><a href="{{ route('RootAdminEdit', [ 'id' => $data ? $data->id : '' ]) }}">编辑管理员</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">管理员信息</header>
			<div class="panel-body">
				<form method="post" class="form-horizontal" action="{{ route('RootAdminEditAction') }}">
				{!! csrf_field() !!}
				@if ($data)
					<div class="form-group">
						<label class="col-lg-2 col-sm-2 control-label">#</label>
						<div class="col-lg-10">
							<p class="form-control-static">{{ $data->id }}</p>
						</div>
					</div>
				@endif
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">用户名</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" name="username" value="{{ Request::old('username', $data ? $data->username : '') }}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">密码</label>
						<div class="col-sm-10">
							<input class="form-control" type="password" name="password">
							@if ($data)<p class="help-block">不修改密码请留空。</p>@endif
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">确认密码</label>
						<div class="col-sm-10">
							<input class="form-control" type="password" name="password_confirmation">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">角色</label>
						<div class="col-sm-10">
						@if ($roles->isEmpty())
							<p class="form-control-static">请先在<a href="{{ route('RootRoleList') }}">角色管理</a>中创建角色</p>
						@else
							@if ($data && $data->id == 1)
								<p class="form-control-static">Root</p>
							@else
			                    <p class="help-block">每个用户至少拥有一个角色。</p>
								@foreach ($roles as $item)
									<label class="checkbox-custom inline check-info">
				                        <input type="checkbox" name="roles[]" value="{{ $item->id }}" id="role-{{ $item->id }}"
											@if ($data && $data->roles && in_array($item->id, (array)Request::old('roles', $data->roles->pluck('id')->all())))checked @endif
											@if ( ! has_role(auth()->user(), $item)) disabled @endif
										>
										<label for="role-{{ $item->id }}">{{ $item->name }}</label>
				                    </label>
			                	@endforeach
		                	@endif
		                @endif
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 col-sm-2 control-label">权限</label>
						<div class="col-sm-10">
						@if ($permissions->isEmpty())
							<p class="form-control-static">请先在<a href="{{ route('RootPermissionList') }}">权限管理</a>中创建权限</p>
						@else
							@if ($data && $data->id == 1)
								<p class="form-control-static">所有权限</p>
							@else
								<p class="help-block">除用户角色拥有的权限外，单独给该用户的权限。</p>
								<div class="clearfix">
								<?php $_temp = null; ?>
								@foreach ($permissions as $item)
									@if ($item->group !== $_temp)
										@if ($_temp !== null)
										</dl>
										@endif
										<dl class="col-lg-3">
											<dt>{{ $item->group or '' }}</dt>
										<?php $_temp = $item->group; ?>
									@endif
									<dd>
										<label for="purview-{{ $item->key }}" class="checkbox-custom inline check-info">
					                        <input type="checkbox" name="permissions[]" value="{{ $item->key }}" id="purview-{{ $item->key }}"
					                          @if ($data && in_array($item->key, (array)Request::old('permissions', $data->permissions->pluck('key')->all()))) checked @endif
					                          @if ( ! has_permission(auth()->user(), $item)) disabled @endif
					                        >
					                        <label for="purview-{{ $item->key }}">{{ $item->name }}</label>
					                    </label>
				                    </dd>
				                @endforeach
			                    </div>
		                    @endif
		                @endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<input type="hidden" name="id" value="{{ $data ? $data->id : '' }}">
							<button class="btn btn-primary" type="submit" @if ($data && ! sub_admin(auth()->user(), $data)) disabled="disabled" @endif >保存</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
@endsection
