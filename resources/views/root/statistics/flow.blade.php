@extends('root.layouts.master')

@section('title', '数据统计')
@section('small-title', '每日各流水盈亏统计图。')

@section('breadcrumb')
<li>数据统计</li>
<li><a href="{{ route('RootStatisticsFlow') }}">流水统计</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row state-overview">
	<div class="col-lg-4 col-sm-6">
		<section class="panel purple">
			<div class="symbol">
				<i class="fa fa-bitcoin"></i>
			</div>
			<div class="value white">
				<h1>{{ $total_fee_btc }}</h1>
				<p>总收取比特币手续费</p>
			</div>
		</section>
	</div>
	<div class="col-lg-4 col-sm-6">
		<section class="panel green">
			<div class="symbol">
				<i class="fa fa-bitcoin"></i>
			</div>
			<div class="value white">
				<h1>{{ $total_deposit_btc }}</h1>
				<p>总存款比特币</p>
			</div>
		</section>
	</div>
	<div class="col-lg-4 col-sm-6">
		<section class="panel ">
			<div class="symbol red-color">
				<i class="fa fa-bitcoin"></i>
			</div>
			<div class="value gray">
				<h1 class="red-color">{{ $sold_withdraw_btc }}</h1>
				<p>总提现比特币</p>
			</div>
		</section>
	</div>
	<div class="col-lg-4 col-sm-6">
		<section class="panel purple">
			<div class="symbol">
				<i class="fa fa-gbp"></i>
			</div>
			<div class="value white">
				<h1>{{ $total_fee_ltc }}</h1>
				<p>总收取莱特币手续费</p>
			</div>
		</section>
	</div>
	<div class="col-lg-4 col-sm-6">
		<section class="panel green">
			<div class="symbol">
				<i class="fa fa-gbp"></i>
			</div>
			<div class="value white">
				<h1>{{ $total_deposit_ltc }}</h1>
				<p>总存款莱特币</p>
			</div>
		</section>
	</div>
	<div class="col-lg-4 col-sm-6">
		<section class="panel ">
			<div class="symbol red-color">
				<i class="fa fa-gbp"></i>
			</div>
			<div class="value gray">
				<h1 class="red-color">{{ $sold_withdraw_ltc }}</h1>
				<p>总提现莱特币</p>
			</div>
		</section>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				统计条件
				<span class="tools pull-right">
					<a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
				</span>
			</header>
			<div class="panel-body">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date_range" value="{{ Request::get('date_range', $date_range) }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
		</section>
	</div>
</div>

@if ($data->isEmpty())
	<p class="text-center">统计条件下没有任何数据</p>
@else
	<div class="row">
		@foreach (C('coins') as $coin)
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					流水（{{ $coin }}）
					<span class="tools pull-right">
						<a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
					</span>
				</header>
				<div class="panel-body">
					<div id="StatisticsFlow{{ $coin }}"></div>
				</div>
			</section>
		</div>
		@endforeach
	</div>
@endif

@endsection

@push('script')
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('slicklab/js/hightcharts/js/highcharts.src.js') }}"></script>
<script>
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'right',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	});

	@foreach (C('coins') as $coin)
	$('#StatisticsFlow{{ $coin }}').iCharts({
		height: 500,
		categories: [
			@foreach ($data->pluck('date')->unique() as $date)
			'{{ $date }}',
			@endforeach
		],
		series: [
			@foreach (array_keys(trans('transaction.type')) as $type)
			{
				name: '{{ trans('transaction.type.' . $type) }}',
				data: [
				@foreach ($data->where('coin', $coin)->where('type', $type)->groupBy('date') as $item)
					@php
						$items = $item->where('coin', $coin)->where('type', $type);
					@endphp
					{{ $items->sum('total_amount') }},
				@endforeach
				]
			},
			@endforeach
		]
	});
	@endforeach
</script>
@endpush