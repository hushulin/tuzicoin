@extends('root.layouts.master')

@section('title', '用户统计')
@section('small-title', '每日注册用户数统计折线图。')

@section('breadcrumb')
<li>数据统计</li>
<li><a href="{{ route('RootStatisticsUser') }}">用户统计</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row state-overview">
	<div class="col-lg-6">
		<section class="panel purple">
			<div class="symbol">
				<i class="fa fa-users"></i>
			</div>
			<div class="value white">
				<h1>{{ number_format($user_count) }}</h1>
				<p>总会员</p>
			</div>
		</section>
	</div>
	<div class="col-lg-6">
		<section class="panel green">
			<div class="symbol">
				<i class="fa fa-flash"></i>
			</div>
			<div class="value white">
				<h1>{{ number_format($online_count) }}</h1>
				<p>当前在线</p>
			</div>
		</section>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				统计条件
				<span class="tools pull-right">
					<a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
				</span>
			</header>
			<div class="panel-body">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date_range" value="{{ Request::get('date_range', $date_range) }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
		</section>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				统计图
				<span class="tools pull-right">
					<a class="t-collapse fa fa-chevron-down" href="javascript:;"></a>
				</span>
			</header>
			<div class="panel-body">
				<div id="Statistics"></div>
			</div>
		</section>
	</div>
</div>

@endsection

@push('script')
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('slicklab/js/hightcharts/js/highcharts.src.js') }}"></script>
<script>
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'right',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	});

	$('#Statistics').iCharts({
		height: 500,
		floor: 0,
		categories: [
			@foreach ($new_users->pluck('date')->unique() as $date)
			'{{ $date }}',
			@endforeach
		],
		series: [
			{
				name: '新增',
				data: [
				@foreach ($new_users->groupBy('date') as $item)
					{{ $item->sum('count') }},
				@endforeach
				]
			},
			{
				name: '活跃',
				data: [
				@foreach ($active_users->groupBy('date') as $item)
					{{ $item->sum('count') }},
				@endforeach
				]
			}
		]
	});
</script>
@endpush