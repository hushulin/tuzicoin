@extends('root.layouts.master')

@section('title', '用户管理')
@section('small-title', '管理所有注册的用户账号。')

@section('breadcrumb')
<li>用户管理</li>
<li><a href="{{ route('RootUserList') }}">用户列表</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                用户列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">编号</label>
						<input class="form-control" type="text" name="id" value="{{ Request::get('id') }}" placeholder="编号">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>编号</th>
							<th>用户名</th>
							<th>国家</th>
							<th>手机</th>
							<th>可用余额</th>
							<th>冻结资产</th>
							<th>总资产</th>
							<th>状态</th>
							<th>注册时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr data-id="{{ $item->id }}" data-username="{{ $item->username }}">
							<td>{{ $item->id }}</td>
							<td>{{ $item->username }}</td>
							<td>{{ $item->country_code }}</td>
							<td>{{ $item->mobile }}</td>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($item->wallet($coin)->balance_available) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($item->wallet($coin)->balance_locked) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($item->wallet($coin)->balance_total) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
							<td>
								@if ($item->online)
									<span class="label label-success">在线</span>
								@else
									<span class="label label-default">离线</span>
								@endif
							</td>
							<td>{{ $item->created_at }}</td>
							<td>
								<a class="btn btn-xs btn-info" href="{{ route('RootUserDetail', [ 'id' => $item->id ]) }}"><i class="fa fa-info"></i> 详情</a>
								<button class="btn btn-xs btn-warning" type="button" data-role="login"><i class="fa fa-sign-in"></i> 登录</button>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');

	$('[data-role="login"]').on('click', function(){
		var data = $(this).parents('tr').data();
		iconfirm({
			action	: '{{ route('RootUserLogin') }}',
			body	: '确定要使用账号 ' + data.username + ' 登录系统吗？',
			data		: data,
			target	: '_blank'
		});
		return false;
	});
</script>
@endpush
