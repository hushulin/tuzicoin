@extends('root.layouts.master')

@section('title', '登陆日志')
@section('small-title', '用户的登陆日志记录。')

@section('breadcrumb')
<li>用户管理</li>
<li><a href="{{ route('RootUserLoginLogList') }}">登陆日志</a></li>
@endsection

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.css') }}"/>
<style>
	#InputDate { min-width: 210px; }
</style>
@endpush

@section('main')
<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                用户列表
            </header>
            <div class="tbl-top">
				<form class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only">用户ID</label>
						<input class="form-control" type="text" name="user_id" value="{{ Request::get('user_id') }}" placeholder="用户ID">
					</div>
					<div class="form-group">
						<label class="sr-only">用户名</label>
						<input class="form-control" type="text" name="username" value="{{ Request::get('username') }}" placeholder="用户名">
					</div>
					<div class="form-group">
						<label class="sr-only">手机</label>
						<input class="form-control" type="tel" name="mobile" value="{{ Request::get('mobile') }}" placeholder="手机">
					</div>
					<div class="form-group">
						<label class="sr-only" for="InputDate">日期</label>
						<div class="input-group">
							<input id="InputDate" class="form-control" type="text" name="date" value="{{ Request::get('date') }}" placeholder="日期" data-mask="9999-99-99 ~ 9999-99-99" autocomplete="off">
						</div>
					</div>

					<button type="submit" class="btn btn-success">搜索</button>
				</form>
			</div>
			<div class="panel-body">
			@if ($data->isEmpty())
				<p class="text-center">没有任何项目</p>
			@else
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>用户名</th>
							<th>手机</th>
							<th>IP</th>
							<th>浏览器</th>
							<th>操作系统</th>
							<th>登陆时间</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $item)
						<tr data-id="{{ $item->id }}">
							<td>{{ $item->id }}</td>
							<td>{{ $item->user->username }}</td>
							<td>{{ $item->user->mobile }}</td>
							<td>
							@foreach($item->ips as $ip)
								@include('root.components.tooltip', [ 'text' => $ip, 'title' => ip_query($ip) ])
							@endforeach
							</td>
							<td>{{ $item->browser }}</td>
							<td>{{ $item->platform }}</td>
							<td>{{ $item->created_at }}</td>
							<td>
								<a class="btn btn-xs btn-default" href="{{ route('RootUserLoginLogList', [ 'user_id' => $item->user_id ]) }}"><i class="fa fa-info"></i> 只看此人</a>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			@endif
			</div>
			@include('root.layouts.paginate')
		</section>
	</div>
</div>
@endsection

@push('script')
<script src="{{ asset('slicklab/js/bs-input-mask.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('slicklab/js/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script>
	$('#InputDate').daterangepicker({
		ranges: {
			'最近7天': [moment().subtract(7, 'day'), moment()],
			'最近30天': [moment().subtract(30, 'day'), moment()],
			'本周': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
			'上周': [moment().subtract(1, 'week').startOf('isoWeek'), moment().subtract(1, 'week').endOf('isoWeek')],
			'本月': [moment().startOf('month'), moment().endOf('month')],
			'上月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		opens: 'left',
		maxDate: moment(),
		locale: {
			format: 'YYYY-MM-DD',
			separator: ' ~ ',
			applyLabel: '应用',
			cancelLabel: '清空',
			weekLabel: '周',
			customRangeLabel: '自定义时间段',
			daysOfWeek: ['日', '一', '二', '三', '四', '五','六'],
			monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			firstDay: 1
		},
		buttonClasses: ['btn']
	}).on('cancel.daterangepicker', function() {
		$(this).val('');
	}).val('{{ Request::get('date') }}');
</script>
@endpush
