@extends('root.layouts.master')

@section('title', '用户详情')
@section('small-title', '注册用户的详细信息。')

@section('breadcrumb')
<li>用户管理</li>
<li><a href="{{ route('RootUserList') }}">用户列表</a></li>
<li><a href="{{ route('RootUserDetail', [ 'id' => $data->id  ]) }}">用户详情</a></li>
@endsection

@section('main')
<div class="row">
	<div class="col-lg-6">
		<section class="panel">
			<header class="panel-heading"> 基本信息 </header>
			<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th width="120">编号</th>
							<td>{{ $data->id }}</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>用户名</th>
							<td>{{ $data->username }}</td>
						</tr>
						<tr>
							<th>状态</th>
							<td>
							@if ($data->online)
								<span class="label label-success">在线</span>
							@else
								<span class="label label-default">离线</span>
							@endif
							</td>
						</tr>
						<tr>
							<th>国家</th>
							<td>{{ $data->country_code }}</td>
						</tr>
						<tr>
							<th>手机</th>
							<td>{{ $data->mobile }} <span class="label label-success">已验证</span></td>
						</tr>
						<tr>
							<th>电子邮箱</th>
							<td>
								{{ $data->email or '-' }}
								@if ($data->email_verified)
									<span class="label label-success">已验证</span>
								@endif
							</td>
						</tr>
						<tr>
							<th>可用余额</th>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($data->wallet($coin)->balance_available) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
						</tr>
						<tr>
							<th>冻结资产</th>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($data->wallet($coin)->balance_locked) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
						</tr>
						<tr>
							<th>总资产</th>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($data->wallet($coin)->balance_total) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
						</tr>
						<tr>
							<th>比特币地址</th>
							<td>{{ $data->bitcoin_address }}</td>
						</tr>
						<tr>
							<th>身份验证</th>
							<td>
								@if ( ! $data->authentication_id)
									<span class="label label-default">未验证</span>
								@else
									<a class="btn btn-success btn-xs" href="{{ route('RootAuthenticationList', [ 'id' => $data->authentication_id ]) }}"><i class="fa fa-link"></i> 已验证</a>
								@endif
							</td>
						</tr>
						<tr>
							<th>谷歌二次验证</th>
							<td>
								@if ($data->google2fa_secret)
									<span class="label label-success">已启用</span>
								@else
									<span class="label label-default">未启用</span>
								@endif
							</td>
						</tr>
						<tr>
							<th>简介</th>
							<td>{{ $data->introduction }}</td>
						</tr>
						<tr>
							<th>注册IP</th>
							<td>
							@foreach ($data->register_ips as $ip)
								@include('root.components.tooltip', [ 'text' => $ip, 'title' => ip_query($ip) ])
							@endforeach
							</td>
						</tr>
						<tr>
							<th>注册时间</th>
							<td>{{ $data->created_at }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
	</div>
	<div class="col-lg-6">
		<section class="panel">
			<header class="panel-heading"> 信用信息 </header>
			<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th width="150">信任人数</th>
							<td>{{ $data->trust_count }}</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>累计交易次数</th>
							<td>{{ $data->trade_count }}</td>
						</tr>
						<tr>
							<th>累计购买交易次数</th>
							<td>{{ $data->buy_trade_count }}</td>
						</tr>
						<tr>
							<th>累计出售交易次数</th>
							<td>{{ $data->sell_trade_count }}</td>
						</tr>
						<tr>
							<th>累计交易量</th>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($data->getAttribute('volume_' . strtolower($coin))) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
						</tr>
						<tr>
							<th>累计购买交易量</th>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($data->getAttribute('buy_volume_' . strtolower($coin))) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
						</tr>
						<tr>
							<th>累计出售交易量</th>
							<td>
								@foreach (C('coins') as $coin)
									{{ btc_format($data->getAttribute('sell_volume_' . strtolower($coin))) }} {{ $coin }}
									@if ( ! $loop->last) / @endif
								@endforeach
							</td>
						</tr>
						<tr>
							<th>好评率</th>
							<td>{{ $data->rating }}%</td>
						</tr>
						<tr>
							<th>好评数</th>
							<td>{{ $data->positive_feedback_count }}</td>
						</tr>
						<tr>
							<th>中评数</th>
							<td>{{ $data->neutral_feedback_count }}</td>
						</tr>
						<tr>
							<th>差评数</th>
							<td>{{ $data->negative_feedback_count }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
