<tr>
	<td>
		@if ($data->buyer_id == Auth::id())
			<user :user_id="{{ $data->seller_id }}"></user>
		@else
			<user :user_id="{{ $data->buyer_id }}"></user>
		@endif
	</td>
	<td><a href="{{ route('order.detail', [ 'id' => $data->id ]) }}">{{ $data->id }}</a></td>
	<td>
		@if ($data->buyer_id == Auth::id())
			{{ __('Buy') }}
		@else
			{{ __('Sell') }}
		@endif
	</td>
	<td>{{ btc_format($data->amount) }} {{ $data->counter->currency_code }}</td>
	<td>{{ btc_format($data->quantity) }} {{ $data->counter->coin }}</td>
	<td>{{ $data->created_at }}</td>
	<td>
		@if ($data->status === App\Models\Order::STATUS_WAIT_PAYMENT)
			@if ($data->buyer_id == Auth::id())
				<span class="text-warning">{{ __('Created') }}</span>
			@else
				<span class="text-warning">{{ __('Trade Created') }}</span>
			@endif
		@elseif ($data->status === App\Models\Order::STATUS_UNCONFIRMED)
			<span class="text-primary">{{ __('Paid') }}</span>
		@elseif ($data->status === App\Models\Order::STATUS_REVOKED)
			<span class="text-default">{{ __('Closed') }}</span>
		@elseif ($data->status === App\Models\Order::STATUS_EXPIRED)
			<span class="text-default">{{ __('Expired') }}</span>
		@elseif ($data->status === App\Models\Order::STATUS_CONFIRMED)
			@if ($data->buyer_feedback && $data->seller_feedback)
				<span class="text-success">{{ __('Finished') }}</span>
			@else
				@if ($data->buyer_id == Auth::id())
					@if ($data->buyer_feedback)
							<span class="text-success">{{ __('Has been reviewed') }}</span>
					@else
							<span class="text-info">{{ __('Has been received') }}</span>
					@endif
				@else
					@if ($data->seller_feedback)
							<span class="text-success">{{ __('Has been reviewed') }}</span>
					@else
							<span class="text-info">{{ __('Has been released') }}</span>
					@endif
				@endif
			@endif
		@endif
	</td>
	<td>
		<a class="btn btn-info btn-xs" href="{{ route('order.detail', [ 'id' => $data->id ]) }}">
			<span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
			{{ __('Trade operations') }}
			@if ($data->unread_message_count)
				<span class="badge">{{ $data->unread_message_count }}</span>
			@endif
		</a>
	</td>
</tr>