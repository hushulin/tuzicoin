@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<order-status :order="{{ $order }}"></order-status>
		</div>
		<div class="col-md-12">
			@include('components.message')
		</div>
		<div class="col-md-12">
			<div class="alert alert-info order-info" role="alert">
				<h4>{{ __('Trade information') }}</h4>
				<dl>
					<dt>{{ __('Price') }}:</dt>
					<dd>{{ $order->price }} {{ $order->counter->currency_code }}</dd>
					<dt>{{ __('Quantity') }}:</dt>
					<dd>{{ $order->quantity }} {{ $order->counter->coin }}</dd>
					<dt>{{ __('Trading Amount') }}:</dt>
					<dd>{{ $order->amount }} {{ $order->counter->currency_code }}</dd>
					@if ($order->counter->user_id == Auth::id())
						<dt>{{ __('Fee') }}:</dt>
						<dd>{{ $order->fee }} {{ $order->counter->coin }}</dd>
					@endif
				</dl>
			</div>
		</div>
		<div class="col-md-8">
			<chat :order="{{ $order }}" :messages="{{ $messages->map(function($message){ $message->addHidden('source', 'target'); return $message; }) }}"></chat>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">{{ __('Trade operations') }}</div>
				<div class="panel-body">
					<dl>
						<dt>{{ __('Trade ID') }}</dt>
						<dd>{{ $order->id }}</dd>
					</dl>
					<dl>
						<dt>{{ __('Payment method') }}</dt>
						<dd>{{ trans('counter.payment_provider.' . $order->counter->payment_provider) }}</dd>
					</dl>
					<dl>
						<dt>{{ __('Leave message') }}</dt>
						<dd><span class="message">{{ $order->counter->message }}</span></dd>
					</dl>

					<order-tips :order="{{ $order }}"></order-tips>

					<order-operations :order="{{ $order }}"></order-operations>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('data')
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	@foreach ($messages->pluck('source', 'source_id')->merge($messages->pluck('target', 'target_id'))->put($order->buyer_id, $order->buyer)->put($order->seller_id, $order->seller)->filter()->unique() as $user)
		window.users[{{ $user->id }}] = {!! $user !!};
	@endforeach
</script>
@endpush