<div class="panel-body">
	@if ($orders->isEmpty())
		<p class="text-muted">{{ __('No data') }}</p>
	@else
		<table class="table">
			<thead>
				<tr>
					<th>{{ __('Partner') }}</th>
					<th>{{ __('ID') }}</th>
					<th>{{ __('Type') }}</th>
					<th>{{ __('Amount') }}</th>
					<th>{{ __('Quantity') }}</th>
					<th>{{ __('Created at') }}</th>
					<th>{{ __('Status') }}</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@each('order.item', $orders, 'data')
			</tbody>
		</table>
		{{ $orders->appends(request()->all())->links() }}
	@endif
</div>

@push('data')
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	@foreach ($orders->pluck('buyer', 'buyer_id')->merge($orders->pluck('seller', 'seller_id'))->unique() as $user)
		window.users[{{ $user->id }}] = {!! $user !!};
	@endforeach
</script>
@endpush