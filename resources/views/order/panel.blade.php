@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
				<div class="panel-heading">{{ __('Order') }}</div>

				<div class="panel-body">
					@include('components.message')

					<ul class="nav nav-tabs">
						@foreach ([
							'Processing',
							'Completed'
						] as $type)
							<li @if (request('type') == $type) class="active" @endif >
								<a href="{{ route('order.list', array_merge(request()->except('page'), [ 'type' => $type ])) }}">
									{{ __($type) }}
									@php
										$count = ${strtolower($type) . '_message_count'};
									@endphp
									@if ($count)
										<span class="badge">{{ $count }}</span>
									@endif
								</a>
							</li>
						@endforeach
					</ul>
				</div>
				@include('order.orders')
			</div>
        </div>
    </div>
</div>
@endsection
