<div class="profile">
	<user :user_id="{{ $user->id }}" :link-enable="false"></user>
	@include('profile.credit')
	@if ($user->introduction)
		<p class="introduction">{{ __('Intro') }}: <span class="message">{{ $user->introduction }}</span></p>
	@endif
</div>

@push('data')
<script>
	{{-- 载入用户信息列表 --}}
	window.users = window.users || {};
	window.users[{{ $user->id }}] = {!! $user !!};
</script>
@endpush