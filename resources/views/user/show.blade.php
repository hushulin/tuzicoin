@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-body">
    				<div class="row">
    					<div class="col-md-6">
    						@include('user.profile')
    					</div>
    					<div class="col-md-6">
    						@auth
    						<div class="trust-ignore">
    							@if (Auth::id() == $user->id)
    								{{-- 不能信任和屏蔽自己 --}}
    								<div class="form-btn">
	    								<button type="button" class="btn btn-primary btn-sm" disabled="disabled">{{ __('Trust') }}</button>
	    								<button type="button" class="btn btn-info btn-sm" disabled="disabled">{{ __('Block') }}</button>
    								</div>
    							@else
		    						<form method="post" class="form-btn">
										{{ csrf_field() }}
										<input type="hidden" name="user_id" value="{{ $user->id }}">
										@if (Auth::user()->trusts()->where('target_id', $user->id)->value('id'))
											<button type="submit" class="btn btn-default btn-sm" formaction="{{ route('trust.remove') }}">{{ __('Have trusted') }}</button>
										@else
											<button type="submit" class="btn btn-primary btn-sm" formaction="{{ route('trust.add') }}">{{ __('Trust') }}</button>
										@endif
										@if (Auth::user()->ignores()->where('target_id', $user->id)->value('id'))
											<button type="submit" class="btn btn-default btn-sm" formaction="{{ route('ignore.remove') }}">{{ __('Have blocked') }}</button>
										@else
											<button type="submit" class="btn btn-info btn-sm" formaction="{{ route('ignore.add') }}">{{ __('Block') }}</button>
										@endif
									</form>
								@endif
							</div>
							@endauth

							<div class="user-check">
								<ul>
									@if ($user->email_verified)
										<li>
											<span class="label label-danger">
												<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
											</span>
											{{ __('Verified Email') }}
										</li>
									@else
										<li>
											<span class="label label-default">
												<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
											</span>
											{{ __('Unverified Email') }}
										</li>
									@endif

									@if ($user->mobile_verified)
										<li>
											<span class="label label-primary">
												<i class="glyphicon glyphicon-phone" aria-hidden="true"></i>
											</span>
											{{ __('Verified Mobile') }}
										</li>
									@else
										<li>
											<span class="label label-default">
												<i class="glyphicon glyphicon-phone" aria-hidden="true"></i>
											</span>
											{{ __('Unverified Mobile') }}
										</li>
									@endif

									@if ($user->authentication_id)
										<li>
											<span class="label label-success">
												<i class="glyphicon glyphicon-credit-card" aria-hidden="true"></i>
											</span>
											{{ __('Verified ID card') }}
										</li>
									@else
										<li>
											<span class="label label-default">
												<i class="glyphicon glyphicon-credit-card" aria-hidden="true"></i>
											</span>
											{{ __('Unverified ID card') }}
										</li>
									@endif
								</ul>
							</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="col-md-12">
    		<div class="panel panel-default">
				<div class="panel-heading">{{ __('Counters') }}</div>
				<div class="panel-body">
					<ul class="nav nav-tabs">
						@foreach (trans('counter.type') as $type => $text)
							<li @if (request('type') == $type) class="active" @endif >
								<a href="{{ route('user', array_merge(request()->except('page'), [ 'id' => $user->id, 'type' => $type ])) }}">{{ $text }}</a>
							</li>
						@endforeach
					</ul>
				</div>
				<div class="panel-body">
					@if ($counters->isEmpty())
						<p class="text-muted">{{ __('No data') }}</p>
					@else
						<table class="table">
							<thead>
								<tr>
									<th>{{ __('Payment methods') }}</th>
									<th>{{ __('Limits') }}</th>
									<th>{{ __('Price') }}</th>
									<th width="10"></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($counters as $item)
									<tr>
										<td>{{ trans('counter.payment_provider.' . $item->payment_provider) }}</td>
										<td>{{ $item->min_amount }}-{{ $item->max_amount }} {{ $item->currency_code }}</td>
										<td>{{ $item->price }} {{ $item->currency_code }}/{{ $item->coin }}</td>
										<td>
											<a class="btn btn-primary btn-sm" href="{{ route('counter.show', [ 'id' => $item->id ]) }}">{{ $item->type == App\Models\Counter::TYPE_ONLINE_SELL ? __('Buy') : __('Sell') }}</button>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						{{ $counters->links() }}
					@endif
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
