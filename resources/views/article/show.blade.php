@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="text-center">{{ $data->title }}</h3>
				</div>
				<div class="panel-body">{!! $data->content !!}</div>
				<div class="panel-footer">
					<div class="text-right">
						<time>{{ $data->created_at->toDateString() }}</time>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">{{ __($title) }}</div>
				<div class="panel-body">
					@foreach ($articles as $article)
						@if($article->type==1)
							<p><a href="{{ route('article.notice') }}?id={{$article->id}}&type=1">{{ $article->title }}</a></p>
						@elseif($article->type==2)
							<p><a href="{{ route('article.help') }}?id={{$article->id}}&type=2">{{ $article->title }}</a></p>
						@endif
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection