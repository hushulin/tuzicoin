OTC项目部署
1.	项目运行环境apache或者Nginx+php7.0.1以上+mysql5.6*+redis+nodejs 安装好composer 
2.	运行环境搭建好以后安装 npm install 安装项目所需依赖包 在项目根目录运行 php artisan migrate 迁移数据  运行php artisan db:seed 填充初始数据 也可直接导入代码包里的sql文件 注：先创建好数据库
    项目测试配置文件.env  正式配置文件在config/目录下  正式上线应该配置好config下的配置删除.env 开启debug短信默认为手机号后六位  storage目录需要有写入权限 项目访问根目录配置在 public/ 目录下
    前端编译 npm run dev 运行所有 Mix 任务  npm run production 运行所有 Mix 任务并减少输出
3.	laravel广播 安装laravel-echo-sever 安装参考地址 https://github.com/tlaverdure/laravel-echo-server 运行laravel-echo-server init 初始化配置
    初始化后的配置文件参考laravel-echo-server.json
        {
        "authHost": "http://btcotc.dev",
        "authEndpoint": "/broadcasting/auth",
        "clients": [
            {
                "appId": "661d51f8b5a81f2c",
                "key": "29edc2e41bf71bf2e8596ab4f2dde6e7"
            }
        ],
        "database": "redis",
        "databaseConfig": {
            "redis": {},
            "sqlite": {
                "databasePath": "/database/laravel-echo-server.sqlite"
            }
        },
        "devMode": true,
        "host": null,
        "port": "6001",
        "protocol": "http",
        "socketio": {},
        "sslCertPath": "",
        "sslKeyPath": "",
        "sslCertChainPath": "",
        "sslPassphrase": "",
        "apiOriginAllow": {
            "allowCors": true,
            "allowOrigin": "http://btcotc.dev:80",
            "allowMethods": "GET, POST",
            "allowHeaders": "Origin, Content-Type, X-Auth-Token, X-Requested-With, Accept, Authorization, X-CSRF-TOKEN, X-Socket-Id"
        }
    }
    laravel广播参考文档http://laravelacademy.org/post/6851.html
4.	服务守护进程运行管理建议使用Supervisor 管理进程可以把队列和laravel-echo-server 配置为后台守护进程参考文档 http://supervisord.org  https://www.latrell.me/post-462.html
        队列配置文件参考
        ;[program:btcotc-worker]
        process_name=%(program_name)s_%(process_num)02d
        command=/usr/local/bin/php /home/www/btcotc/artisan queue:work --sleep=3 --tries=3
        autostart=true
        autorestart=true
        user=apache
        numprocs=1
        redirect_stderr=true
        stdout_logfile=/home/www/btcotc/storage/logs/worker.lo

        ;laravel-echo-server配置参考
        [program:btcotc-echo-server]
        directory=/home/www/btcotc
        command=laravel-echo-server start
        autostart=true
        autorestart=true
        ;user=apache
        redirect_stderr=true
        stdout_logfile=/home/www/btcotc/storage/logs/laravel-echo-server.log
 
5.	任务调度在服务器定时任务加入  * * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1  频率可根据需求自己定义 /path/to/为项目跟目录 
完成！
