<?php

// 发送短信验证码。
Route::post('verify/mobile', 'VerifyController@mobile')->name('verify.mobile');
// 发送邮件验证码。
Route::post('verify/email', 'VerifyController@email')->name('verify.email');

// 取得美元与指定货币的汇率。
Route::get('exchange-rate', 'ExchangeRateController@price')->name('exchange-rate');
