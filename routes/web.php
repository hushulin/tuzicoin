<?php
/**
 * 前台路由表
 *
 * @author Latrell Chan
 *
 */
/**
 * 前端模板页面
 */
// 广告发布页面
Route::any('counterCreate', 'Front\NortonController@counterCreate');
//下单页面
Route::any('createVirtualOrder', 'Front\NortonController@createVirtualOrder');
// 验证控制器。
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ResetPasswordController@showResetPasswordForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@resetPassword');
Route::get('password/secondary/create',
  'Auth\ResetPasswordController@showCreateSecondaryPasswordForm')->name('password.secondary.create');
Route::get('password/secondary/reset',
  'Auth\ResetPasswordController@showResetSecondaryPasswordForm')->name('password.secondary.reset');
Route::post('password/secondary/reset', 'Auth\ResetPasswordController@resetSecondaryPassword');

// 首页。
Route::get('/', 'HomeController@index')->name('home');
// 购买。
Route::get('buy/{coin?}', 'HomeController@buy')->name('buy');
// 出售。
Route::get('sell/{coin?}', 'HomeController@sell')->name('sell');

// 用户资料页。
Route::get('user/{id}', 'UserController@show')->name('user');
// 用户头像图片。
Route::get('user/avatar/{id}', 'UserController@showAvatar')->name('user.avatar');

// 基本信息。
Route::get('profile', 'ProfileController@index')->name('profile');
Route::post('profile', 'ProfileController@save')->name('profile.save');
// 修改头像。
Route::post('profile/avatar', 'ProfileController@avatar')->name('profile.avatar');
// 身份验证。
Route::get('verification', 'ProfileController@showVerificationForm')->name('profile.verification');
Route::post('verification/identity-card',
  'ProfileController@verificationIdentityCard')->name('profile.verification.identity-card');
Route::post('verification/passport', 'ProfileController@verificationPassport')->name('profile.verification.passport');
Route::post('verification/driver-license',
  'ProfileController@verificationDriverLicense')->name('profile.verification.driver-license');
Route::get('verification/image', 'ProfileController@showVerificationImage')->name('profile.verification.image');

// 安全设置。
Route::get('security', 'SecurityController@index')->name('security');

// 支付信息设置
Route::get('/pay/set', 'SecurityController@showPaySettingForm')->name('pay.info.setting');

// 绑定邮箱。
Route::get('security/email', 'SecurityController@showEmailForm')->name('security.email');
Route::post('security/email', 'SecurityController@bindEmail');
// 修改手机。
Route::get('security/mobile', 'SecurityController@showMobileForm')->name('security.mobile');
Route::post('security/mobile', 'SecurityController@bindMobile');

// 谷歌二次验证。
Route::get('google2fa/bind', 'Google2faController@showBindForm')->name('google2fa.bind');
Route::post('google2fa/bind', 'Google2faController@bind');
Route::get('google2fa/unbind', 'Google2faController@showUnbindForm')->name('google2fa.unbind');
Route::post('google2fa/unbind', 'Google2faController@unbind');

// 登录密码。
Route::get('password/login', 'PasswordController@showLoginPasswordForm')->name('password.login');
Route::post('password/login', 'PasswordController@modifyLoginPassword');
Route::get('password/secondary', 'PasswordController@showSecondaryPasswordForm')->name('password.secondary');
Route::post('password/secondary', 'PasswordController@modifySecondaryPassword');

// 钱包。
Route::get('wallet/{tab?}/{coin?}', 'WalletController@showWalletForm')->name('wallet')->where('tab', join('|', [
  'deposit',
  'withdraw'
]));
Route::get('wallet/qrcode/{address}/{amount?}', 'WalletController@qrcode')->name('wallet.qrcode');
Route::post('wallet/send', 'WalletController@send')->name('wallet.send');

// 创建柜台。
Route::get('counter/create', 'CounterController@showCreateForm')->name('counter.create');
Route::post('counter/create', 'CounterController@create');
// 编辑柜台。
Route::get('counter/edit', 'CounterController@showEditForm')->name('counter.edit');
Route::post('counter/edit', 'CounterController@edit');
Route::post('counter/zd', 'CounterController@zd')->name('counter.zd');

// 柜台管理。
Route::get('counter/list', 'CounterController@list')->name('counter.list');
// 关闭柜台。
Route::post('counter/closed', 'CounterController@closed')->name('counter.closed');
// 开放柜台。
Route::post('counter/open', 'CounterController@open')->name('counter.open');
// 柜台详情。
Route::get('counter/show', 'CounterController@show')->name('counter.show');

// 创建订单。
Route::post('order/create', 'OrderController@create')->name('order.create');
// 订单详情。
Route::get('order/detail', 'OrderController@detail')->name('order.detail');
// 订单列表。
Route::get('order/list', 'OrderController@showList')->name('order.list');
// 撤销订单。
Route::get('order/revoke', 'OrderController@revokeConfirmForm')->name('order.revoke');
Route::post('order/revoke', 'OrderController@revoke');
// 重启订单。
Route::post('order/restart', 'OrderController@restart')->name('order.restart');
// 已付款。
Route::post('order/payment', 'OrderController@payment')->name('order.payment');
// 释放托管。
Route::post('order/release', 'OrderController@release')->name('order.release');
// 评论订单。
Route::post('order/review', 'OrderController@review')->name('order.review');

// 消息。
// 发送消息。
Route::post('message/send', 'MessageController@send')->name('message.send');
// 标记消息为已读。
Route::post('message/read', 'MessageController@read')->name('message.read');
// 显示消息内的图片。
Route::get('message/image/{message_id}', 'MessageController@showImage')->name('message.image');

// 信任关系。
// 添加信任。
Route::post('trust/add', 'TrustController@add')->name('trust.add');
// 移除信任。
Route::post('trust/remove', 'TrustController@remove')->name('trust.remove');
// 信任我的人。
Route::get('trust/by', 'TrustController@by')->name('trust.by');
// 我信任的人。
Route::get('trust/my', 'TrustController@my')->name('trust.my');

// 黑名单。
// 添加黑名单。
Route::post('ignore/add', 'IgnoreController@add')->name('ignore.add');
// 移除黑名单。
Route::post('ignore/remove', 'IgnoreController@remove')->name('ignore.remove');
// 我屏蔽的人。
Route::get('ignore/my', 'IgnoreController@my')->name('ignore.my');

// 文章详情。
//Route::get('article/{id}', 'ArticleController@show')->name('article.show');
Route::get('article/notice', 'ArticleController@show')->name('article.notice');
Route::get('article/help', 'ArticleController@show')->name('article.help');

// 更新支付宝收款信息
Route::post('/user/payinfo/update/ali', 'UserController@updateAliPay')->name('update.ali');
Route::post('/user/payinfo/update/wechat', 'UserController@updateWechatPay')->name('update.whchat');
Route::post('/user/payinfo/update/bank', 'UserController@updateBank')->name('update.bank');