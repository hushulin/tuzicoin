<?php
use App\Models\Order;

/*
 |--------------------------------------------------------------------------
 | Broadcast Channels
 |--------------------------------------------------------------------------
 |
 | Here you may register all of the event broadcasting channels that your
 | application supports. The given channel authorization callbacks are
 | used to check if an authenticated user can listen to the channel.
 |
 */

// 全站聊天频道。目前只是用于判断用户是否在线。
Broadcast::channel('chat', function ($user) {
	return $user->only('id', 'username');
});

// 订单消息频道。
Broadcast::channel('order.{order_id}', function ($user, $order_id) {
	$order = Order::find($order_id);
	return $user->id === $order->buyer_id || $user->id === $order->seller_id;
});

// 用户消息通知。
Broadcast::channel('user.{id}', function ($user, $id) {
	return (int) $user->id === (int) $id;
});
