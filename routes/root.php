<?php
/**
 * 后台路由表
 *
 * @author Latrell Chan
 *
 */

// 用户验证。
Route::group([
	'prefix' => 'auth'
], function () {

	// 登录。
	Route::get('login', [
		'as' => 'RootLogin',
		'uses' => 'AuthController@getLogin'
	]);
	Route::post('login', [
		'as' => 'RootLoginAction',
		'uses' => 'AuthController@postLogin'
	]);

	// 退出。
	Route::any('logout', [
		'as' => 'RootLogout',
		'uses' => 'AuthController@anyLogout'
	]);
});

// 需要登录的路由组。
Route::group([
	'middleware' => [
		'auth:root',
		'permission:root',
		'audit:root'
	]
], function () {

	// 仪表盘。
	Route::get('/', [
		'as' => 'RootDashboard',
		'uses' => 'HomeController@getDashboard'
	]);

	// 管理员管理。
	Route::group([
		'prefix' => 'admin'
	], function () {

		// 用户列表。
		Route::get('list', [
			'as' => 'RootAdminList',
			'uses' => 'AdminController@getList'
		]);

		// 用户编辑页。
		Route::get('edit', [
			'as' => 'RootAdminEdit',
			'uses' => 'AdminController@getEdit'
		]);

		// 保存编辑。
		Route::post('save', [
			'as' => 'RootAdminEditAction',
			'uses' => 'AdminController@postEdit'
		]);

		// 删除用户。
		Route::post('delete', [
			'as' => 'RootAdminDelete',
			'uses' => 'AdminController@postDelete'
		]);
	});

	// 角色管理。
	Route::group([
		'prefix' => 'role'
	], function () {

		// 角色列表。
		Route::get('list', [
			'as' => 'RootRoleList',
			'uses' => 'RoleController@getList'
		]);

		// 角色编辑页。
		Route::get('edit', [
			'as' => 'RootRoleEdit',
			'uses' => 'RoleController@getEdit'
		]);

		// 保存编辑。
		Route::post('save', [
			'as' => 'RootRoleEditAction',
			'uses' => 'RoleController@postEdit'
		]);

		// 删除角色。
		Route::post('delete', [
			'as' => 'RootRoleDelete',
			'uses' => 'RoleController@postDelete'
		]);
	});

	// 权限管理。
	Route::group([
		'prefix' => 'permission'
	], function () {

		// 权限列表。
		Route::get('list', [
			'as' => 'RootPermissionList',
			'uses' => 'PermissionController@getList'
		]);

		// 权限编辑页。
		Route::get('edit', [
			'as' => 'RootPermissionEdit',
			'uses' => 'PermissionController@getEdit'
		]);

		// 保存编辑。
		Route::post('save', [
			'as' => 'RootPermissionEditAction',
			'uses' => 'PermissionController@postEdit'
		]);

		// 删除权限。
		Route::post('delete', [
			'as' => 'RootPermissionDelete',
			'uses' => 'PermissionController@postDelete'
		]);
	});

	// 操作记录管理。
	Route::group([
		'prefix' => 'operation-record'
	], function () {

		// 操作记录列表。
		Route::get('list', [
			'as' => 'RootOperationRecordList',
			'uses' => 'OperationRecordController@getList'
		]);

		// 操作记录详情。
		Route::get('detail', [
			'as' => 'RootOperationRecordDetail',
			'uses' => 'OperationRecordController@getDetail'
		]);

		// 响应内容查看。
		Route::get('response-view', [
			'as' => 'RootOperationRecordResponseView',
			'uses' => 'OperationRecordController@getResponseView'
		]);
	});

	// 账号信息。
	Route::group([
		'prefix' => 'profile'
	], function () {

		// 修改密码。
		Route::get('change-password', [
			'as' => 'RootChangePassowrd',
			'uses' => 'ProfileController@getChangePassword'
		]);
		Route::post('change-password', [
			'as' => 'RootChangePassowrdAction',
			'uses' => 'ProfileController@postChangePassword'
		]);
	});

	// 配置管理。
	Route::group([
		'prefix' => 'config'
	], function () {

		// 编辑系统配置。
		Route::get('edit', [
			'as' => 'RootConfigEdit',
			'uses' => 'ConfigController@getEdit'
		]);
		// 保存系统配置。
		Route::post('save', [
			'as' => 'RootConfigSave',
			'uses' => 'ConfigController@postSave'
		]);
	});

	// 会员管理。
	Route::group([
		'prefix' => 'user'
	], function () {

		// 会员列表。
		Route::get('list', [
			'as' => 'RootUserList',
			'uses' => 'UserController@getList'
		]);

		// 会员详情。
		Route::get('detail', [
			'as' => 'RootUserDetail',
			'uses' => 'UserController@getDetail'
		]);

		// 登陆日志。
		Route::get('login-log', [
			'as' => 'RootUserLoginLogList',
			'uses' => 'UserController@getLoginLogList'
		]);

		// 伪装登录。
		Route::post('login', [
			'as' => 'RootUserLogin',
			'uses' => 'UserController@postLogin'
		]);
	});

	// 提现管理。
	Route::group([
		'prefix' => 'withdraw'
	], function () {

		// 提现列表。
		Route::get('list', [
			'as' => 'RootWithdrawList',
			'uses' => 'WithdrawController@getList'
		]);

		// 处理完成。
		Route::post('completed', [
			'as' => 'RootWithdrawCompleted',
			'uses' => 'WithdrawController@postCompleted'
		]);

		// 拒绝提现。
		Route::post('refuse', [
			'as' => 'RootWithdrawRefuse',
			'uses' => 'WithdrawController@postRefuse'
		]);
	});

	// 存款单管理。
	Route::group([
		'prefix' => 'deposit'
	], function () {

		// 存款单列表。
		Route::get('list', [
			'as' => 'RootDepositList',
			'uses' => 'DepositController@getList'
		]);
	});

	// 交易记录。
	Route::group([
		'prefix' => 'transaction'
	], function () {

		// 交易记录列表。
		Route::get('list', [
			'as' => 'RootTransactionList',
			'uses' => 'TransactionController@getList'
		]);
	});

	// 钱包地址管理。
	Route::group([
		'prefix' => 'address'
	], function () {

		// 地址列表。
		Route::get('list', [
			'as' => 'RootAddressList',
			'uses' => 'AddressController@getList'
		]);

		// 导入地址。
		Route::get('create', [
			'as' => 'RootAddressCreate',
			'uses' => 'AddressController@getCreate'
		]);
		Route::post('save', [
			'as' => 'RootAddressSave',
			'uses' => 'AddressController@postSave'
		]);
	});

	// 订单管理。
	Route::group([
		'prefix' => 'order'
	], function () {

		// 订单列表。
		Route::get('list', [
			'as' => 'RootOrderList',
			'uses' => 'OrderController@getList'
		]);

        // 编辑订单。
        Route::get('edit', [
            'as' => 'RootOrderEdit',
            'uses' => 'OrderController@getEdit'
        ]);

        // 保存编辑。
        Route::post('save', [
            'as' => 'RootOrderEditAction',
            'uses' => 'OrderController@postEdit'
        ]);
	});

	// 柜台管理。
	Route::group([
		'prefix' => 'conuter'
	], function () {

		// 柜台列表。
		Route::get('list', [
			'as' => 'RootCounterList',
			'uses' => 'CounterController@getList'
		]);
	});

	// 国家信息。
	Route::group([
		'prefix' => 'country'
	], function () {

		// 国家列表。
		Route::get('list', [
			'as' => 'RootCountryList',
			'uses' => 'CountryController@getList'
		]);
	});

	// 身份验证。
	Route::group([
		'prefix' => 'authentication'
	], function () {

		// 验证信息列表。
		Route::get('list', [
			'as' => 'RootAuthenticationList',
			'uses' => 'AuthenticationController@getList'
		]);

		// 身份验证图片。
		Route::get('image', [
			'as' => 'RootAuthenticationImage',
			'uses' => 'AuthenticationController@showVerificationImage'
		]);

		// 编辑验证信息。
		Route::get('edit', [
			'as' => 'RootAuthenticationEdit',
			'uses' => 'AuthenticationController@getEdit'
		]);

		// 保存编辑。
		Route::post('save', [
			'as' => 'RootAuthenticationSave',
			'uses' => 'AuthenticationController@postSave'
		]);

		// 删除验证信息。
		Route::post('delete', [
			'as' => 'RootAuthenticationDelete',
			'uses' => 'AuthenticationController@postDelete'
		]);
	});

	// 市场价数据。
	Route::group([
		'prefix' => 'market-price'
	], function () {

		// 市场价列表。
		Route::get('list', [
			'as' => 'RootMarketPriceList',
			'uses' => 'MarketPriceController@getList'
		]);
	});

	// 法币汇率。
	Route::group([
		'prefix' => 'exchange-rate'
	], function () {

		// 汇率列表。
		Route::get('list', [
			'as' => 'RootExchangeRateList',
			'uses' => 'ExchangeRateController@getList'
		]);
	});

	// 国家信息。
	Route::group([
		'prefix' => 'country'
	], function () {

		// 国家列表。
		Route::get('list', [
			'as' => 'RootCountryList',
			'uses' => 'CountryController@getList'
		]);
	});

	// 货币信息。
	Route::group([
		'prefix' => 'currency'
	], function () {

		// 货币列表。
		Route::get('list', [
			'as' => 'RootCurrencyList',
			'uses' => 'CurrencyController@getList'
		]);
	});

	// 统计。
	Route::group([
		'prefix' => 'statistics'
	], function () {

		// 用户统计。
		Route::get('user', [
			'as' => 'RootStatisticsUser',
			'uses' => 'StatisticsController@getUser'
		]);

		// 流水统计。
		Route::get('flow', [
			'as' => 'RootStatisticsFlow',
			'uses' => 'StatisticsController@getFlow'
		]);
	});

	// 文章管理。
	Route::group([
		'prefix' => 'article'
	], function () {

		// 文章列表。
		Route::get('list', [
			'as' => 'RootArticleList',
			'uses' => 'ArticleController@getList'
		]);

		// 编辑文章。
		Route::get('edit', [
			'as' => 'RootArticleEdit',
			'uses' => 'ArticleController@getEdit'
		]);

		// 保存文章。
		Route::post('save', [
			'as' => 'RootArticleSave',
			'uses' => 'ArticleController@postSave'
		]);

		// 删除文章。
		Route::post('delete', [
			'as' => 'RootArticleDelete',
			'uses' => 'ArticleController@postDelete'
		]);
	});
});
