#!/bin/bash
#author: Eric Hu <hushulin12321@gmail.com>
#date: 2018-05-24

function getBin {
	local t1=$1
	echo `/usr/bin/whereis $t1 | awk '{ print $2 }'`
}

basepath=$(cd `dirname $0`; pwd)

cd ${basepath}

`getBin chmod` -R a+rw storage bootstrap/cache public

# `getBin composer` install

# `getBin composer` update

`getBin composer` dump-autoload

# `getBin npm` install

# `getBin npm` run production